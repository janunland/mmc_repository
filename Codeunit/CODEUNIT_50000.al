codeunit 50000 SalesHeaderEvents
{
    trigger OnRun()
    begin

    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterValidateEvent', 'Sell-to Customer No.', false, false)]
    local procedure MyProcedure(var REC: Record "Sales Header")
    begin
        REC.OrderOnlyNo := DelChr(REC."No.", '=', '-');

        Rec."Order Date Registration" := Today;
    end;




}