codeunit 50001 "MMC_Subcriber"
{
    trigger OnRun()
    begin

    end;

    [EventSubscriber(ObjectType::Table, Database::"Purch. Rcpt. Line", 'OnBeforeInsertInvLineFromRcptLine', '', true, true)]
    local procedure AddInvoiceNo(VAR PurchLine: Record "Purchase Line"; var PurchRcptLine: record "Purch. Rcpt. Line")
    begin

        IF PurchHeader.GET(PurchLine."Document Type"::Order, PurchRcptLine."Order No.") THEN
            PurchLine."MMC Invoice No." := PurchHeader."Vendor Invoice No.";
        PurchLine."MMC Ship-to Name" := PurchHeader."Ship-to Name";



    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostDropShipment', '', true, true)]
    local procedure SalesPostOnAfterPostSalesDoc(var SalesOrderHeader: Record "Sales Header"; var SalesShptHeader: Record "Sales Shipment Header")
    var
        SalesLine: Record "Sales Line";

    begin
        IF SalesOrderHeader."Document Type" <> SalesOrderHeader."Document Type"::Order THEN
            EXIT;

        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", SalesOrderHeader."Document Type");
        SalesLine.SETRANGE("Document No.", SalesOrderHeader."No.");
        if SalesLine.FindSet() then begin
            repeat
                if SalesLine.Quantity <> SalesLine."Qty. to Ship" then begin
                    UpdateHeader := true;

                end;

            until (SalesLine.Next() = 0) or UpdateHeader;
        end;
        if UpdateHeader then
            SalesOrderHeader.ICTransfer := 'PartShp' else
            SalesOrderHeader.ICTransfer := 'CompShp';
        SalesOrderHeader.Modify();
    END;




    var
        PurchHeader: Record "Purchase Header";
        PurchLine: record "Purchase Line";
        UpdateHeader: Boolean;







}