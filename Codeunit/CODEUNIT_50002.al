codeunit 50002 "MMC Value Entry"
{
    Permissions = tabledata "Value Entry" = rim;
    trigger OnRun()
    begin

    end;

    [EventSubscriber(ObjectType::Table, Database::"Value Entry", 'OnAfterInsertEvent', '', true, true)]


    local procedure OnAfterInsertEvent(VAR REC: Record "Value Entry")

    begin

        if SSH.get(rec."Document No.") then Begin
            rec.Auftrag := ssh."Order No.";

            // Sales Header
            if SH.get(SH."Document Type"::Order, rec.Auftrag) then
                rec.Description := SH."Ship-to Name";
            // Sales Line
            if SL.get(SL."Document Type"::Order, rec.Auftrag, rec."Document Line No.") then
                rec.UnitPrice := SL."Unit Price";
            rec.Modify();
        END;
        //Sales Invoice Header
        // if SIH.get(rec."Document No.") then begin
        //    rec.Auftrag := sih."Order No.";
        //END;



    end;



    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', true, true)]
    local procedure SalesPostOnAfterPostSalesDoc(var SalesHeader: Record "Sales Header"; SalesInvHdrNo: Code[20]; SalesShptHdrNo: Code[20])
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
        SalesShipmentLine: Record "Sales Shipment Line";
        ValueEntry: Record "Value Entry";
    begin



        SalesInvoiceLine.Reset();
        SalesInvoiceLine.SetRange("Document No.", SalesInvHdrNo);
        SalesInvoiceLine.SetRange(Type, SalesInvoiceLine.Type::Item);
        if SalesInvoiceLine.FindSet() then begin
            repeat
                ValueEntry.Reset();
                ValueEntry.SetRange("Document Type", ValueEntry."Document Type"::"Sales Invoice");
                ValueEntry.SetRange("Document No.", SalesInvoiceLine."Document No.");
                ValueEntry.SetRange("Document Line No.", SalesInvoiceLine."Line No.");
                if ValueEntry.FindLast() then begin

                    ValueEntry.UnitPrice := SalesInvoiceLine."Unit Price";
                    ValueEntry.Description := SalesHeader."Ship-to Name";
                    ValueEntry.Auftrag := SalesHeader."No.";
                    ValueEntry.Modify();
                end;
            until SalesInvoiceLine.Next() = 0;
        end;
    end;



    var
        ValEntry: record "Value Entry";
        SSH: record "Sales Shipment Header";
        SH: record "Sales Header";
        SIH: record "Sales Invoice Header";
        SL: record "Sales Line";
        SIL: record "Sales Invoice Line";

}
