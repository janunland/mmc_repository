pageextension 50000 MMC_CustomerCard extends "Customer Card"
{
    layout
    {
        addlast(General)
        {
            field("Credit Check Factor"; "Credit Check Factor") { }
            field("Credit Check Date"; "Credit Check Date") { }
            field("Atradius-ID"; "Atradius-ID") { }
            field(CRMID; CRMID) { }
            field(CRMURL; CRMURL)
            {
                Editable = false;
            }
            field("Combine Shipment Code"; "Combine Shipment Code") { }
            field(LTG; LTG) { }
            field("Area Responsible"; "Area Responsible") { }
            field(MCA; MCA) { }
            field("MCA-ID"; "MCA-ID") { }
            field("Internal Limit"; "Internal Limit") { }
            field("Creditreform-ID"; "Creditreform-ID") { }

        }
    }
    actions
    {
        addlast("&Customer")
        {


            Action(MyAction1)
            {
                ApplicationArea = All;
                Caption = 'CRM';
                Image = CalculateCalendar;

                trigger OnAction();
                begin
                    HYPERLINK(CRMURL);
                end;
            }
            Action(MyAction2)
            {
                ApplicationArea = All;
                Caption = 'Ticket';
                image = Archive;

                trigger OnAction();
                begin
                    HYPERLINK(STRSUBSTNO('http://opti.intra.hitachi.local/#/queues/nav_id/%1', "No."));
                end;
            }

            Action(MyAction3)
            {
                ApplicationArea = All;
                Caption = 'Open Orders';
                image = Archive;

                trigger OnAction();
                var
                    Question: Text;
                    Answer: Boolean;
                    CustomerNo: Code[20];
                    Text000: Label 'Do you want to open Order Page for this Customer?';
                    SOL: Page "Sales Order List";
                    SH: record "Sales Header";

                begin
                    SH.SetCurrentKey("Sell-to Customer No.");
                    SH.setrange("Sell-to Customer No.", "No.");
                    SOL.SetTableView(SH);
                    CustomerNo := "No.";
                    Question := Text000;
                    Answer := Dialog.CONFIRM(Question, TRUE, CustomerNo);
                    if Answer = true then
                        SOL.run;
                end;
            }


        }
    }

}