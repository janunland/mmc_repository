pageextension 50001 MMC_CustomerList extends "Customer List"
{
    layout
    {
        addlast(control1)
        {
            field("Credit Check Factor"; "Credit Check Factor") { }
            field("Credit Check Date"; "Credit Check Date") { }
            field("Atradius-ID"; "Atradius-ID") { }
            field("Combine Shipment Code"; "Combine Shipment Code") { }
            field(MCA; MCA) { }
            field("MCA-ID"; "MCA-ID") { }
            field("Internal Limit"; "Internal Limit") { }

        }

    }
}