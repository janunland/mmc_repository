pageextension 50002 MMC_OrderCard extends "Sales Order"
{
    layout
    {
        addlast(General)
        {
            field("Order Date Registration"; "Order Date Registration") { }
            field("MMC_Customer Type"; "MMC_Customer Type") { }
            field("Combine shiptments code"; "Combine shiptments code") { }
            field(OrderOnlyNo; OrderOnlyNo) { }
            field("External Document No."; "External Document No.") { }
            field(Restbudget; CalcRemainingTestToolBudget) { CaptionML = DEU = 'Restbudget', ENU = 'Remaining budget'; }
            field(TTB; TTB) { }
            field("Due Date"; "Due Date") { }
            field(ICTransfer; ICTransfer) { }
            field(ChangedDateSub; ChangedDateSub)
            {
                CaptionML = ENU = 'Changed Date',
                DEU = 'geändertes gep. Lieferdatum';

            }


        }
        addfirst(FactBoxes)
        {
            part(MMC_STOCK_FACT; MMC_STOCK_FACT)
            {
                ApplicationArea = Suite;
                Provider = SalesLines;
                SubPageLink = "Item No." = field("No.");
                CaptionML = ENU = 'Moldino Stocks',
                DEU = 'Moldino Lager';

            }






        }

    }
    actions
    {
        addlast(Documents)
        {


            Action(MyAction1)
            {
                ApplicationArea = All;
                Caption = 'Ticket';
                image = Archive;

                trigger OnAction();
                begin
                    HYPERLINK(STRSUBSTNO('http://opti.intra.hitachi.local/#/queues/nav_id/%1', "No."));
                end;
            }
            Action(MyAction2)
            {
                ApplicationArea = All;
                Caption = 'Ret.Ship.Document';
                image = Archive;

                trigger OnAction();
                begin
                    Rec.SETRANGE("No.", Rec."No.");
                    REPORT.RUNMODAL(50009, TRUE, TRUE, Rec);
                end;
            }




        }
        modify(ICDropShip)


        {
            trigger OnBeforeAction();

            begin

                IF TTB = true THEN BEGIN
                    IF "Document Type" = "Document Type"::Order THEN BEGIN
                        ShipAndInvoice := Ship;

                        begin
                            DecreaseTestToolBudget;


                        END;
                    END;
                END;

            End;

            trigger OnAfterAction()
            begin
                Rec.ICTransfer := 'ICDrop';
                rec.Modify();


            end;
        }
    }

    trigger OnAfterGetRecord()
    begin
        if not Dimension.GET('18', "Sell-to Customer No.", 'DEBITORENGRUPPE') then
            CLEAR(Dimension);
        "MMC_Customer Type" := Dimension."Dimension Value Code";

        if not Cust.GET("Sell-to Customer No.") then
            CLEAR(Cust);
        "MMC_Customer Type" := Dimension."Dimension Value Code";
        "Combine shiptments code" := Cust."Combine Shipment Code";


    end;

    var

        Dimension: record "Default Dimension";
        Cust: record Customer;
        SalesHeader: record "Sales Header";
        ShipAndInvoice: Boolean;
        SL2: record "Sales Line";





    procedure DecreaseTestToolBudget();

    var

        salesLineLoc: record "Sales Line";
        testtoolBudgetLoc: record "MMC Testtool Budget";
        testtoolBudgetDetailsLoc: record "MMC Testtool Budget Details";
        DocumentAmount: decimal;
        LineToInvoice: Boolean;
        ILE: record "FAV External Inventory";
        Inventory: Decimal;
        NoDeduct: Boolean;




    begin


        testtoolBudgetLoc.SETRANGE("Sales Person", "Salesperson Code");
        testtoolBudgetLoc.SETFILTER("Valid from", '<=%1', "Posting Date");
        testtoolBudgetLoc.SETFILTER("Valid to", '>=%1', "Posting Date");



        IF testtoolBudgetLoc.FINDFIRST THEN BEGIN
            salesLineLoc.SETRANGE("Document Type", "Document Type");
            salesLineLoc.SETRANGE("Document No.", "No.");



            DocumentAmount := 0;



            IF salesLineLoc.FINDFirst THEN BEGIN

                with ILE do begin
                    SETRANGE("Item No.", salesLineLoc."No.");
                    SETRANGE("Location Code", salesLineLoc."Location Code");



                    if FINDFirst then begin
                        ile.calcsums(Quantity);


                    END;
                END;
                IF salesLineLoc.Quantity >= ile.Quantity then begin
                    NoDeduct := true;
                END;

                if NoDeduct <> true then
                    REPEAT




                        LineToInvoice := TRUE;
                        IF NOT ShipAndInvoice THEN BEGIN
                            IF salesLineLoc.Quantity <= ile.Quantity then begin

                                LineToInvoice := salesLineLoc."Quantity" - salesLineLoc."Quantity Shipped" <> 0;
                            END;

                        END;
                        IF LineToInvoice THEN BEGIN
                            IF salesLineLoc.Quantity <= ile.Quantity then begin



                                //testtoolBudgetLoc."Remaining budget (MW)" -= (salesLineLoc."Qty. to Ship" * salesLineLoc."Unit Price");
                                testtoolBudgetLoc."Remaining budget (MW)" -= (salesLineLoc."Qty. to Invoice" * salesLineLoc."Unit Price");


                                testtoolBudgetLoc.MODIFY;

                                DocumentAmount -= (salesLineLoc."Qty. to Invoice" * salesLineLoc."Unit Price");

                            END;


                        END;

                        if salesLineLoc.Quantity > ile.Quantity then begin
                            Message('No stock for ' + salesLineLoc."No.");

                        END;

                    UNTIL salesLineLoc.NEXT = 0;

            END;

        END;


        IF salesLineLoc.Quantity <= ile.Quantity then begin
            testtoolBudgetDetailsLoc.SETRANGE("Sales Person", "Salesperson Code");
            testtoolBudgetDetailsLoc.SETFILTER("Valid from", '<=%1', "Posting Date");
            testtoolBudgetDetailsLoc.SETFILTER("Valid to", '>=%1', "Posting Date");
            IF testtoolBudgetDetailsLoc.FINDLAST THEN BEGIN
                testtoolBudgetDetailsLoc.INIT;
                testtoolBudgetDetailsLoc.Position := testtoolBudgetDetailsLoc.Position + 1000;
                testtoolBudgetDetailsLoc."Sales Person" := testtoolBudgetLoc."Sales Person";
                testtoolBudgetDetailsLoc."Valid from" := testtoolBudgetLoc."Valid from";
                testtoolBudgetDetailsLoc."Valid to" := testtoolBudgetLoc."Valid to";
                testtoolBudgetDetailsLoc."Document Type" := "Document Type";
                testtoolBudgetDetailsLoc."Document No" := "No.";
                testtoolBudgetDetailsLoc.Amount := DocumentAmount;
                testtoolBudgetDetailsLoc.INSERT;

            END ELSE BEGIN
                testtoolBudgetDetailsLoc.INIT;
                testtoolBudgetDetailsLoc.Position := 1000;
                testtoolBudgetDetailsLoc."Sales Person" := testtoolBudgetLoc."Sales Person";
                testtoolBudgetDetailsLoc."Valid from" := testtoolBudgetLoc."Valid from";
                testtoolBudgetDetailsLoc."Valid to" := testtoolBudgetLoc."Valid to";
                testtoolBudgetDetailsLoc."Document Type" := "Document Type";
                testtoolBudgetDetailsLoc."Document No" := "No.";
                testtoolBudgetDetailsLoc.Amount := DocumentAmount;
                testtoolBudgetDetailsLoc.INSERT;

            END;
            //stop kvs008.tst

        END;
    End;

    procedure CheckStock() NoDeduct: Boolean;

    var

        salesLineLoc: record "Sales Line";
        testtoolBudgetLoc: record "MMC Testtool Budget";
        testtoolBudgetDetailsLoc: record "MMC Testtool Budget Details";
        nostockcount: integer;

        ILE: record "Item Ledger Entry";






    begin


        testtoolBudgetLoc.SETRANGE("Sales Person", "Salesperson Code");
        testtoolBudgetLoc.SETFILTER("Valid from", '<=%1', "Posting Date");
        testtoolBudgetLoc.SETFILTER("Valid to", '>=%1', "Posting Date");
        nostockcount := 0;


        IF testtoolBudgetLoc.FINDFIRST THEN BEGIN
            salesLineLoc.SETRANGE("Document Type", "Document Type");
            salesLineLoc.SETRANGE("Document No.", "No.");







            IF salesLineLoc.FINDFirst THEN
                repeat

                    with ILE do begin
                        SETRANGE("Item No.", salesLineLoc."No.");
                        SETRANGE("Location Code", salesLineLoc."Location Code");



                        if FINDFirst then begin
                            ile.calcsums(Quantity);
                            IF salesLineLoc.Quantity >= ile.Quantity then begin
                                nostockcount += 1;

                            END;
                        END;
                    END;
                    if nostockcount > 0 then begin
                        NoDeduct := true;
                    END;
                until salesLineLoc.NEXT = 0;

        END;

    End;

}