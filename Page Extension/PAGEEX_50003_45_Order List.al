pageextension 50003 MMC_OrderList extends "Sales Order List"
{
    layout
    {
        addlast(control1)

        {
            field("MMC_Customer Type"; "MMC_Customer Type") { }
            field("Combine shiptments code"; "Combine shiptments code") { }
            field("Changed by IC"; "Changed by IC") { }
            field(TTB; TTB) { }
            field(ChangedDateSub; ChangedDateSub)
            {
                CaptionML = ENU = 'Changed Date',
                DEU = 'geändertes gep. Lieferdatum';

            }
            field(ICTransfer; ICTransfer) { }
            field("PO Number"; "PO Number") { }
            field("HQ Number"; "HQ Number") { }
            field("Order Date Registration"; "Order Date Registration") { }


        }
    }
    actions
    {
        addlast(Navigation)
        {



            Action(DelOrd)
            {
                ApplicationArea = All;
                Caption = 'Delete invoiced Orders';
                image = Archive;

                trigger OnAction();
                begin
                    Report.Run(299);
                end;
            }



        }
    }

}