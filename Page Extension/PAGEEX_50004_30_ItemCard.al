pageextension 50004 MMC_ItemCard extends "Item Card"
{
    layout
    {
        addlast(Item)

        {
            field("Customer Request"; "Customer Request")
            {
                CaptionML = ENU = 'Customer Request',
                DEU = 'Auf Kundenwunsch';

            }
            field(Tooltype; Tooltype)
            {
                CaptionML = ENU = 'Tooltype',
                DEU = 'Tooltype';
            }

            field("Packing Unit"; "Packing Unit")
            {
                CaptionML = ENU = 'Packing Unit',
                DEU = 'Verpackungseinheit';
            }
            field(Outranged; Outranged)
            {
                CaptionML = ENU = 'Outranged Item',
                        DEU = 'Auslaufkennzeichen';
            }

        }
    }

}