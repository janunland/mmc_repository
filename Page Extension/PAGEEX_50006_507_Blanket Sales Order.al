pageextension 50006 "MMC_Blanket Sales Order" extends "Blanket Sales Order"
{
    layout
    {
        addlast(General)
        {
            field("Order Date Registration"; "Order Date Registration") { }
            field("MMC_Customer Type"; "MMC_Customer Type") { }
            field("Combine shiptments code"; "Combine shiptments code") { }
            field(OrderOnlyNo; OrderOnlyNo) { }


        }


    }
    actions
    {
        addlast(Navigation)
        {


            Action(MyAction1)
            {
                ApplicationArea = All;
                Caption = 'Ticket';
                image = Archive;

                trigger OnAction();
                begin
                    HYPERLINK(STRSUBSTNO('http://opti.intra.hitachi.local/#/queues/nav_id/%1', "No."));
                end;
            }


        }
    }

    trigger OnAfterGetRecord()
    begin
        if not Dimension.GET('18', "Sell-to Customer No.", 'DEBITORENGRUPPE') then
            CLEAR(Dimension);
        "MMC_Customer Type" := Dimension."Dimension Value Code";

        if not Cust.GET("Sell-to Customer No.") then
            CLEAR(Cust);
        "MMC_Customer Type" := Dimension."Dimension Value Code";
        "Combine shiptments code" := Cust."Combine Shipment Code";


    end;

    var
        Dimension: record "Default Dimension";
        Cust: record Customer;


}