pageextension 50007 "MMC Sales Quote" extends "Sales Quote"
{


    layout
    {
        addfirst(FactBoxes)
        {
            part(MMC_STOCK_FACT; MMC_STOCK_FACT)
            {
                ApplicationArea = Suite;
                Provider = SalesLines;
                SubPageLink = "Item No." = field("No.");
                CaptionML = ENU = 'Moldino Stocks',
                DEU = 'Moldino Lager';

            }






        }
    }



    actions
    {
        addlast(Navigation)
        {


            Action(MyAction1)
            {
                ApplicationArea = All;
                Caption = 'Ticket';
                image = Archive;

                trigger OnAction();
                begin
                    HYPERLINK(STRSUBSTNO('http://opti.intra.hitachi.local/#/queues/nav_id/%1', "No."));
                end;
            }


        }
    }
    trigger OnAfterGetRecord();
    begin
        "Quote Valid To" := Today + 30;
    end;


}