pageextension 50008 "MMC Posted Sales Credit Memo" extends "Posted Sales Credit Memo"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast(Navigation)
        {


            Action(MyAction1)
            {
                ApplicationArea = All;
                Caption = 'Ticket';
                image = Archive;

                trigger OnAction();
                begin
                    HYPERLINK(STRSUBSTNO('http://opti.intra.hitachi.local/#/queues/nav_id/%1', "No."));
                end;
            }


        }
    }
}