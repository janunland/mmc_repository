pageextension 50011 "MMC Posted Sales Invoice" extends "Posted Sales Invoice"
{
    layout
    {
        addlast(General)

        {
            field("Order Date Registration"; "Order Date Registration") { }
            field("MMC_Customer Type"; "MMC_Customer Type") { }
            field("Combine shiptments code"; "Combine shiptments code") { }

        }
    }

}