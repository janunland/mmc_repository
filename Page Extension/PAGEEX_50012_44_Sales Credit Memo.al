pageextension 50012 "MMC Sales Credit Memo" extends "Sales Credit Memo"
{
    layout
    {

        addlast(General)
        {
            field(TTB; TTB) { }
        }


    }

    actions
    {


        modify(Post)
        {
            trigger OnBeforeAction();
            begin

                IF TTB = true THEN BEGIN
                    IF "Document Type" = "Document Type"::"Credit Memo" THEN BEGIN
                        ShipAndInvoice := Ship;
                        IncreaseTestToolBudget;
                    END;
                END;






            End;
        }

    }
    var

        Dimension: record "Default Dimension";
        Cust: record Customer;
        SalesHeader: record "Sales Header";
        ShipAndInvoice: Boolean;
        SL2: record "Sales Line";

    procedure IncreaseTestToolBudget();

    var

        salesLineLoc: record "Sales Line";
        testtoolBudgetLoc: record "MMC Testtool Budget";
        testtoolBudgetDetailsLoc: record "MMC Testtool Budget Details";
        DocumentAmount: decimal;
        LineToInvoice: Boolean;




    begin


        testtoolBudgetLoc.SETRANGE("Sales Person", "Salesperson Code");
        testtoolBudgetLoc.SETFILTER("Valid from", '<=%1', "Posting Date");
        testtoolBudgetLoc.SETFILTER("Valid to", '>=%1', "Posting Date");



        IF testtoolBudgetLoc.FINDFIRST THEN BEGIN
            salesLineLoc.SETRANGE("Document Type", "Document Type");
            salesLineLoc.SETRANGE("Document No.", "No.");


            DocumentAmount := 0;



            IF salesLineLoc.FINDSET THEN BEGIN
                REPEAT


                    testtoolBudgetLoc."Remaining budget (MW)" += (salesLineLoc."Qty. to Invoice" * salesLineLoc."Unit Price");
                    testtoolBudgetLoc.MODIFY;
                    DocumentAmount += (salesLineLoc."Qty. to Invoice" * salesLineLoc."Unit Price");

                UNTIL salesLineLoc.NEXT = 0;
            END;
        END;


        testtoolBudgetDetailsLoc.SETRANGE("Sales Person", "Salesperson Code");
        testtoolBudgetDetailsLoc.SETFILTER("Valid from", '<=%1', "Posting Date");
        testtoolBudgetDetailsLoc.SETFILTER("Valid to", '>=%1', "Posting Date");
        IF testtoolBudgetDetailsLoc.FINDLAST THEN BEGIN
            testtoolBudgetDetailsLoc.INIT;
            testtoolBudgetDetailsLoc.Position := testtoolBudgetDetailsLoc.Position + 1000;
            testtoolBudgetDetailsLoc."Sales Person" := testtoolBudgetLoc."Sales Person";
            testtoolBudgetDetailsLoc."Valid from" := testtoolBudgetLoc."Valid from";
            testtoolBudgetDetailsLoc."Valid to" := testtoolBudgetLoc."Valid to";
            testtoolBudgetDetailsLoc."Document Type" := "Document Type";
            testtoolBudgetDetailsLoc."Document No" := "No.";
            testtoolBudgetDetailsLoc.Amount := DocumentAmount;
            testtoolBudgetDetailsLoc.INSERT;

        END ELSE BEGIN
            testtoolBudgetDetailsLoc.INIT;
            testtoolBudgetDetailsLoc.Position := 1000;
            testtoolBudgetDetailsLoc."Sales Person" := testtoolBudgetLoc."Sales Person";
            testtoolBudgetDetailsLoc."Valid from" := testtoolBudgetLoc."Valid from";
            testtoolBudgetDetailsLoc."Valid to" := testtoolBudgetLoc."Valid to";
            testtoolBudgetDetailsLoc."Document Type" := "Document Type";
            testtoolBudgetDetailsLoc."Document No" := "No.";
            testtoolBudgetDetailsLoc.Amount := DocumentAmount;
            testtoolBudgetDetailsLoc.INSERT;

        END;
        //stop kvs008.tst

    End;

}