pageextension 50015 "MMC Posted Purch.Inv Subform" extends "Posted Purch. Invoice Subform"
{
    layout
    {
        addlast(Control1)
        {
            field("MMC Invoice No."; "MMC Invoice No.") { }
            field("Order No."; "Order No.") { }
            field("MMC Ship-to Name"; "MMC Ship-to Name") { }
            field(Netweight; Netweight) { }
            field("Tariff No."; "Tariff No.") { }
            field(CompNetWeight; CompNetWeight) { }
            field("Customer No."; "Customer No.") { }
            field(Province; Province) { }
            field(Salesperson; Salesperson) { }


        }
    }

    actions
    {
        // Add changes to page actions here
    }
    trigger OnAfterGetRecord()
    begin
        CompNetWeight := rec.Quantity * Netweight;


    end;

    var
        CompNetWeight: Decimal;
}