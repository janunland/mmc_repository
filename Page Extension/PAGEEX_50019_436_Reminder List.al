pageextension 50019 "MMC Reminder List" extends "Reminder List"
{
    layout
    {

        addlast(Control1)
        {
            field("Internal Limit"; "Internal Limit") { }
            field("Credit Limit"; "Credit Limit") { }
            field("Reminder Level"; "Reminder Level") { }
        }

    }

    actions
    {
        // Add changes to page actions her
    }

    var
        myInt: Integer;
}