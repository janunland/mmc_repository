pageextension 50022 MMC_Sales_Order_subform extends "Sales Order Subform"
{
    layout
    {
        addlast(Control1)

        {
            field("customer request"; "Customer Wish") { }
            field(ChangedDate; ChangedDate)
            {
                CaptionML = ENU = 'Changed Date',
                DEU = 'geändertes gep. Lieferdatum';

            }
            field(DatebeforeChange; DatebeforeChange)
            {
                CaptionML = ENU = 'Date before change',
                DEU = 'Datum vor der Änderung';

            }





        }
    }

    actions
    {
        // Add changes to page actions here
    }

    var
        myInt: Integer;
}