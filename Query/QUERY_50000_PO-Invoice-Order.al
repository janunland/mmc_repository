query 50000 "MMC PO Invoice Order"
{
    QueryType = Normal;
    OrderBy = ascending (DocumentNo_, OrderNo_, CustomerName);
    Caption = 'MMC PO Invoice Order';


    elements
    {
        dataitem("PurchaseLine"; "Purchase Line")
        {
            DataItemTableFilter = "Document Type" = const (Invoice), Type = const (Item);


            column("DocumentNo_"; "Document No.") { }
            column("OrderNo_"; "Order Number") { }
            column(CustomerName; "Customer Name") { }
            column(Dumm; "Net Weight")
            {
                Method = Max;

            }


        }

    }


}