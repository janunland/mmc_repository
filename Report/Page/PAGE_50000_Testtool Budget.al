page 50000 "MMC Testtool Budget"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    Editable = true;
    SourceTable = 50000;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field("Sales Person"; "Sales Person") { }
                field("Salesperson Nam."; "Salesperson Nam.") { }
                field("Valid from"; "Valid from") { }
                field("Valid to"; "Valid to") { }
                field("Budget (MW)"; "Budget (MW)") { }
                field("Remaining budget (MW)"; "Remaining budget (MW)") { }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action("Budget zurücksetzen")
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    ResettRemainingBudget;
                end;
            }

            action("Show Details")
            {
                ApplicationArea = All;

                trigger OnAction()
                begin



                    testtoolBudgetDetailsLoc.SETRANGE(testtoolBudgetDetailsLoc."Sales Person", "Sales Person");
                    PAGE.RUN(50002, testtoolBudgetDetailsLoc);



                end;
            }
        }
    }

    var
        testtoolBudgetDetailsLoc: record "MMC Testtool Budget Details";

}