page 50003 "MMC Purchase Invoice Order"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = Integer;
    Caption = 'MMC Purchase Invoice Order';



    layout
    {
        area(Content)
        {
            repeater(Control100)
            {
                field("Purchase Invoice"; MMCPOInvoiceOrder.DocumentNo_) { ApplicationArea = All; }
                field("Customer Order No"; MMCPOInvoiceOrder.OrderNo_) { ApplicationArea = All; }
                field("Customer Name"; MMCPOInvoiceOrder.CustomerName) { ApplicationArea = All; }


            }
        }
    }
    trigger OnOpenPage()
    begin
        MMCPOInvoiceOrder.Open();
        while MMCPOInvoiceOrder.Read() do
            NumberOfRows := NumberOfRows + 1;

        MMCPOInvoiceOrder.Close();
        MMCPOInvoiceOrder.Open();
        SetRange(Number, 1, NumberOfRows);
    end;

    trigger OnClosePage()
    begin
        MMCPOInvoiceOrder.Close();
    end;

    trigger OnAfterGetRecord()
    begin
        if not MMCPOInvoiceOrder.Read() then
            exit;
    end;








    var
        MMCPOInvoiceOrder: Query "MMC PO Invoice Order";
        NumberOfRows: Integer;
}