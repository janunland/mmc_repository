page 50004 "MMC_STOCK_FACT"
{
    PageType = ListPart;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "FAV External Inventory";

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("Location Code"; "Location Code")
                {
                    ApplicationArea = All;

                }
                field(Quantity; Quantity) { }
            }
        }
    }


}