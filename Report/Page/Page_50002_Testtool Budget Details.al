page 50002 "Testtool Budget Details"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    Editable = true;
    SourceTable = "MMC Testtool Budget Details";

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field(Position; Position) { }
                field("Sales Person"; "Sales Person") { }
                field("Salesperson Nam."; "Salesperson Nam.") { }
                field("Valid from"; "Valid from") { }
                field("Valid to"; "Valid to") { }
                field("Document Type"; "Document Type") { }
                field("Document No"; "Document No") { }
                field(Amount; Amount) { }
            }
        }
    }

    actions
    {
        area(Processing)
        {

        }
    }

    var
        myInt: Integer;
}