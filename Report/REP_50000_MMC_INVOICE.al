report 50000 "MMC Sales - Invoice AR"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report/Sales - Invoice NAI.rdl';

    CaptionML = DEU = 'Verkauf - Rechnung',
                ENU = 'Sales - Invoice';
    EnableHyperlinks = true;
    Permissions = TableData "Sales Shipment Buffer" = rimd;

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Geb. Verkaufsrechnung',
                                     ENU = 'Posted Sales Invoice';
            column(No_SalesInvHdr; "No.")
            {
            }
            column(InvDiscountAmtCaption; InvDiscountAmtCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(PaymentTermsCaption; PaymentTermsDescCaptionLbl)
            {
            }
            column(ShipmentMethodCaption; ShptMethodDescCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }
            column(VATCaption; VATCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATIdentifierCaption; VATIdentifierCaptionLbl)
            {
            }
            column(CompanyInfoHomePageCaption; HomePageCaptionLbl)
            {
            }
            column(CompanyInfoEmailCaption; EMailCaptionLbl)
            {
            }
            column(ShowDiscount; ShowDiscount)
            {
            }
            column(SumNetWeightCaption; SumNetWeightLbl)
            {
            }
            column(SumGrossWeightCaption; SumGrossWeightLbl)
            {
            }
            column(LanguageCode; "Sales Invoice Header"."Language Code")
            {
            }
            column(Lieferantennummer; Cust."Our Account No.") { }
            column(Offer; "Sales Invoice Header"."Quote No.") { }
            column(Lieferdatum; Format("Sales Invoice Header"."Shipment Date")) { }
            column(OurAccounttxt; OurAccountCaption) { }
            column(Offertxt; OfferCaption) { }
            column(Transportreasoncpt; TransportReasonLbl) { }
            column(TransportReason; TransportReasontxt) { }
            column(DueDateN; DueDateNew) { }
            column(DueDateLbl; DueDatetxt) { }
            column(InvoiceText; SalesSetup."Invoice Text") { }
            column(Fiscal_Code; "Fiscal Code") { }
            column(Fiscallbl; Fiscaltxt) { }


            column(LogLief; LogLS) { }

            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(DocumentCaptionCopyText; STRSUBSTNO(DocumentCaption, "Sales Invoice Header"."No.", CopyText))
                    {
                    }
                    column(CurrReportPageNo; STRSUBSTNO(Text005, FORMAT(CurrReport.PAGENO)))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(ShowSellToInformation; ShowSellToInfo)
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name; "Sales Invoice Header"."Sell-to Customer Name")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name2; "Sales Invoice Header"."Sell-to Customer Name 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name3; "Sales Invoice Header"."Sell-to Customer Name 3")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name4; "Sales Invoice Header"."Sell-to Customer Name 4")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Address; "Sales Invoice Header"."Sell-to Address")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Address2; "Sales Invoice Header"."Sell-to Address 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Postcode; "Sales Invoice Header"."Sell-to Post Code")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_City; "Sales Invoice Header"."Sell-to City")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesInvHdr; "Sales Invoice Header"."Bill-to Customer No.")
                    {
                    }
                    column(PostingDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Posting Date"))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesInvHdr; "Sales Invoice Header"."VAT Registration No.")
                    {
                    }
                    column(DueDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Due Date", 0, 4))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesInvHdr; "Sales Invoice Header"."Your Reference")
                    {
                    }
                    column(SalesInvHdrExternalDocumentNo; "Sales Invoice Header"."External Document No.")
                    {
                    }
                    column(SalesInvHdrExternalDocumentNoCaption; "Sales Invoice Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(OrderTitle; OrderTitle)
                    {
                    }
                    column(OrderTitleNo; OrderTitleNo)
                    {
                    }
                    column(OrderNoText; OrderNoText)
                    {
                    }
                    column(HdrOrderNo_SalesInvHdr; "Sales Invoice Header"."Order No.")
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(DocDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Document Date", 0, 4))
                    {
                    }
                    column(PricesInclVAT_SalesInvHdr; "Sales Invoice Header"."Prices Including VAT")
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PricesInclVATYesNo_SalesInvHdr; FORMAT("Sales Invoice Header"."Prices Including VAT"))
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text005, ''))
                    {
                    }
                    column(Text5000000_SalesInvHdrNo; STRSUBSTNO(Text5000000, STRSUBSTNO(DocumentCaption, "Sales Invoice Header"."No.", '')))
                    {
                    }
                    column(PaymentTermsDescription; PaymentTerms.Description)
                    {
                    }
                    column(ShipmentMethodDescription; ShipmentMethod.Description)
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(PrepayInvoice; PrepayInvoice)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(DueDate_SalesInvHdrCaption; SalesInvDueDateCaptionLbl)
                    {
                    }
                    column(InvNoCaption; InvNoCaptionLbl)
                    {
                    }
                    column(SalesInvPostingDateCptn; SalesInvPostingDateCptnLbl)
                    {
                    }
                    column(BilltoCustNo_SalesInvHdrCaption; SalesInvHdrBilltoCustomerNoCaptionLbl)
                    {
                    }
                    column(PricesInclVAT_SalesInvHdrCaption; "Sales Invoice Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }
                    column(Text5244793; Text5244793)
                    {
                    }
                    column(Text5244794; Text5244794)
                    {
                    }
                    column(Text5244795; Text5244795)
                    {
                    }
                    column(Text5244796; Text5244796)
                    {
                    }
                    column(Text5244797; Text5244797)
                    {
                    }
                    column(Text5244798; Text5244798)
                    {
                    }
                    column(Text5244799; Text5244799)
                    {
                    }
                    column(Text5244800; Text5244800)
                    {
                    }
                    column(Text5244801; Text5244801)
                    {
                    }
                    column(Text5244802; Text5244802)
                    {
                    }
                    column(Text5244803; Text5244803)
                    {
                    }
                    column(Text5244804; Text5244804)
                    {
                    }
                    column(Text5244805; Text5244805)
                    {
                    }
                    column(Text5244806; Text5244806)
                    {
                    }
                    column(PrintUIDText; false)
                    {
                    }
                    column(UIDText; '')
                    {
                    }
                    column(SumNetWeight; SumNetWeight)
                    {
                    }
                    column(SumGrossWeight; SumGrossWeight)
                    {
                    }
                    column(ShowNetWeight; ShowNetWeight)
                    {
                    }
                    column(ShowGrossWeight; ShowGrossWeight)
                    {
                    }
                    column(SalesInvoiceHeaderOrderDate; FORMAT("Sales Invoice Header"."Order Date", 0, 4))
                    {
                    }
                    column(PaymentDiscountCaption; PaymentDiscountLbl)
                    {
                    }
                    column(SalesInvHeaderOrderDateCaption; "Sales Invoice Header".FIELDCAPTION("Order Date"))
                    {
                    }
                    column(SalesInvHeaderPaymentDiscount; "Sales Invoice Header"."Payment Discount %")
                    {
                    }
                    column(SenderGLNLbl; SenderGLNLbl)
                    {
                    }
                    column(ShipmentGLNLbl; ShipmentGLNlbl)
                    {
                    }
                    column(InvoiceGLNLbl; InvoiceGLNLbl)
                    {
                    }
                    column(ShipmentGLN; ShipmentGLN)
                    {
                    }
                    column(InvoiceGLN; InvoiceGLN)
                    {
                    }
                    column(SalesInvoiceNoLbl; SalesShipmentNoLbl)
                    {
                    }
                    column(SenderLbl; SenderLbl)
                    {
                    }
                    column(OrderDateLbl; OrderDateLbl)
                    {
                    }
                    column(ShipmentAddressLbl; ShipmentAddressLbl)
                    {
                    }
                    column(OrderDate; "Sales Invoice Header"."Order Date")
                    {
                    }
                    column(DocumentDate; "Sales Invoice Header"."Document Date")
                    {
                    }
                    column(ReferenceLbl; ReferenceLbl)
                    {
                    }
                    column(CompanyInfoGLN; CompanyInfo.GLN)
                    {
                    }
                    column(ShippingAddressText; ShippingAddressText)
                    {
                    }
                    column(ShipmentDateLbl; ShipmentDateLbl)
                    {
                    }
                    column(VATNoLbl; VATNoLbl)
                    {
                    }
                    //column(CustAssociationDescr; Associationgr.Name)
                    //{
                    //}
                    //column(CustAssociationSortCode; Cust."OPP Association Sort Code")
                    //{
                    //}


                    column(ShippingAgentCode; "Sales Invoice Header"."Shipping Agent Code")
                    {
                    }
                    column(ShippingAgentCode_Caption; "Sales Invoice Header".FIELDCAPTION("Shipping Agent Code"))
                    {
                    }
                    column(PackageTrackingNo; "Sales Invoice Header"."Package Tracking No.")
                    {
                    }
                    column(PackageTrackingNo_Caption; "Sales Invoice Header".FIELDCAPTION("Package Tracking No."))
                    {
                    }
                    column(ShipmentDate_SalesInvHdr; "Sales Invoice Header"."Shipment Date")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(Commission; "Sales Invoice Header".Commission)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(CommissionCaption; "Sales Invoice Header".FIELDCAPTION(Commission))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonCaption; "Sales Invoice Header".FIELDCAPTION("Office Sales Person"))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonName; OfficeSalesperson.Name)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonEMail; OfficeSalesperson."E-Mail")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(Currenytxt; Text008) { }
                    column(CurrenyCode; 'EUR') { }
                    column(CustoGLNtxt; CUSTGLNtxt) { }
                    column(CustGLN; Cust.gln) { }
                    column(UserCpt; userlbl) { }
                    column(UserName; User."Full Name") { }
                    column(SalesPurchOfficePersonPhoneNo; OfficeSalesperson."Phone No.")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    dataitem(TempSalesShptLine; "Sales Shipment Line")
                    {
                        DataItemTableView = SORTING("Document No.", "Line No.");
                        UseTemporary = true;
                        column(SalesShipmentHeaderNo; TempSalesShptLine."Document No.")
                        {
                        }
                        column(ShipmentDate; TempSalesShptLine."Shipment Date")
                        {
                        }
                    }
                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Invoice Header";
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FINDSET then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1, %2 %3', DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            NAIExtPreText.RESET;
                            NAIExtPreText.SETRANGE("Table ID", DATABASE::"Sales Invoice Header");
                            NAIExtPreText.SETRANGE("Document No.", "Sales Invoice Header"."No.");
                            NAIExtPreText.SETRANGE("Assigned for", NAIExtPreText."Assigned for"::"Posted Invoice");
                            NAIExtPreText.SETRANGE("Text Position", NAIExtPreText."Text Position"::Pretext);
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Invoice Header";
                        DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(112), "Print Attribute" = CONST(true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }
                    }
                    dataitem("Sales Invoice Line"; "Sales Invoice Line")
                    {
                        DataItemLink = "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Invoice Header";
                        DataItemTableView = SORTING("Document No.", "Line No.");
                        column(LineAmt_SalesInvLine; "Line Amount")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(LineAmount_txt; LineAmount_txt)
                        {
                        }
                        column(Desc_SalesInvLine; Description)
                        {
                        }
                        column(Desc_SalesInvLine2; "Description 2")
                        {
                        }
                        column(Desc_SalesInvLine3; "Description 3")
                        {
                        }
                        column(No_SalesInvLine; "No.")
                        {
                        }
                        column(Qty_SalesInvLine; Quantity)
                        {
                        }
                        column(Quantity_txt; Quantity_txt)
                        {
                        }
                        column(UOM_SalesInvLine; "Unit of Measure")
                        {
                        }
                        column(UnitPrice_SalesInvLine; "Unit Price")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 2;
                        }
                        column(UnitPrice_txt; UnitPrice_txt)
                        {
                        }
                        column(Discount_SalesInvLine; "Line Discount %")
                        {
                        }
                        column(Discount_txt; Discount_txt)
                        {
                        }
                        column(VATIdentifier_SalesInvLine; "VAT Identifier")
                        {
                        }
                        column(PostedShipmentDate; FORMAT(PostedShipmentDate))
                        {
                        }
                        column(Type_SalesInvLine; FORMAT(Type))
                        {
                        }
                        column(PositionNo_SalesInvLine; "Position No.")
                        {
                        }
                        column(QuantityWp_SalesInvLine; 0.0)
                        {
                        }
                        column(ShowLines; ShowLines)
                        {
                        }
                        column(Text5244785; Text5244785)
                        {
                        }
                        column(HideFields; HideFields)
                        {
                        }
                        column(InvDiscLineAmt_SalesInvLine; -"Inv. Discount Amount")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(TotalSubTotal; TotalSubTotal)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalInvDiscAmount; TotalInvDiscAmount)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(Amount_SalesInvLine; Amount)
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(Amount_txt; Amount_txt)
                        {
                        }
                        column(TotalAmount; TotalAmount)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(Amount_AmtInclVAT; "Amount Including VAT" - Amount)
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(AmtInclVAT_SalesInvLine; "Amount Including VAT")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmtText; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(TotalAmountInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountVAT; TotalAmountVAT)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(LineAmtAfterInvDiscAmt; -("Line Amount" - "Inv. Discount Amount" - "Amount Including VAT"))
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseDisc_SalesInvHdr; "Sales Invoice Header"."VAT Base Discount %")
                        {
                            AutoFormatType = 1;
                        }
                        column(TotalPaymentDiscOnVAT; TotalPaymentDiscOnVAT)
                        {
                            AutoFormatType = 1;
                        }
                        column(TotalInclVATText_SalesInvLine; TotalInclVATText)
                        {
                        }
                        column(VATAmtText_SalesInvLine; VATAmountLine.VATAmountText)
                        {
                        }
                        column(DocNo_SalesInvLine; "Document No.")
                        {
                        }
                        column(LineNo_SalesInvLine; "Line No.")
                        {
                        }
                        column(UnitPriceCaption; UnitPriceCaptionLbl)
                        {
                        }
                        column(SalesInvLineDiscCaption; SalesInvLineDiscCaptionLbl)
                        {
                        }
                        column(AmountCaption; AmountCaptionLbl)
                        {
                        }
                        column(PostedShipmentDateCaption; PostedShipmentDateCaptionLbl)
                        {
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(LineAmtAfterInvDiscCptn; LineAmtAfterInvDiscCptnLbl)
                        {
                        }
                        column(Desc_SalesInvLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(No_SalesInvLineCaption; FIELDCAPTION("No."))
                        {
                        }
                        column(Qty_SalesInvLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(UOM_SalesInvLineCaption; UOM_SalesInvLineCaptionLbl)
                        {
                        }
                        column(VATIdentifier_SalesInvLineCaption; VATIdentCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesInvLineCaption; '')
                        {
                        }
                        column(PositionNo_SalesInvLineCaption; SalesInvLinePositionNoCaptionLbl)
                        {
                        }
                        column(DiscountCaption; DiscountCaptionLbl)
                        {
                        }
                        column(LineDisc_SalesInvoiceLine; "Line Discount %")
                        {
                        }
                        column(HideInvoiceLine; HideInvoiceLine)
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(QuantityInBacklog; "Backlog Quantity")
                        {
                        }
                        column(QtyBacklog_txt; QtyBacklog_txt)
                        {
                        }
                        column(LinePaymentDisc; LinePaymentDisc)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalPaymentDisc; TotalPaymentDisc)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(GTINLbl; GTINLbl)
                        {
                        }
                        column(ItemGTIN; ItemGTIN)
                        {
                        }
                        column(UPV; UnitPriceVar) { }
                        column(LDV; LineDiscVar) { }
                        column(SAV; LineAmtVar) { }
                        column(QtyAB; QtyABvar) { }
                        column(CountryOfOrigin_SalesInvLine; "Country/Region of Origin Code")
                        {
                        }
                        column(VendorShipmentNo_SalesInvLine; "Sales Invoice Line"."Vendor Shipment No.")
                        {
                        }
                        column(VendorShipmentNo_SalesInvLineCaption; "Sales Invoice Line".FIELDCAPTION("Vendor Shipment No."))
                        {
                        }
                        dataitem(PaymentReportingArgument; "Payment Reporting Argument")
                        {
                            DataItemTableView = SORTING(Key);
                            UseTemporary = true;
                            column(PaymentServiceLogo; Logo)
                            {
                            }
                            column(PaymentServiceURLText; "URL Caption")
                            {
                            }
                            column(PaymentServiceURL; GetTargetURL)
                            {
                            }

                            trigger OnPreDataItem();
                            var
                                PaymentServiceSetup: Record "Payment Service Setup";
                            begin
                                PaymentServiceSetup.CreateReportingArgs(PaymentReportingArgument, "Sales Invoice Header");
                                if ISEMPTY then
                                    CurrReport.BREAK;
                            end;
                        }
                        dataitem(ValueEntry2; "Value Entry")
                        {
                            DataItemLink = "Document No." = FIELD("Document No."), "Document Line No." = FIELD("Line No.");
                            DataItemTableView = SORTING("Document No.") WHERE("Document Type" = CONST("Sales Invoice"));
                            column(SerialNoCaptionLbl; SerialNoCaptionLbl)
                            {
                            }
                            column(ItemSerialNo; ItemSerialNo)
                            {
                            }
                            column(LotNoCaptionLbl; LotNoCaptionLbl)
                            {
                            }
                            column(ItemLotNo; ItemLotNo)
                            {
                            }
                            column(ExpirationDateCaptionLbl; ExpirationDateCaptionLbl)
                            {
                            }
                            column(ItemExpirationDate; FORMAT(ItemExpirationDate))
                            {
                            }
                            column(ItemQuantity; ABS(ItemQuantity))
                            {
                            }

                            trigger OnAfterGetRecord();
                            var
                                ItemLedgerEntry2: Record "Item Ledger Entry";
                            begin
                                // >>> 2111-34284 25-11-2015 SMS
                                ItemSerialNo := '';
                                ItemLotNo := '';
                                ItemExpirationDate := 0D;
                                ItemQuantity := 0;
                                if ItemLedgerEntry2.GET(ValueEntry2."Item Ledger Entry No.") then begin
                                    ItemSerialNo := ItemLedgerEntry2."Serial No.";
                                    ItemLotNo := ItemLedgerEntry2."Lot No.";
                                    ItemExpirationDate := ItemLedgerEntry2."Expiration Date";
                                    ItemQuantity := ItemLedgerEntry2.Quantity;
                                end;
                                // <<< 2111-34284 25-11-2015 SMS
                            end;
                        }
                        dataitem("Sales Shipment Buffer"; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(SalesShptBufferPostDate; FORMAT(SalesShipmentBuffer."Posting Date"))
                            {
                            }
                            column(SalesShptBufferQty; SalesShipmentBuffer.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(ShipmentCaption; ShipmentCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then
                                    SalesShipmentBuffer.FIND('-')
                                else
                                    SalesShipmentBuffer.NEXT;
                            end;

                            trigger OnPreDataItem();
                            begin
                                SalesShipmentBuffer.SETRANGE("Document No.", "Sales Invoice Line"."Document No.");
                                SalesShipmentBuffer.SETRANGE("Line No.", "Sales Invoice Line"."Line No.");

                                SETRANGE(Number, 1, SalesShipmentBuffer.COUNT);
                            end;
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText_DimLoop; DimText)
                            {
                            }
                            column(LineDimCaption; LineDimCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FINDSET then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Invoice Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(PrePaymentCrMemo; "Integer")
                        {
                            DataItemTableView = SORTING(Number);

                            trigger OnAfterGetRecord();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if Number = 1 then
                                    TempSalesCrMemoHeader.FINDSET
                                else
                                    TempSalesCrMemoHeader.NEXT;
                                // <<< 2111-49001 15-02-2017 RM
                            end;

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if not ShowLines then
                                    CurrReport.BREAK;

                                TempSalesCrMemoHeader.RESET;
                                if TempSalesCrMemoHeader.ISEMPTY then
                                    CurrReport.BREAK;

                                PrePaymentCrMemo.SETRANGE(Number, 1, TempSalesCrMemoHeader.COUNT);
                                // <<< 2111-49001 15-02-2017 RM
                            end;
                        }
                        dataitem("Sales Header Archive"; "Sales Header Archive")
                        {
                            DataItemTableView = SORTING("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
                            column(Text5244802_OrderNo; Text5244802 + "Sales Header Archive"."No.")
                            {
                            }
                            column(SalesHeaderArchive_DocumentType; "Document Type")
                            {
                            }
                            column(SalesHeaderArchive_No; "No.")
                            {
                            }
                            column(SalesHeaderArchive_DocNoOccurrence; "Doc. No. Occurrence")
                            {
                            }
                            column(SalesHeaderArchive_VersionNo; "Version No.")
                            {
                            }
                            dataitem("Sales Line Archive"; "Sales Line Archive")
                            {
                                DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No."), "Doc. No. Occurrence" = FIELD("Doc. No. Occurrence"), "Version No." = FIELD("Version No.");
                                DataItemTableView = SORTING("Document Type", "Document No.", "Doc. No. Occurrence", "Version No.", "Line No.");
                                column(SalesLineArchive_No; "No.")
                                {
                                }
                                column(SalesLineArchive_Description; Description)
                                {
                                }
                                column(SalesLineArchive_Quantity; Quantity)
                                {
                                }
                                column(SalesLineArchive_UnitofMeasure; "Unit of Measure")
                                {
                                }
                                column(SalesLineArchive_Type; Type)
                                {
                                }
                                column(SalesLineArchive_PositionNo; "Position No.")
                                {
                                }
                                column(SalesLineArchive_DocumentType; "Document Type")
                                {
                                }
                                column(SalesLineArchive_DocumentNo; "Document No.")
                                {
                                }
                                column(SalesLineArchive_DocNoOccurrence; "Doc. No. Occurrence")
                                {
                                }
                                column(SalesLineArchive_VersionNo; "Version No.")
                                {
                                }
                                column(SalesLineArchive_LineNo; "Line No.")
                                {
                                }
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if (not ShowLines) and (LastInvLineNo <> "Sales Invoice Line"."Line No.") then
                                    CurrReport.BREAK;
                                SETRANGE("Attached to Prepayment No.", "Sales Invoice Header"."No.");
                                // <<< 2111-49001 15-02-2017 RM
                            end;
                        }
                        dataitem(AsmLoop; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(KitPosNo; '')
                            {
                            }
                            column(TempPostedAsmLineNo; BlanksForIndent + TempPostedAsmLine."No.")
                            {
                            }
                            column(TempPostedAsmLineQuantity; TempPostedAsmLine.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(TempPostedAsmLineDesc; BlanksForIndent + TempPostedAsmLine.Description)
                            {
                            }
                            column(TempPostAsmLineVartCode; BlanksForIndent + TempPostedAsmLine."Variant Code")
                            {
                            }
                            column(TempPostedAsmLineUOM; GetUOMText(TempPostedAsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then
                                    TempPostedAsmLine.FINDSET
                                else
                                    TempPostedAsmLine.NEXT;
                            end;

                            trigger OnPreDataItem();
                            begin
                                CLEAR(TempPostedAsmLine);

                                if not DisplayAssemblyInformation then
                                    CurrReport.BREAK;

                                CollectAsmInformation;
                                CLEAR(TempPostedAsmLine);

                                SETRANGE(Number, 1, TempPostedAsmLine.COUNT);
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source No." = FIELD("Document No."), "Source Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Invoice Line";
                            DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(113), "Print Attribute" = CONST(true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                            NAISetup: Record "NAI - Setup";
                            GeneralSalesMgmt: Codeunit "General Sales Management";
                        begin
                            // >>> 2111-50253 19-06-2017 MMA
                            ItemGTIN := '';
                            // <<< 2111-50253 19-06-2017 MMA
                            // >>> 2111-34284 24-11-2015 DB
                            ItemTariffNo := '';
                            if "Sales Invoice Line".Type = "Sales Invoice Line".Type::Item then begin
                                // >>> 2111-37329 28-07-2016 MK
                                // ItemRec.GET("Sales Invoice Line"."No.");
                                // ItemTariffNo := ItemRec."Tariff No.";
                                // >>> 2111-50253 19-06-2017 MMA
                                //IF ItemRec.GET("Sales Invoice Line"."No.") THEN
                                if ItemRec.GET("Sales Invoice Line"."No.") then begin
                                    // <<< 2111-50253 19-06-2017 MMA
                                    ItemTariffNo := ItemRec."Tariff No.";
                                    // <<< 2111-37329 28-07-2016 MK
                                    // >>> 2111-50253 19-06-2017 MMA
                                    NAISetup.GET;
                                    if NAISetup."Print GTIN on Invoice" then begin
                                        ItemGTIN := ItemRec.GTIN;
                                    end;
                                end;
                                // <<< 2111-50253 19-06-2017 MMA
                            end;

                            // >>> 2111-62373 01-06-2018 RM
                            //QuantityInBacklog := 0;
                            //QuantityInvoiced := 0;
                            //OrderQuantity := 0;

                            // >>> 2111-35647 25-02-2016 RM
                            // IF (Type <> Type::" ") AND ("Order No." <> '') THEN BEGIN
                            //// <<< 2111-35647 25-02-2016 RM
                            //  SalesShipmentLineRel.RESET;
                            //  // >>> 2111-35647 25-02-2016 RM
                            //  SalesShipmentLineRel.SETCURRENTKEY("Order No.","Order Line No.","Posting Date");
                            //  // <<< 2111-35647 25-02-2016 RM
                            //  SalesShipmentLineRel.SETRANGE("Order No.","Sales Invoice Line"."Order No.");
                            //  SalesShipmentLineRel.SETRANGE("Order Line No.","Sales Invoice Line"."Order Line No.");
                            //  IF SalesShipmentLineRel.FINDFIRST THEN BEGIN
                            //    OrderQuantity := SalesShipmentLineRel."Order Quantity";
                            //  END;
                            //
                            //  SalesInvoiceLineRel.RESET;
                            //  // >>> 2111-35647 25-02-2016 RM
                            //  SalesInvoiceLineRel.SETCURRENTKEY("Order No.","Order Line No.","Posting Date");
                            //  // <<< 2111-35647 25-02-2016 RM
                            //  SalesInvoiceLineRel.SETRANGE("Order No.","Sales Invoice Line"."Order No.");
                            //  SalesInvoiceLineRel.SETRANGE("Order Line No.","Sales Invoice Line"."Order Line No.");
                            //  SalesInvoiceLineRel.SETFILTER("Posting Date",'>=%1',"Sales Invoice Line"."Posting Date");
                            //  IF SalesInvoiceLineRel.FINDSET THEN BEGIN
                            //    REPEAT
                            //      QuantityInvoiced += SalesInvoiceLineRel.Quantity;
                            //    UNTIL SalesInvoiceLineRel.NEXT = 0;
                            //    QuantityInBacklog := OrderQuantity - QuantityInvoiced;
                            //  END;
                            //// >>> 2111-35647 25-02-2016 RM
                            //END;
                            //// <<< 2111-35647 25-02-2016 RM
                            // <<< 2111-62373 01-06-2018 RM
                            // <<< 2111-34284 24-11-2015 DB

                            // >>> 2111-34612 27-11-2015 SMS
                            if Type <> Type::" " then begin
                                NAISetup.GET;
                                if (Quantity = 0) and NAISetup."Suppress 0-Qty-Pos. On Invoice" then begin
                                    SkipInvLine := true;
                                    InvLineNoToSkip := "Line No.";
                                    HideInvoiceLine := true;
                                end else begin
                                    SkipInvLine := false;
                                    HideInvoiceLine := false;
                                end;
                            end;

                            if Type = Type::" " then begin
                                if SkipInvLine and ("Attached to Line No." = InvLineNoToSkip) then begin
                                    HideInvoiceLine := true;
                                    CurrReport.SKIP;
                                end;
                            end;
                            // <<< 2111-34612 27-11-2015 SMS

                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // <<< 2111-47914 21-09-2016 MMA
                            // >>> 2111-34604 27-11-2015 SMS
                            if "Sales Invoice Line".Type = "Sales Invoice Line".Type::Item then begin
                                GetItemCrossReference;
                            end;
                            // <<< 2111-34604 27-11-2015 SMS

                            // >>> 2111-49001 15-02-2017 RM
                            if PrepayInvoice and ShowArchivLine then begin
                                if LastInvLineNo = "Sales Invoice Line"."Line No." then begin
                                    "Sales Invoice Line".Description := Text5244801;
                                end;
                            end;

                            begin
                                if "Sales Invoice Line"."Assigned to Line No." > 0 then
                                    UnitPriceVar := '' else
                                    UnitPriceVar := Format("Sales Invoice Line"."Unit Price");

                                if "Sales Invoice Line"."Assigned to Line No." > 0 then
                                    LineDiscVar := '' else
                                    LineDiscVar := Format("Sales Invoice Line"."Line Discount %");

                                if "Sales Invoice Line"."Assigned to Line No." > 0 then
                                    LineAmtVar := Format("Sales Invoice Line"."Line Amount") else
                                    LineAmtVar := Format("Sales Invoice Line"."Line Amount");

                                if "Sales Invoice Line"."Assembly BOM exploded" = true then
                                    LineDiscVar := '';

                                if "Sales Invoice Line"."Assembly BOM exploded" = true then
                                    LineAmtVar := '';

                                if "Sales Invoice Line".Description = 'SET Promotion' then
                                    QtyABvar := '' else
                                    QtyABvar := Format("Sales Invoice Line".Quantity);

                            end;


                            SalesInvNo := "Sales Invoice Line"."No.";

                            ShowLines := false;
                            HideFields := false;
                            if PrepayInvoice and ("Sales Invoice Line".Type = "Sales Invoice Line".Type::"G/L Account") then begin
                                ShowLines := true;
                                HideFields := true;
                            end else begin
                                if (not PrepayInvoice) and ("Sales Invoice Line".Type = "Sales Invoice Line".Type::"G/L Account") and
                                   ("Sales Invoice Line".Quantity < 0)
                                then begin
                                    if ("Sales Invoice Header"."Prepayment Order No." <> '') or ("Sales Invoice Header"."Order No." <> '') then begin
                                        CreatePrePaymentCrMemoLines;
                                        ShowLines := true;
                                        HideFields := true;
                                    end;
                                end;
                            end;

                            if ShowLines then begin
                                if not CreatePrePaymentInvoiceLines then begin
                                    ShowLines := false;
                                    HideFields := false;
                                end;
                            end;
                            // <<< 2111-49001 15-02-2017 RM

                            PostedShipmentDate := 0D;
                            if Quantity <> 0 then
                                PostedShipmentDate := FindPostedShipmentDate;

                            // >>> 2111-49001 15-02-2017 RM
                            //IF (Type = Type::"G/L Account") AND (NOT ShowInternalInfo) THEN
                            if (Type = Type::"G/L Account") and (not ShowInternalInfo) and (not ShowGLAccount) then
                                // <<< 2111-49001 15-02-2017 RM
                                "No." := '';

                            VATAmountLine.INIT;
                            VATAmountLine."VAT Identifier" := "VAT Identifier";
                            VATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                            VATAmountLine."Tax Group Code" := "Tax Group Code";
                            VATAmountLine."VAT %" := "VAT %";
                            VATAmountLine."VAT Base" := Amount;
                            VATAmountLine."Amount Including VAT" := "Amount Including VAT";
                            VATAmountLine."Line Amount" := "Line Amount";
                            if "Allow Invoice Disc." then
                                VATAmountLine."Inv. Disc. Base Amount" := "Line Amount";
                            VATAmountLine."Invoice Discount Amount" := "Inv. Discount Amount";
                            VATAmountLine."VAT Clause Code" := "VAT Clause Code";
                            // >>> 2111-49193 20-02-2017 MMA
                            //VATAmountLine.InsertLine;
                            GeneralSalesMgmt.VATAmountLineInsertLine(VATAmountLine);
                            // <<< 2111-49193 20-02-2017 MMA

                            TotalSubTotal += "Line Amount";
                            TotalInvDiscAmount -= "Inv. Discount Amount";
                            TotalAmount += Amount;
                            TotalAmountVAT += "Amount Including VAT" - Amount;
                            TotalAmountInclVAT += "Amount Including VAT";
                            TotalPaymentDiscOnVAT += -("Line Amount" - "Inv. Discount Amount" - "Amount Including VAT");

                            // >>> 2111-49001 15-02-2017 RM
                            if ShowLines then
                                ShowPrepayAndInvoice := true;
                            // <<< 2111-49001 15-02-2017 RM

                            // >>> 2111-25088 02-09-2014 LK
                            // >>> 2111-51001 21-08-2017 MW
                            //IF "VAT Prod. Posting Group" = TaxSetup."13b VAT Product Posting Group" THEN
                            if "VAT Prod. Posting Group" = TaxSetup."13b-Tax. Prod. Posting Group" then
                                // <<< 2111-51001 21-08-2017 MW
                                "13bLineFound" := true;
                            // <<< 2111-25088 02-09-2014 LK

                            // >>> 2111-34887 23-12-2015 FS
                            Quantity_txt := '';
                            UnitPrice_txt := '';
                            Amount_txt := '';
                            LineAmount_txt := '';
                            QtyBacklog_txt := '';
                            Discount_txt := '';
                            if Type <> Type::" " then begin
                                Quantity_txt := FORMAT(Quantity, 0, 0);
                                UnitPrice_txt := FORMAT("Unit Price", 0, 0);
                                Amount_txt := FORMAT(Amount, 0, 0);
                                LineAmount_txt := FORMAT("Line Amount", 0, 0);
                                // >>> 2111-62487 13-08-2018 SMS
                                //QtyBacklog_txt := FORMAT(QuantityInBacklog,0,0);
                                QtyBacklog_txt := FORMAT("Backlog Quantity", 0, 0);
                                // <<< 2111-62487 13-08-2018 SMS
                                Discount_txt := FORMAT("Line Discount %", 0, 0);
                            end else begin
                                Quantity_txt := '';
                                UnitPrice_txt := '';
                                Amount_txt := '';
                                LineAmount_txt := '';
                                QtyBacklog_txt := '';
                                Discount_txt := '';
                            end;
                            // <<< 2111-34887 23-12-2015 FS

                            // >>> 2111-33644 14-03-2016 RM
                            if PrepayInvoice and ShowArchivLine then begin
                                PrepaymentTotalLineAmount += "Sales Invoice Line"."Line Amount";
                                if LastInvLineNo > "Sales Invoice Line"."Line No." then begin
                                    CurrReport.SKIP;
                                end;
                                "Line Amount" := PrepaymentTotalLineAmount;
                                "Unit Price" := PrepaymentTotalLineAmount;
                            end;
                            // <<< 2111-33644 14-03-2016 RM

                            // >>> 2111-35848 14-04-2016 DB
                            if not "No Payment Discount" then begin
                                LinePaymentDisc := "Amount Including VAT" * ("Sales Invoice Header"."Payment Discount %" / 100);
                                TotalPaymentDisc += LinePaymentDisc;
                            end;
                            // <<< 2111-35848 14-04-2016 DB
                        end;

                        trigger OnPostDataItem();
                        var
                            GeneralReportManagement: Codeunit "General Report Management";
                        begin
                            // >>> 2111-34115 26-01-2016 MMA
                            // Item Tracking:
                            if ShowLotSN then begin
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(true);
                                TrackingSpecCount :=
                                    ItemTrackingDocMgt.RetrieveDocumentItemTracking(TrackingSpecBuffer, "Sales Invoice Header"."No.", DATABASE::"Sales Invoice Header", 0);
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(false);

                                // >>> 2111-51001 21-08-2017 MW
                                GeneralReportManagement.FillExpirationDateInTrackSpec(TrackingSpecBuffer);
                                // <<< 2111-51001 21-08-2017 MW
                            end;
                            // <<< 2111-34115 26-01-2016 MMA
                        end;

                        trigger OnPreDataItem();
                        var
                            "---NAI---": Integer;
                            SalesInvLine: Record "Sales Invoice Line";
                        begin
                            VATAmountLine.DELETEALL;
                            SalesShipmentBuffer.RESET;
                            SalesShipmentBuffer.DELETEALL;
                            FirstValueEntryNo := 0;
                            MoreLines := FIND('+');
                            while MoreLines and (Description = '') and ("No." = '') and (Quantity = 0) and (Amount = 0) do
                                MoreLines := NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SETRANGE("Line No.", 0, "Line No.");
                            CurrReport.CREATETOTALS("Line Amount", Amount, "Amount Including VAT", "Inv. Discount Amount");

                            // >>> 2111-49001 15-02-2017 RM
                            LastInvLineNo := 0;
                            if PrepayInvoice and ShowArchivLine then begin
                                "Sales Invoice Line".SETFILTER(Type, '<>%1', "Sales Invoice Line".Type::" ");
                                SalesInvLine.RESET;
                                SalesInvLine.SETRANGE("Document No.", "Sales Invoice Header"."No.");
                                SalesInvLine.SETFILTER(Type, '<>%1', SalesInvLine.Type::" ");
                                if SalesInvLine.FINDLAST then begin
                                    LastInvLineNo := SalesInvLine."Line No.";
                                end;
                                // >>> 2111-33644 14-03-2016 RM
                                PrepaymentTotalLineAmount := 0;
                                // <<< 2111-33644 14-03-2016 RM
                            end;
                            // <<< 2111-49001 15-02-2017 RM

                            // >>> 2111-49001 15-02-2017 RM
                            ShowPrepayAndInvoice := false;
                            // <<< 2111-49001 15-02-2017 RM

                            // >>> 2111-25088 02-09-2014 LK
                            "13bLineFound" := false;
                            // <<< 2111-25088 02-09-2014 LK
                        end;
                    }
                    dataitem(VATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmountLineVATBase; VATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Invoice Line".GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineVATAmount; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineLineAmount; VATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; VATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscountAmt; (VATAmountLine."Invoice Discount Amount" * -1))
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineVAT; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATAmntSpecificCaption; VATAmntSpecificCaptionLbl)
                        {
                        }
                        column(InvDiscBaseAmtCaption; InvDiscBaseAmtCaptionLbl)
                        {
                        }
                        column(LineAmountCaption; LineAmountCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(
                              VATAmountLine."Line Amount", VATAmountLine."Inv. Disc. Base Amount",
                              VATAmountLine."Invoice Discount Amount", VATAmountLine."VAT Base", VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATClauseEntryCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATClauseVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATClauseCode; VATAmountLine."VAT Clause Code")
                        {
                        }
                        column(VATClauseDescription; VATClause.Description)
                        {
                        }
                        column(VATClauseDescription2; VATClause."Description 2")
                        {
                        }
                        column(VATClauseAmount; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATClausesCaption; VATClausesCap)
                        {
                        }
                        column(VATClauseVATIdentifierCaption; VATIdentifierCaptionLbl)
                        {
                        }
                        column(VATClauseVATAmtCaption; VATAmountCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                            if not VATClause.GET(VATAmountLine."VAT Clause Code") then
                                CurrReport.SKIP;
                            VATClause.TranslateDescription("Sales Invoice Header"."Language Code");
                        end;

                        trigger OnPreDataItem();
                        begin
                            CLEAR(VATClause);
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VatCounterLCY; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineVAT_VatCounterLCY; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier_VatCounterLCY; VATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                            VALVATBaseLCY :=
                              VATAmountLine.GetBaseLCY(
                                "Sales Invoice Header"."Posting Date", "Sales Invoice Header"."Currency Code",
                                "Sales Invoice Header"."Currency Factor");
                            VALVATAmountLCY :=
                              VATAmountLine.GetAmountLCY(
                                "Sales Invoice Header"."Posting Date", "Sales Invoice Header"."Currency Code",
                                "Sales Invoice Header"."Currency Factor");
                        end;

                        trigger OnPreDataItem();
                        begin
                            if (not GLSetup."Print VAT specification in LCY") or
                               ("Sales Invoice Header"."Currency Code" = '')
                            then
                                CurrReport.BREAK;

                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VALVATBaseLCY, VALVATAmountLCY);

                            if GLSetup."LCY Code" = '' then
                                VALSpecLCYHeader := Text007 + Text008
                            else
                                VALSpecLCYHeader := Text007 + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Invoice Header"."Posting Date", "Sales Invoice Header"."Currency Code", 1);
                            CalculatedExchRate := ROUND(1 / "Sales Invoice Header"."Currency Factor" * CurrExchRate."Exchange Rate Amount", 0.000001);
                            VALExchRate := STRSUBSTNO(Text009, CalculatedExchRate, CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem("13b"; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(TaxSetup_13bInvoiceText; TaxSetup."13b-Invoice Text")
                        {
                        }

                        trigger OnPreDataItem();
                        var
                            GenBusinessPostingGroup: Record "Gen. Business Posting Group";
                        begin
                            // >>> 2111-25088 02-09-2014 LK
                            if not "13bLineFound" then
                                CurrReport.BREAK;

                            Custprint13b := false;
                            if GenBusinessPostingGroup.GET(Cust."Gen. Bus. Posting Group") then begin
                                if GenBusinessPostingGroup."Show 13b-Taxation on Document" then begin
                                    Custprint13b := true;
                                end;
                            end;

                            if not Custprint13b then
                                CurrReport.BREAK;

                            "13b".SETRANGE(Number, 1, 1);
                            // <<< 2111-25088 02-09-2014 LK
                        end;
                    }
                    dataitem(Total; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesInvHdr; "Sales Invoice Header"."Sell-to Customer No.")
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShiptoAddressCaption; ShiptoAddrCaptionLbl)
                        {
                        }
                        column(SelltoCustNo_SalesInvHdrCaption; "Sales Invoice Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowShippingAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(PaymentAndInvoice; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(ShowPrepayAndInvoice; ShowPrepayAndInvoice)
                        {
                        }
                        dataitem(PaymentTotal; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(SalesInvoiceLineTotal__Document_No__; TempSalesInvoiceLineTotal."Document No.")
                            {
                            }
                            column(SalesInvoiceLineTotal__Posting_Date_; FORMAT(TempSalesInvoiceLineTotal."Posting Date"))
                            {
                            }
                            column(SalesInvoiceLineTotal_Amount; TempSalesInvoiceLineTotal.Amount)
                            {
                            }
                            column(SalesInvoiceLineTotal_Amount___SalesInvoiceLineTotal__Amount_Including_VAT_; TempSalesInvoiceLineTotal."Amount Including VAT" - TempSalesInvoiceLineTotal.Amount)
                            {
                            }
                            column(SalesInvoiceLineTotal__Amount_Including_VAT_; TempSalesInvoiceLineTotal."Amount Including VAT")
                            {
                            }
                            column(PaymentTotal_Number; Number)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if Number = 1 then
                                    TempSalesInvoiceLineTotal.FINDSET
                                else
                                    TempSalesInvoiceLineTotal.NEXT;
                                // <<< 2111-49001 15-02-2017 RM
                            end;

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if not ShowLines then
                                    CurrReport.BREAK;

                                TempSalesInvoiceLineTotal.RESET;
                                if TempSalesInvoiceLineTotal.ISEMPTY then
                                    CurrReport.BREAK;

                                PaymentTotal.SETRANGE(Number, 1, TempSalesInvoiceLineTotal.COUNT);
                                // <<< 2111-49001 15-02-2017 RM
                            end;
                        }
                        dataitem(InvoiceTotal; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(SalesInvoiceLineTotal2__Amount_Including_VAT_; TempSalesInvoiceLineTotal2."Amount Including VAT")
                            {
                            }
                            column(SalesInvoiceLineTotal2_Amount___SalesInvoiceLineTotal2__Amount_Including_VAT_; TempSalesInvoiceLineTotal2."Amount Including VAT" - TempSalesInvoiceLineTotal2.Amount)
                            {
                            }
                            column(SalesInvoiceLineTotal2_Amount; TempSalesInvoiceLineTotal2.Amount)
                            {
                            }
                            column(SalesInvoiceLineTotal2__Posting_Date_; FORMAT(TempSalesInvoiceLineTotal2."Posting Date"))
                            {
                            }
                            column(SalesInvoiceLineTotal2__Document_No__; TempSalesInvoiceLineTotal2."Document No.")
                            {
                            }
                            column(InvoiceTotal_Number; Number)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if Number = 1 then
                                    TempSalesInvoiceLineTotal2.FINDSET
                                else
                                    TempSalesInvoiceLineTotal2.NEXT;
                                // <<< 2111-49001 15-02-2017 RM
                            end;

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if not ShowLines then
                                    CurrReport.BREAK;
                                TempSalesInvoiceLineTotal2.RESET;
                                if not TempSalesInvoiceLineTotal2.ISEMPTY then
                                    TempSalesInvoiceLineTotal2.DELETEALL;

                                CreateInvoiceTotal;

                                TempSalesInvoiceLineTotal2.RESET;
                                if TempSalesInvoiceLineTotal2.ISEMPTY then
                                    CurrReport.BREAK;

                                InvoiceTotal.SETRANGE(Number, 1, TempSalesInvoiceLineTotal2.COUNT);
                                // <<< 2111-49001 15-02-2017 RM
                            end;
                        }
                    }
                    dataitem(ItemTrackingLine; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(TrackingSpecBufferNo; TrackingSpecBuffer."Item No.")
                        {
                        }
                        column(TrackingSpecBufferDesc; TrackingSpecBuffer.Description)
                        {
                        }
                        column(TrackingSpecBufferLotNo; TrackingSpecBuffer."Lot No.")
                        {
                        }
                        column(TrackingSpecBufferSerNo; TrackingSpecBuffer."Serial No.")
                        {
                        }
                        column(TrackingSpecBufferQty; TrackingSpecBuffer."Quantity (Base)")
                        {
                        }
                        column(TrackingSpecBufferExpDate; TrackingSpecBuffer."Expiration Date")
                        {
                        }
                        column(ShowTotal; ShowTotal)
                        {
                        }
                        column(ShowGroup; ShowGroup)
                        {
                        }
                        column(QuantityCaption; QuantityCaptionLbl)
                        {
                        }
                        column(SerialNoCaption; SerialNoCaptionLbl)
                        {
                        }
                        column(LotNoCaption; LotNoCaptionLbl)
                        {
                        }
                        column(DescriptionCaption; DescriptionCaptionLbl)
                        {
                        }
                        column(NoCaption; NoCaptionLbl)
                        {
                        }
                        column(ExpirationDate; ExpirationDateLbl)
                        {
                        }
                        dataitem(TotalItemTracking; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                            column(Quantity1; TotalQty)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            if Number = 1 then
                                TrackingSpecBuffer.FINDSET
                            else
                                TrackingSpecBuffer.NEXT;

                            ShowTotal := false;
                            if ItemTrackingAppendix.IsStartNewGroup(TrackingSpecBuffer) then
                                ShowTotal := true;

                            ShowGroup := false;
                            if (TrackingSpecBuffer."Source Ref. No." <> OldRefNo) or
                               (TrackingSpecBuffer."Item No." <> OldNo)
                            then begin
                                OldRefNo := TrackingSpecBuffer."Source Ref. No.";
                                OldNo := TrackingSpecBuffer."Item No.";
                                TotalQty := 0;
                            end else
                                ShowGroup := true;
                            TotalQty += TrackingSpecBuffer."Quantity (Base)";
                            // <<< 2111-49001 15-02-2017 RM
                        end;

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            if TrackingSpecCount = 0 then
                                CurrReport.BREAK;
                            CurrReport.NEWPAGE;
                            SETRANGE(Number, 1, TrackingSpecCount);
                            TrackingSpecBuffer.SETCURRENTKEY("Source ID", "Source Type", "Source Subtype", "Source Batch Name",
                              "Source Prod. Order Line", "Source Ref. No.");
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            NAIExtPostText.RESET;
                            NAIExtPostText.SETRANGE("Table ID", DATABASE::"Sales Invoice Header");
                            NAIExtPostText.SETRANGE("Document No.", "Sales Invoice Header"."No.");
                            NAIExtPostText.SETRANGE("Assigned for", NAIExtPostText."Assigned for"::"Posted Invoice");
                            NAIExtPostText.SETRANGE("Text Position", NAIExtPostText."Text Position"::Posttext);
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                    dataitem(Totals; "Integer")
                    {
                        DataItemLinkReference = CopyLoop;
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(TotalNetAmount; TotalAmount)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountIncludingVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalVATAmount; TotalAmountVAT)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalInvoiceDiscountAmount; TotalInvDiscAmount)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(InvoiceTotalSubTotal; TotalSubTotal)
                        {
                            AutoFormatExpression = "Sales Invoice Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalSubTotalMinusInvoiceDiscount; TotalSubTotal + TotalInvDiscAmount)
                        {
                        }
                        column(TotalVATAmountText; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalExcludingVATText; TotalExclVATText)
                        {
                        }
                        column(TotalIncludingVATText; TotalInclVATText)
                        {
                        }
                        column(InvoiceTotalText; TotalText)
                        {
                        }
                    }

                    trigger OnPreDataItem();
                    begin
                        // >>> 2111-34115 26-01-2016 MMA
                        // Item Tracking:
                        if ShowLotSN then begin
                            TrackingSpecCount := 0;
                            OldRefNo := 0;
                            ShowGroup := false;
                        end;
                        // <<< 2111-34115 26-01-2016 MMA
                    end;
                }

                trigger OnAfterGetRecord();
                begin
                    if Number > 1 then begin
                        CopyText := Text003;
                        OutputNo += 1;
                    end;
                    CurrReport.PAGENO := 1;

                    TotalSubTotal := 0;
                    TotalInvDiscAmount := 0;
                    TotalAmount := 0;
                    TotalAmountVAT := 0;
                    TotalAmountInclVAT := 0;
                    TotalPaymentDiscOnVAT := 0;
                    // >>> 2111-49001 15-02-2017 RM
                    PrePaymentLineNo := 10000;
                    TempSalesInvoiceLineTotal.RESET;
                    if not TempSalesInvoiceLineTotal.ISEMPTY then
                        TempSalesInvoiceLineTotal.DELETEALL;
                    // <<< 2111-49001 15-02-2017 RM

                    // >>> 2111-35848 15-04-2016 DB
                    TotalPaymentDisc := 0;
                    // <<< 2111-35848 15-04-2016 DB
                end;

                trigger OnPostDataItem();
                begin
                    if not CurrReport.PREVIEW then
                        SalesInvCountPrinted.RUN("Sales Invoice Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + Cust."Invoice Copies" + 1;
                    if NoOfLoops <= 0 then
                        NoOfLoops := 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                "---NAI---": Integer;
                SalesHeaderArchive: Record "Sales Header Archive";
                ShiptoAddress: Record "Ship-to Address";
                ShipBillCustomer: Record Customer;
                GeneralSalesMgmt: Codeunit "General Sales Management";
                GeneralReportMgmt: Codeunit "General Report Management";
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");
                // >>> 2111-49001 15-02-2017 RM
                CompanyInfo.GET;
                // <<< 2111-49001 15-02-2017 RM

                // >>> 2111-49001 15-02-2017 RM
                PrepayInvoice := "Prepayment Invoice";
                // <<< 2111-49001 15-02-2017 RM
                // >>> 15.10.19 MMC OP
                User.RESET;
                User.SETRANGE("User Name", "Sales Invoice Header"."User ID");
                IF NOT User.FINDFIRST THEN
                    User.INIT;

                Begin
                    Payline.INIT;
                    Payline.SetRange("Sales/Purchase", 1);
                    Payline.setrange(Type, 2);
                    Payline.SetRange(Code, "Sales Invoice Header"."No.");
                    Payline.FindFirst();
                    DueDateNew := Payline."Due Date";


                END;

                salesshipmentheader.init;
                salesshipmentheader.setrange("Order No.", "Order No.");
                if salesshipmentheader.FindFirst() then begin
                    LogLS := salesshipmentheader."Logistician Shipment No."
                End;





                //<<< 15.10.19 MMC OP

                // >>> 2111-49001 15-02-2017 RM
                ShowArchivLine := false;
                if PrepayInvoice then begin
                    SalesHeaderArchive.RESET;
                    SalesHeaderArchive.SETRANGE("Attached to Prepayment No.", "No.");
                    if SalesHeaderArchive.FINDFIRST then begin
                        ShowArchivLine := true;
                    end;
                end;
                // <<< 2111-49001 15-02-2017 RM

                // >>> 2111-49001 15-02-2017 RM
                if "Prepayment Invoice" then begin
                    OrderTitle := FIELDCAPTION("Prepayment Order No.");
                    OrderTitleNo := "Prepayment Order No.";
                end else begin
                    OrderTitle := FIELDCAPTION("Order No.");
                    OrderTitleNo := "Order No.";
                end;
                // <<< 2111-49001 15-02-2017 RM

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 15-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, MAXSTRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 15-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 15-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 15-02-2017 RM
                end;

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                if "Order No." = '' then
                    OrderNoText := ''
                else
                    OrderNoText := FIELDCAPTION("Order No.");

                // >>> 2111-49001 15-02-2017 RM
                if ShowOfficeSalesperson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 15-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 15-02-2017 RM
                end;
                // <<< 2111-49001 15-02-2017 RM

                // >>> 2111-62379 20-06-2018 BD
                if "Office Sales Person" <> '' then begin
                    OfficeSalesperson.GET("Office Sales Person");
                end else begin
                    OfficeSalesperson.INIT;
                end;
                // <<< 2111-62379 20-06-2018 BD

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                if "VAT Registration No." = '' then
                    VATNoText := ''
                else
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                if "Currency Code" = '' then begin
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006, GLSetup."LCY Code");
                end else begin
                    TotalText := STRSUBSTNO(Text001, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006, "Currency Code");
                end;
                FormatAddr.SalesInvBillTo(CustAddr, "Sales Invoice Header");
                if not Cust.GET("Bill-to Customer No.") then
                    CLEAR(Cust);

                if "Payment Terms Code" = '' then
                    PaymentTerms.INIT
                else begin
                    PaymentTerms.GET("Payment Terms Code");
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                end;
                if "Shipment Method Code" = '' then
                    ShipmentMethod.INIT
                else begin
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                end;
                FormatAddr.SalesInvShipTo(ShipToAddr, CustAddr, "Sales Invoice Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                for i := 1 to ARRAYLEN(ShipToAddr) do
                    if ShipToAddr[i] <> CustAddr[i] then
                        ShowShippingAddr := true;

                if LogInteraction then
                    if not CurrReport.PREVIEW then begin
                        if "Bill-to Contact No." <> '' then
                            SegManagement.LogDocument(
                              4, "No.", 0, 0, DATABASE::Contact, "Bill-to Contact No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '')
                        else
                            SegManagement.LogDocument(
                              4, "No.", 0, 0, DATABASE::Customer, "Bill-to Customer No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '');
                    end;

                // >>> 2111-23178 31-07-2014 UQ
                ShowDiscount := IsDiscount("Sales Invoice Header");
                // <<< 2111-23178 31-07-2014 UQ

                // >>> 2111-34381 28-12-2015 DB
                ShippingInformationMgt.GetExtendedShippingInfo("Sales Invoice Header", SumNetWeight, SumGrossWeight, ShowNetWeight, ShowGrossWeight);
                // <<< 2111-34381 28-12-2015 DB

                // >>> 2111-49109 17-02-2017 MW
                ShipmentGLN := '';
                InvoiceGLN := '';
                ShippingAddressText := '';

                GeneralSalesMgmt.GetSalesShptLineFromInvoiceHeader("Sales Invoice Header", TempSalesShptLine);

                if "Sales Invoice Header"."Ship-to Code" <> '' then begin
                    if ShiptoAddress.GET("Sales Invoice Header"."Sell-to Customer No.", "Sales Invoice Header"."Ship-to Code") then begin
                        ShipmentGLN := ShiptoAddress.GLN;
                    end;

                end else begin
                    if ShipBillCustomer.GET("Sales Invoice Header"."Sell-to Customer No.") then begin
                        ShipmentGLN := ShipBillCustomer.GLN;
                    end;
                end;

                if "Sales Invoice Header"."Bill-to Customer No." <> '' then begin
                    if ShipBillCustomer.GET("Sales Invoice Header"."Bill-to Customer No.") then begin
                        InvoiceGLN := ShipBillCustomer.GLN;
                    end;
                end;

                for i := 1 to ARRAYLEN(ShipToAddr) do begin
                    if ShipToAddr[i] <> '' then begin
                        if i = 1 then begin
                            ShippingAddressText := ShipToAddr[i];
                        end else begin
                            ShippingAddressText := ShippingAddressText + ', ' + ShipToAddr[i];
                        end;
                    end;
                end;
                // <<< 2111-49109 17-02-2017 MW

                // >>> 2111-50313 23-05-2017 HD
                //if not Associationgr.GET(Associationgr.Type::Customer, Cust."OPP Association No.") then begin
                //    Associationgr.INIT;
                //end;
                // <<< 2111-50313 23-05-2017 HD

                // >>> 2111-62234 13-03-2018 RM
                ShowSellToInfo := not GeneralReportMgmt.SalesInvHeaderSellToEqualsBillTo("Sales Invoice Header");
                // <<< 2111-62234 13-03-2018 RM
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                    }
                    field(ShowGLAccount; ShowGLAccount)
                    {
                        CaptionML = DEU = 'Sachkonto anzeigen',
                                    ENU = 'Show G/L Account';
                    }
                    field(ShowLotSN; ShowLotSN)
                    {
                        CaptionML = DEU = 'Chargennr./Seriennr. Anhang anzeigen',
                                    ENU = 'Show Serial/Lot Number Appendix';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                    }
                    field(DisplayAsmInformation; DisplayAssemblyInformation)
                    {
                        CaptionML = DEU = 'Montagekomponenten anzeigen',
                                    ENU = 'Show Assembly Components';
                    }
                    field(ShowOfficeSalesperson; ShowOfficeSalesperson)
                    {
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
        end;

        trigger OnOpenPage();
        var
            NAISetup: Record "NAI - Setup";
        begin
            InitLogInteraction;
            LogInteractionEnable := LogInteraction;

            // >>> 2111-37251 29-08-2016 SMS
            if NAISetup.GET then begin
                ShowLotSN := NAISetup."Print Tracking On Invoice";
            end;
            // <<< 2111-37251 29-08-2016 SMS
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        GLSetup.GET;
        CompanyInfo.GET;
        SalesSetup.GET;
        // >>> 2111-50022 08-12-2017 MY
        CompanyInfo.VerifyAndSetPaymentInfo;
        // <<< 2111-50022 08-12-2017 MY
        // >>> 2111-25088 02-09-2014 LK
        TaxSetup.GET;
        // <<< 2111-25088 02-09-2014 LK
    end;

    trigger OnPreReport();
    begin
        if not CurrReport.USEREQUESTPAGE then
            InitLogInteraction;
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'Total %1', ENU = 'Total %1';
        Text002: TextConst DEU = 'Total %1 inkl. MwSt.', ENU = 'Total %1 Incl. VAT', ITA = 'Importo %1 totale incl. IVA';
        Text003: TextConst DEU = 'KOPIE', ENU = 'COPY';
        Text004: TextConst DEU = 'Rechnung %1 %2', ENU = 'Invoice %1 %2', ITA = 'Fattura %1 %2';
        Text005: TextConst DEU = 'Seite %1', ENU = 'Page %1';
        Text006: TextConst DEU = 'Total %1 ohne MwSt.', ENU = 'Total %1 Excl. VAT', ITA = 'Importo %1 totale escl. IVA';
        GLSetup: Record "General Ledger Setup";
        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo2: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        Cust: Record Customer;
        VATAmountLine: Record "VAT Amount Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        RespCenter: Record "Responsibility Center";
        Language: Record Language;
        CurrExchRate: Record "Currency Exchange Rate";
        TempPostedAsmLine: Record "Posted Assembly Line" temporary;
        VATClause: Record "VAT Clause";
        SalesInvCountPrinted: Codeunit "Sales Inv.-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        SalesShipmentBuffer: Record "Sales Shipment Buffer" temporary;
        User: Record User;
        PostedShipmentDate: Date;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        OrderNoText: Text[80];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        NextEntryNo: Integer;
        FirstValueEntryNo: Integer;
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        LogInteraction: Boolean;
        DisplayAssemblyInformation: Boolean;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        VALSpecLCYHeader: Text[80];
        Text007: TextConst DEU = 'MwSt.-Betrag Spezifikation in ', ENU = 'VAT Amount Specification in ';
        Text008: TextConst DEU = 'Landeswährung', ENU = 'Local Currency', ITA = 'Valuta';
        VALExchRate: Text[50];
        Text009: TextConst DEU = 'Wechselkurs: %1/%2', ENU = 'Exchange rate: %1/%2';
        CalculatedExchRate: Decimal;
        OutputNo: Integer;
        TotalSubTotal: Decimal;
        TotalAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        TotalAmountVAT: Decimal;
        TotalInvDiscAmount: Decimal;
        TotalPaymentDiscOnVAT: Decimal;
        [InDataSet]
        LogInteractionEnable: Boolean;
        UnitPriceVar: Text[20];
        LineDiscVar: Text[20];
        LineAmtVar: Text[20];
        Text010: TextConst DEU = 'Abschlagsrechnung %1 %2', ENU = 'Prepayment Invoice %1 %2';
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        HomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.', ITA = 'Ns. num. di partita IVA';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Lieferantennummer', ENU = 'Account No.', ITA = 'Ns cod. fornitore';
        SalesInvDueDateCaptionLbl: TextConst DEU = 'Fälligkeitsdatum', ENU = 'Due Date', ITA = 'Data scadenza';
        InvNoCaptionLbl: TextConst DEU = 'Rechnungsnr.', ENU = 'Invoice No.', ITA = 'Fattura no.';
        SalesInvPostingDateCptnLbl: TextConst DEU = 'Buchungsdatum', ENU = 'Posting Date';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        UnitPriceCaptionLbl: TextConst DEU = 'VK-Preis', ENU = 'Unit Price', ITA = 'prezzo unit.';
        SalesInvLineDiscCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Discount %', ITA = 'sconto %';
        AmountCaptionLbl: TextConst DEU = 'Betrag', ENU = 'Amount', ITA = 'Importo';
        VATClausesCap: TextConst DEU = 'MwSt.-Klausel', ENU = 'VAT Clause';
        PostedShipmentDateCaptionLbl: TextConst DEU = 'Versand-   datum', ENU = 'Shipment Date', ITA = 'Data di spedizione';
        SubtotalCaptionLbl: TextConst DEU = 'Zwischensumme', ENU = 'Subtotal';
        LineAmtAfterInvDiscCptnLbl: TextConst DEU = 'Skonto auf MwSt.', ENU = 'Payment Discount on VAT';
        ShipmentCaptionLbl: TextConst DEU = 'Warenausgang', ENU = 'Shipment';
        LineDimCaptionLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        VATAmntSpecificCaptionLbl: TextConst DEU = 'MwSt.-Betrag - Spezifikation', ENU = 'VAT Amount Specification', ITA = 'Identificazione importo IVA';
        InvDiscBaseAmtCaptionLbl: TextConst DEU = 'Rechnungsrab.-Bem.grundlage', ENU = 'Invoice Discount Base Amount', ITA = 'base sovrapprezzo';
        LineAmountCaptionLbl: TextConst DEU = 'Zeilenbetrag', ENU = 'Line Amount', ITA = 'Importo Riga';
        ShiptoAddrCaptionLbl: TextConst DEU = 'Lieferadresse', ENU = 'Ship-to Address', ITA = 'Indrizzo di consegna';
        EMailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        InvDiscountAmtCaptionLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Invoice Discount Amount', ITA = 'sovrapprezzo';
        VATCaptionLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %', ITA = 'IVA %';
        VATBaseCaptionLbl: TextConst DEU = 'MwSt.-Bemessungsgrundlage', ENU = 'VAT Base', ITA = 'Imponibile';
        VATAmtCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount', ITA = 'Imposta';
        VATIdentifierCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier', ITA = 'identificazione IVA';
        TotalCaptionLbl: TextConst DEU = 'Gesamt', ENU = 'Total', ITA = 'totale';
        PaymentTermsDescCaptionLbl: TextConst DEU = 'Zahlungsbedingungen', ENU = 'Payment Terms', ITA = 'Cond. di pag.';
        ShptMethodDescCaptionLbl: TextConst DEU = 'Lieferbedingung', ENU = 'Shipment Method', ITA = 'Mod. di consegna';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date', ITA = 'Data fattura';
        OurAccountCaption: TextConst DEU = 'Lieferantennummer', ENU = 'Account No.';
        OfferCaption: TextConst DEU = 'Angebotnr.', ENU = 'Offerno.', ITA = 'Offerta n.';
        "---NAI---": Integer;
        TempSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        TempSalesCrMemoHeader: Record "Sales Cr.Memo Header" temporary;
        SalesInvoiceLine: Record "Sales Invoice Line";
        SalesCrMemoLine: Record "Sales Cr.Memo Line";
        TempSalesInvoiceLineTotal: Record "Sales Invoice Line" temporary;
        TempSalesInvoiceLineTotal2: Record "Sales Invoice Line" temporary;
        TaxSetup: Record "Tax and Freight Setup";
        //Associationgr: Record "OPP Association";
        OfficeSalesperson: Record "Salesperson/Purchaser";
        ShippingInformationMgt: Codeunit "Shipping Information Mgt.";
        "13bLineFound": Boolean;
        Custprint13b: Boolean;
        [InDataSet]
        ShowGLAccount: Boolean;
        ShowLotSN: Boolean;
        TrackingSpecBuffer: Record "Tracking Specification" temporary;
        ItemTrackingDocMgt: Codeunit "Item Tracking Doc. Management";
        ItemTrackingAppendix: Report "Item Tracking Appendix";
        TrackingSpecCount: Integer;
        OldRefNo: Integer;
        OldNo: Code[20];
        ShowGroup: Boolean;
        ShowTotal: Boolean;
        TotalQty: Decimal;
        PrePaymentLineNo: Integer;
        [InDataSet]
        PrepayInvoice: Boolean;
        [InDataSet]
        ShowLines: Boolean;
        [InDataSet]
        HideFields: Boolean;
        SalesInvNo: Code[20];
        ShowOfficeSalesperson: Boolean;
        ShowArchivLine: Boolean;
        LastInvLineNo: Integer;
        CompanyAddressLine: Text[160];
        OrderTitle: Text[30];
        OrderTitleNo: Code[20];
        ShowPrepayAndInvoice: Boolean;
        ShowDiscount: Boolean;
        ItemTariffNo: Code[20];
        ItemSerialNo: Code[20];
        ItemLotNo: Code[20];
        ItemExpirationDate: Date;
        ItemQuantity: Decimal;
        SkipInvLine: Boolean;
        InvLineNoToSkip: Integer;
        HideInvoiceLine: Boolean;
        CustomerItemNo: Code[20];
        SumNetWeight: Decimal;
        SumGrossWeight: Decimal;
        ShowNetWeight: Boolean;
        ShowGrossWeight: Boolean;
        PrepaymentTotalLineAmount: Decimal;
        LinePaymentDisc: Decimal;
        TotalPaymentDisc: Decimal;
        ShipmentGLN: Text;
        InvoiceGLN: Text;
        ShippingAddressText: Text;
        Quantity_txt: Text[15];
        UnitPrice_txt: Text[15];
        Amount_txt: Text[15];
        LineAmount_txt: Text[15];
        QtyBacklog_txt: Text[15];
        Discount_txt: Text[15];
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        VATPercentageCaptionLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %', ITA = 'IVA %';
        PaymentDiscountLbl: TextConst DEU = 'Skonto', ENU = 'Payment Discount';
        Text5000000: TextConst DEU = 'der %1', ENU = 'of %1';
        Text5244785: TextConst DEU = 'erhaltene Anzahlung (s. Tabelle Abschlagszahlung)', ENU = 'Payment received (see table Prepayments)';
        Text5244793: TextConst DEU = 'Abschlagszahlungen', ENU = 'Prepayments';
        Text5244794: TextConst DEU = 'Nummer', ENU = 'Number';
        Text5244795: TextConst DEU = 'Datum', ENU = 'Date';
        Text5244796: TextConst DEU = 'Betrag Netto', ENU = 'Netto Amount';
        Text5244797: TextConst DEU = 'Betrag Brutto', ENU = 'Gross Amount';
        Text5244798: TextConst DEU = 'Umsatzsteuer', ENU = 'VAT';
        Text5244799: TextConst DEU = 'Abgerechnete Abschlagszahlungen', ENU = 'Settled Payments';
        Text5244800: TextConst DEU = 'Offener Saldo', ENU = 'Open Balance';
        Text5244801: TextConst DEU = 'Abschlagsrechnung zu: ', ENU = 'Prepayment Invoice to: ';
        Text5244802: TextConst DEU = 'Auftrag Nr. ', ENU = 'Sales Order No. ';
        Text5244803: TextConst DEU = 'Art', ENU = 'Type';
        Text5244804: TextConst DEU = 'Abschlagsrechnung', ENU = 'Prepayment Invoice';
        Text5244805: TextConst DEU = 'Abschlagsgutschrift', ENU = 'Prepayment Cr. Memo';
        Text5244806: TextConst DEU = 'Rechnung', ENU = 'Invoice', ITA = 'Fattura no.';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        SalesInvHdrBilltoCustomerNoCaptionLbl: TextConst DEU = 'Kundennr.', ENU = 'Customer No.', ITA = 'Vs codice cliente';
        SalesInvLinePositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.';
        DiscountCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Discount %', ITA = 'sconto %';
        VATAmountCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        UOM_SalesInvLineCaptionLbl: TextConst DEU = 'Einheit', ENU = 'Unit', FRA = 'Unité', NLD = 'Eenheid', ITA = 'Unità di misura';
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ';
        SumNetWeightLbl: TextConst DEU = 'Summe Netto-Gewicht', ENU = 'Sum Net Weight';
        SumGrossWeightLbl: TextConst DEU = 'Summe Brutto-Gewicht', ENU = 'Sum Gross Weight';
        ExpirationDateCaptionLbl: TextConst DEU = 'Ablaufdatum', ENU = 'Expiration Date';
        SerialNoCaptionLbl: TextConst DEU = 'Seriennr.', ENU = 'Serial No.';
        LotNoCaptionLbl: TextConst DEU = 'Chargennr.', ENU = 'Lot No.';
        DescriptionCaptionLbl: TextConst DEU = 'Beschreibung', ENU = 'Description';
        QuantityCaptionLbl: TextConst DEU = 'Menge', ENU = 'Quantity';
        NoCaptionLbl: TextConst DEU = 'Nr.', ENU = 'No.', ITA = 'Cod.Art';
        ExpirationDateLbl: TextConst DEU = 'Ablaufdatum', ENU = 'Expiration Date';
        VATIdentCaptionLbl: TextConst DEU = 'MwSt.-Kennz.', ENU = 'VAT Ident.';
        SenderGLNLbl: TextConst DEU = 'Absender GLN', ENU = 'Sender GLN';
        ShipmentGLNlbl: TextConst DEU = 'WE-GLN', ENU = 'DP-GLN';
        InvoiceGLNLbl: TextConst DEU = 'RG-GLN', ENU = 'IV-GLN';
        SalesShipmentNoLbl: TextConst DEU = 'Lieferscheinnummer', ENU = 'Shipment Number', ITA = 'DDT n.';
        SenderLbl: TextConst DEU = 'Absender', ENU = 'Sender';
        OrderDateLbl: TextConst DEU = 'Auftragsdatum', ENU = 'Order Date';
        ShipmentAddressLbl: TextConst DEU = 'Lieferanschrift', ENU = 'Shipment Address';
        ReferenceLbl: TextConst DEU = 'Referenz', ENU = 'Reference';
        ShipmentDateLbl: TextConst DEU = 'Lieferdatum', ENU = 'Shipment Date', ITA = 'Data di spedizione';
        VATNoLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT No.';
        ItemGTIN: Code[14];
        CUSTGLNtxt: TextConst DEU = 'DUNS', ENU = 'DUNS';
        GTINLbl: TextConst DEU = 'EAN: ', ENU = 'GTIN: ';
        UserLbl: TextConst DEU = 'Sachbearbeiter', ENU = 'Executive Officer', ITA = 'Addetto';
        TransportReasonLbl: TextConst DEU = 'Transportgrund', ENU = 'Transport Reason', ITA = 'Causale Del Trasporto';
        TransportReasontxt: TextConst DEU = 'Verakuf', ENU = 'Sales', ITA = 'Vendita';
        QtyABvar: text[20];
        QtyAB: Text[20];
        ShowSellToInfo: Boolean;

        UPV: Text[20];
        LDV: Text[20];
        SAV: Text[20];
        Payline: record "Posted Payment Lines";
        DueDateNew: Date;
        DueDatetxt: TextConst DEU = 'Fälligkeitsdatum', ENU = 'Due Date', ITA = 'Data scadenza';
        Fiscaltxt: textconst DEU = 'Steuer Code', ENU = 'Fiscal Code', ITA = 'Codice Fiscale';

        salesshipmentheader: Record "Sales Shipment Header";

        LogLS: code[10];

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractTmplCode(4) <> '';
    end;

    procedure FindPostedShipmentDate(): Date;
    var
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesShipmentBuffer2: Record "Sales Shipment Buffer" temporary;
    begin
        NextEntryNo := 1;
        if "Sales Invoice Line"."Shipment No." <> '' then
            if SalesShipmentHeader.GET("Sales Invoice Line"."Shipment No.") then
                exit(SalesShipmentHeader."Posting Date");

        if "Sales Invoice Header"."Order No." = '' then
            exit("Sales Invoice Header"."Posting Date");

        case "Sales Invoice Line".Type of
            "Sales Invoice Line".Type::Item:
                GenerateBufferFromValueEntry("Sales Invoice Line");
            "Sales Invoice Line".Type::"G/L Account", "Sales Invoice Line".Type::Resource,
          "Sales Invoice Line".Type::"Charge (Item)", "Sales Invoice Line".Type::"Fixed Asset":
                GenerateBufferFromShipment("Sales Invoice Line");
            "Sales Invoice Line".Type::" ":
                exit(0D);
        end;

        SalesShipmentBuffer.RESET;
        SalesShipmentBuffer.SETRANGE("Document No.", "Sales Invoice Line"."Document No.");
        SalesShipmentBuffer.SETRANGE("Line No.", "Sales Invoice Line"."Line No.");
        if SalesShipmentBuffer.FIND('-') then begin
            SalesShipmentBuffer2 := SalesShipmentBuffer;
            if SalesShipmentBuffer.NEXT = 0 then begin
                SalesShipmentBuffer.GET(
                  SalesShipmentBuffer2."Document No.", SalesShipmentBuffer2."Line No.", SalesShipmentBuffer2."Entry No.");
                SalesShipmentBuffer.DELETE;
                exit(SalesShipmentBuffer2."Posting Date");
            end;
            SalesShipmentBuffer.CALCSUMS(Quantity);
            if SalesShipmentBuffer.Quantity <> "Sales Invoice Line".Quantity then begin
                SalesShipmentBuffer.DELETEALL;
                exit("Sales Invoice Header"."Posting Date");
            end;
        end else
            exit("Sales Invoice Header"."Posting Date");
    end;

    procedure GenerateBufferFromValueEntry(SalesInvoiceLine2: Record "Sales Invoice Line");
    var
        ValueEntry: Record "Value Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        TotalQuantity: Decimal;
        Quantity: Decimal;
    begin
        TotalQuantity := SalesInvoiceLine2."Quantity (Base)";
        ValueEntry.SETCURRENTKEY("Document No.");
        ValueEntry.SETRANGE("Document No.", SalesInvoiceLine2."Document No.");
        ValueEntry.SETRANGE("Posting Date", "Sales Invoice Header"."Posting Date");
        ValueEntry.SETRANGE("Item Charge No.", '');
        ValueEntry.SETFILTER("Entry No.", '%1..', FirstValueEntryNo);
        if ValueEntry.FIND('-') then
            repeat
                if ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") then begin
                    if SalesInvoiceLine2."Qty. per Unit of Measure" <> 0 then
                        Quantity := ValueEntry."Invoiced Quantity" / SalesInvoiceLine2."Qty. per Unit of Measure"
                    else
                        Quantity := ValueEntry."Invoiced Quantity";
                    AddBufferEntry(
                      SalesInvoiceLine2,
                      -Quantity,
                      ItemLedgerEntry."Posting Date");
                    TotalQuantity := TotalQuantity + ValueEntry."Invoiced Quantity";
                end;
                FirstValueEntryNo := ValueEntry."Entry No." + 1;
            until (ValueEntry.NEXT = 0) or (TotalQuantity = 0);
    end;

    procedure GenerateBufferFromShipment(SalesInvoiceLine: Record "Sales Invoice Line");
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesInvoiceLine2: Record "Sales Invoice Line";
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesShipmentLine: Record "Sales Shipment Line";
        TotalQuantity: Decimal;
        Quantity: Decimal;
    begin
        TotalQuantity := 0;
        SalesInvoiceHeader.SETCURRENTKEY("Order No.");
        SalesInvoiceHeader.SETFILTER("No.", '..%1', "Sales Invoice Header"."No.");
        SalesInvoiceHeader.SETRANGE("Order No.", "Sales Invoice Header"."Order No.");
        if SalesInvoiceHeader.FIND('-') then
            repeat
                SalesInvoiceLine2.SETRANGE("Document No.", SalesInvoiceHeader."No.");
                SalesInvoiceLine2.SETRANGE("Line No.", SalesInvoiceLine."Line No.");
                SalesInvoiceLine2.SETRANGE(Type, SalesInvoiceLine.Type);
                SalesInvoiceLine2.SETRANGE("No.", SalesInvoiceLine."No.");
                SalesInvoiceLine2.SETRANGE("Unit of Measure Code", SalesInvoiceLine."Unit of Measure Code");
                if SalesInvoiceLine2.FIND('-') then
                    repeat
                        TotalQuantity := TotalQuantity + SalesInvoiceLine2.Quantity;
                    until SalesInvoiceLine2.NEXT = 0;
            until SalesInvoiceHeader.NEXT = 0;

        SalesShipmentLine.SETCURRENTKEY("Order No.", "Order Line No.");
        SalesShipmentLine.SETRANGE("Order No.", "Sales Invoice Header"."Order No.");
        SalesShipmentLine.SETRANGE("Order Line No.", SalesInvoiceLine."Line No.");
        SalesShipmentLine.SETRANGE("Line No.", SalesInvoiceLine."Line No.");
        SalesShipmentLine.SETRANGE(Type, SalesInvoiceLine.Type);
        SalesShipmentLine.SETRANGE("No.", SalesInvoiceLine."No.");
        SalesShipmentLine.SETRANGE("Unit of Measure Code", SalesInvoiceLine."Unit of Measure Code");
        SalesShipmentLine.SETFILTER(Quantity, '<>%1', 0);

        if SalesShipmentLine.FIND('-') then
            repeat
                if "Sales Invoice Header"."Get Shipment Used" then
                    CorrectShipment(SalesShipmentLine);
                if ABS(SalesShipmentLine.Quantity) <= ABS(TotalQuantity - SalesInvoiceLine.Quantity) then
                    TotalQuantity := TotalQuantity - SalesShipmentLine.Quantity
                else begin
                    if ABS(SalesShipmentLine.Quantity) > ABS(TotalQuantity) then
                        SalesShipmentLine.Quantity := TotalQuantity;
                    Quantity :=
                      SalesShipmentLine.Quantity - (TotalQuantity - SalesInvoiceLine.Quantity);

                    TotalQuantity := TotalQuantity - SalesShipmentLine.Quantity;
                    SalesInvoiceLine.Quantity := SalesInvoiceLine.Quantity - Quantity;

                    if SalesShipmentHeader.GET(SalesShipmentLine."Document No.") then
                        AddBufferEntry(
                          SalesInvoiceLine,
                          Quantity,
                          SalesShipmentHeader."Posting Date");
                end;
            until (SalesShipmentLine.NEXT = 0) or (TotalQuantity = 0);
    end;

    procedure CorrectShipment(var SalesShipmentLine: Record "Sales Shipment Line");
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
    begin
        SalesInvoiceLine.SETCURRENTKEY("Shipment No.", "Shipment Line No.");
        SalesInvoiceLine.SETRANGE("Shipment No.", SalesShipmentLine."Document No.");
        SalesInvoiceLine.SETRANGE("Shipment Line No.", SalesShipmentLine."Line No.");
        if SalesInvoiceLine.FIND('-') then
            repeat
                SalesShipmentLine.Quantity := SalesShipmentLine.Quantity - SalesInvoiceLine.Quantity;
            until SalesInvoiceLine.NEXT = 0;
    end;

    procedure AddBufferEntry(SalesInvoiceLine: Record "Sales Invoice Line"; QtyOnShipment: Decimal; PostingDate: Date);
    begin
        SalesShipmentBuffer.SETRANGE("Document No.", SalesInvoiceLine."Document No.");
        SalesShipmentBuffer.SETRANGE("Line No.", SalesInvoiceLine."Line No.");
        SalesShipmentBuffer.SETRANGE("Posting Date", PostingDate);
        if SalesShipmentBuffer.FIND('-') then begin
            SalesShipmentBuffer.Quantity := SalesShipmentBuffer.Quantity + QtyOnShipment;
            SalesShipmentBuffer.MODIFY;
            exit;
        end;

        with SalesShipmentBuffer do begin
            "Document No." := SalesInvoiceLine."Document No.";
            "Line No." := SalesInvoiceLine."Line No.";
            "Entry No." := NextEntryNo;
            Type := SalesInvoiceLine.Type;
            "No." := SalesInvoiceLine."No.";
            Quantity := QtyOnShipment;
            "Posting Date" := PostingDate;
            INSERT;
            NextEntryNo := NextEntryNo + 1;
        end;
    end;

    local procedure DocumentCaption(): Text[250];
    var
        "---NAI---": Integer;
        SalesInvHeader: Record "Sales Invoice Header";
        SalesInvLine: Record "Sales Invoice Line";
    begin
        if "Sales Invoice Header"."Prepayment Invoice" then begin
            // >>> 2111-49001 15-02-2017 RM
            //  EXIT(Text010);
            SalesInvHeader.RESET;
            SalesInvHeader.SETCURRENTKEY("No.");
            SalesInvHeader.SETFILTER("No.", '..%1', "Sales Invoice Header"."No.");
            SalesInvHeader.SETFILTER("Prepayment Order No.", "Sales Invoice Header"."Prepayment Order No.");
            if SalesInvHeader.COUNT > 0 then begin
                exit(FORMAT(SalesInvHeader.COUNT) + '. ' + Text010);
            end else begin
                exit(Text010);
            end;
        end;

        //EXIT(Text004);
        SalesInvLine.RESET;
        SalesInvLine.SETRANGE("Document No.", "Sales Invoice Header"."No.");
        SalesInvLine.SETRANGE(Type, SalesInvLine.Type::"G/L Account");
        SalesInvLine.SETFILTER(Quantity, '<%1', 0);
        if SalesInvLine.FINDFIRST then begin
            exit(Text004);
        end else
            exit(Text004);
        // <<< 2111-49001 15-02-2017 RM
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    procedure CollectAsmInformation();
    var
        ValueEntry: Record "Value Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        PostedAsmHeader: Record "Posted Assembly Header";
        PostedAsmLine: Record "Posted Assembly Line";
        SalesShipmentLine: Record "Sales Shipment Line";
    begin
        TempPostedAsmLine.DELETEALL;
        if "Sales Invoice Line".Type <> "Sales Invoice Line".Type::Item then
            exit;
        with ValueEntry do begin
            SETCURRENTKEY("Document No.");
            SETRANGE("Document No.", "Sales Invoice Line"."Document No.");
            SETRANGE("Document Type", "Document Type"::"Sales Invoice");
            SETRANGE("Document Line No.", "Sales Invoice Line"."Line No.");
            if not FINDSET then
                exit;
        end;
        repeat
            if ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") then
                if ItemLedgerEntry."Document Type" = ItemLedgerEntry."Document Type"::"Sales Shipment" then begin
                    SalesShipmentLine.GET(ItemLedgerEntry."Document No.", ItemLedgerEntry."Document Line No.");
                    if SalesShipmentLine.AsmToShipmentExists(PostedAsmHeader) then begin
                        PostedAsmLine.SETRANGE("Document No.", PostedAsmHeader."No.");
                        if PostedAsmLine.FINDSET then
                            repeat
                                TreatAsmLineBuffer(PostedAsmLine);
                            until PostedAsmLine.NEXT = 0;
                    end;
                end;
        until ValueEntry.NEXT = 0;
    end;

    procedure TreatAsmLineBuffer(PostedAsmLine: Record "Posted Assembly Line");
    begin
        CLEAR(TempPostedAsmLine);
        TempPostedAsmLine.SETRANGE(Type, PostedAsmLine.Type);
        TempPostedAsmLine.SETRANGE("No.", PostedAsmLine."No.");
        TempPostedAsmLine.SETRANGE("Variant Code", PostedAsmLine."Variant Code");
        TempPostedAsmLine.SETRANGE(Description, PostedAsmLine.Description);
        TempPostedAsmLine.SETRANGE("Unit of Measure Code", PostedAsmLine."Unit of Measure Code");
        if TempPostedAsmLine.FINDFIRST then begin
            TempPostedAsmLine.Quantity += PostedAsmLine.Quantity;
            TempPostedAsmLine.MODIFY;
        end else begin
            CLEAR(TempPostedAsmLine);
            TempPostedAsmLine := PostedAsmLine;
            TempPostedAsmLine.INSERT;
        end;
    end;

    procedure GetUOMText(UOMCode: Code[10]): Text[10];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        if not UnitOfMeasure.GET(UOMCode) then
            exit(UOMCode);
        exit(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        exit(PADSTR('', 2, ' '));
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    procedure CreatePrePaymentInvoiceLines(): Boolean;
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
        EntryNo: Integer;
    begin
        // >>> 2111-49001 15-02-2017 RM
        EntryNo := GetCustLedgerEntryNo("Sales Invoice Header"."No.", 1, "Sales Invoice Header"."Posting Date");

        TempSalesInvoiceHeader.RESET;
        if not TempSalesInvoiceHeader.ISEMPTY then
            TempSalesInvoiceHeader.DELETEALL;
        SalesInvoiceHeader.RESET;
        SalesInvoiceHeader.SETCURRENTKEY("Prepayment Order No.", "Prepayment Invoice");
        SalesInvoiceHeader.SETRANGE("Prepayment Order No.", "Sales Invoice Header"."Order No.");
        SalesInvoiceHeader.SETRANGE("Prepayment Invoice", true);
        if SalesInvoiceHeader.FINDSET then begin
            repeat
                if EntryNo > GetCustLedgerEntryNo(SalesInvoiceHeader."No.", 1, SalesInvoiceHeader."Posting Date") then begin
                    // >>> 523-24969  16-07-2014 MMA
                    TempSalesInvoiceLineTotal.SETRANGE("Document No.", SalesInvoiceHeader."No.");
                    if not TempSalesInvoiceLineTotal.ISEMPTY then begin
                        exit(true);
                    end;
                    // <<< 523-24969  16-07-2014 MMA
                    TempSalesInvoiceLineTotal.INIT;
                    TempSalesInvoiceLineTotal."Document No." := SalesInvoiceHeader."No.";
                    TempSalesInvoiceLineTotal."Line No." := PrePaymentLineNo;
                    TempSalesInvoiceLineTotal."Posting Date" := SalesInvoiceHeader."Posting Date";
                    PrePaymentLineNo += 10000;

                    SalesInvoiceLine.RESET;
                    SalesInvoiceLine.SETRANGE("Document No.", SalesInvoiceHeader."No.");
                    SalesInvoiceLine.SETRANGE(Type, SalesInvoiceLine.Type::"G/L Account");
                    SalesInvoiceLine.SETRANGE("No.", SalesInvNo);
                    if SalesInvoiceLine.FINDSET then begin
                        repeat
                            TempSalesInvoiceLineTotal.Amount += SalesInvoiceLine.Amount;
                            TempSalesInvoiceLineTotal."Amount Including VAT" += SalesInvoiceLine."Amount Including VAT";
                        until SalesInvoiceLine.NEXT = 0;
                        TempSalesInvoiceLineTotal.INSERT;
                    end;
                end;
            until SalesInvoiceHeader.NEXT = 0;
            exit(true);
        end;
        exit(false);
        // <<< 2111-49001 15-02-2017 RM
    end;

    procedure CreatePrePaymentCrMemoLines(): Boolean;
    var
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        EntryNo: Integer;
    begin
        // >>> 2111-49001 15-02-2017 RM
        EntryNo := GetCustLedgerEntryNo("Sales Invoice Header"."No.", 1, "Sales Invoice Header"."Posting Date");

        TempSalesCrMemoHeader.RESET;
        if not TempSalesCrMemoHeader.ISEMPTY then
            TempSalesCrMemoHeader.DELETEALL;
        SalesCrMemoHeader.RESET;
        SalesCrMemoHeader.SETCURRENTKEY("Prepayment Order No.");

        if "Sales Invoice Header"."Prepayment Order No." <> '' then
            SalesCrMemoHeader.SETRANGE("Prepayment Order No.", "Sales Invoice Header"."Prepayment Order No.")
        else
            if "Sales Invoice Header"."Order No." <> '' then
                SalesCrMemoHeader.SETRANGE("Prepayment Order No.", "Sales Invoice Header"."Order No.");

        if SalesCrMemoHeader.FINDSET then begin
            repeat
                if EntryNo > GetCustLedgerEntryNo(SalesCrMemoHeader."No.", 2, SalesCrMemoHeader."Posting Date") then begin
                    // >>> 523-24969  16-07-2014 MMA
                    TempSalesInvoiceLineTotal.SETRANGE("Document No.", SalesCrMemoHeader."No.");
                    if not TempSalesInvoiceLineTotal.ISEMPTY then begin
                        exit(true);
                    end;
                    // <<< 523-24969  16-07-2014 MMA
                    TempSalesInvoiceLineTotal.INIT;
                    TempSalesInvoiceLineTotal."Document No." := SalesCrMemoHeader."No.";
                    TempSalesInvoiceLineTotal."Line No." := PrePaymentLineNo;
                    TempSalesInvoiceLineTotal."Posting Date" := SalesCrMemoHeader."Posting Date";
                    PrePaymentLineNo += 10000;

                    SalesCrMemoLine.RESET;
                    SalesCrMemoLine.SETRANGE("Document No.", SalesCrMemoHeader."No.");
                    SalesCrMemoLine.SETRANGE(Type, SalesCrMemoLine.Type::"G/L Account");
                    SalesCrMemoLine.SETRANGE("No.", SalesInvNo);
                    if SalesCrMemoLine.FINDSET then begin
                        repeat
                            TempSalesInvoiceLineTotal.Amount -= SalesCrMemoLine.Amount;
                            TempSalesInvoiceLineTotal."Amount Including VAT" -= SalesCrMemoLine."Amount Including VAT";
                        until SalesCrMemoLine.NEXT = 0;
                        TempSalesInvoiceLineTotal.INSERT;
                    end;
                end;
            until SalesCrMemoHeader.NEXT = 0;
            exit(true);
        end;
        exit(false);
        // <<< 2111-49001 15-02-2017 RM
    end;

    procedure CreateInvoiceTotal();
    var
        SalesInvHeader: Record "Sales Invoice Header";
        SalesInvLine: Record "Sales Invoice Line";
        EntryNo: Integer;
    begin
        // >>> 2111-49001 15-02-2017 RM
        EntryNo := GetCustLedgerEntryNo("Sales Invoice Header"."No.", 1, "Sales Invoice Header"."Posting Date");
        PrePaymentLineNo := 10000;
        if "Sales Invoice Header"."Order No." <> '' then begin
            SalesInvHeader.RESET;
            SalesInvHeader.SETRANGE("Order No.", "Sales Invoice Header"."Order No.");
            SalesInvHeader.SETRANGE("Prepayment Invoice", false);
            if SalesInvHeader.FINDSET then begin
                repeat
                    if EntryNo >= GetCustLedgerEntryNo(SalesInvHeader."No.", 1, SalesInvHeader."Posting Date") then begin
                        // >>> 523-24969  16-07-2014 MMA
                        TempSalesInvoiceLineTotal2.SETRANGE("Document No.", SalesInvHeader."No.");
                        if not TempSalesInvoiceLineTotal2.ISEMPTY then begin
                            exit;
                        end;
                        // <<< 523-24969  16-07-2014 MMA
                        TempSalesInvoiceLineTotal2.INIT;
                        TempSalesInvoiceLineTotal2."Document No." := SalesInvHeader."No.";
                        TempSalesInvoiceLineTotal2."Line No." := PrePaymentLineNo;
                        TempSalesInvoiceLineTotal2."Posting Date" := SalesInvHeader."Posting Date";
                        PrePaymentLineNo += 10000;

                        SalesInvLine.RESET;
                        SalesInvLine.SETRANGE("Document No.", SalesInvHeader."No.");
                        SalesInvLine.SETRANGE(Type, SalesInvLine.Type::"G/L Account");
                        if SalesInvLine.FINDSET then begin
                            repeat
                                if SalesInvLine.Quantity < 0 then begin
                                    TempSalesInvoiceLineTotal2.Amount += SalesInvLine.Amount;
                                    TempSalesInvoiceLineTotal2."Amount Including VAT" += SalesInvLine."Amount Including VAT";
                                end;
                            until SalesInvLine.NEXT = 0;
                            TempSalesInvoiceLineTotal2.INSERT;
                        end;
                    end;
                until SalesInvHeader.NEXT = 0;
            end;
        end;
        // <<< 2111-49001 15-02-2017 RM
    end;

    procedure GetCustLedgerEntryNo(DocumentNo: Code[20]; DocumentType: Integer; PostingDate: Date): Integer;
    var
        CustLedgerEntry: Record "Cust. Ledger Entry";
    begin
        // >>> 2111-49001 15-02-2017 RM
        // Document type
        // 1 = Invoice
        // 2 = Credit Memo
        CustLedgerEntry.RESET;
        CustLedgerEntry.SETRANGE("Document No.", DocumentNo);
        if DocumentType = 1 then begin
            CustLedgerEntry.SETRANGE("Document Type", CustLedgerEntry."Document Type"::Invoice);
        end;
        if DocumentType = 2 then begin
            CustLedgerEntry.SETRANGE("Document Type", CustLedgerEntry."Document Type"::"Credit Memo");
        end;
        CustLedgerEntry.SETRANGE("Posting Date", PostingDate);
        if CustLedgerEntry.FINDFIRST then begin
            exit(CustLedgerEntry."Entry No.");
        end else begin
            exit(0);
        end;
        // <<< 2111-49001 15-02-2017 RM
    end;

    local procedure IsDiscount(SalesInvHeaderDisc: Record "Sales Invoice Header"): Boolean;
    var
        SalesInvLineDisc: Record "Sales Invoice Line";
    begin
        // >>> 2111-23178 31-07-2014 UQ
        SalesInvLineDisc.SETRANGE("Document No.", SalesInvHeaderDisc."No.");
        SalesInvLineDisc.SETFILTER("Line Discount %", '<>0');
        exit(not SalesInvLineDisc.ISEMPTY);
        // <<< 2111-23178 31-07-2014 UQ
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-34604 27-11-2015 SMS
        with "Sales Invoice Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-34604 27-11-2015 SMS
    end;
}

