report 50001 "MMC Sales - Credit Memo AR"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report/Sales - Credit Memo NAI.rdl';

    CaptionML = DEU = 'Verkauf - Rechnungskorrektur',
                ENU = 'Sales - Credit Memo';
    Permissions = TableData "Sales Shipment Buffer" = rimd;

    dataset
    {
        dataitem("Sales Cr.Memo Header"; "Sales Cr.Memo Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Geb. Verkaufsgutschrift',
                                     ENU = 'Posted Sales Credit Memo';
            column(No_SalesCrMemoHeader; "No.")
            {
            }
            column(VATAmtLineVATCptn; VATAmtLineVATCptnLbl)
            {
            }
            column(VATAmtLineVATBaseCptn; VATAmtLineVATBaseCptnLbl)
            {
            }
            column(VATAmtLineVATAmtCptn; VATAmtLineVATAmtCptnLbl)
            {
            }
            column(VATAmtLineVATIdentifierCptn; VATAmtLineVATIdentifierCptnLbl)
            {
            }
            column(TotalCptn; TotalCptnLbl)
            {
            }
            column(SalesCrMemoLineDiscCaption; SalesCrMemoLineDiscCaptionLbl)
            {
            }
            column(ShowDiscount; ShowDiscount)
            {
            }
            column(LanguageCode; "Language Code")
            {
            }
            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(DocumentCaptionCopyText; STRSUBSTNO(DocumentCaption, "Sales Cr.Memo Header"."No.", CopyText))
                    {
                    }
                    column(Text5000000_SalesCrMemoHeaderNo; STRSUBSTNO(Text5000000, STRSUBSTNO(DocumentCaption, "Sales Cr.Memo Header"."No.", '')))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(ShowSellToInformation; ShowSellToInfo)
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name; "Sales Cr.Memo Header"."Sell-to Customer Name")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name2; "Sales Cr.Memo Header"."Sell-to Customer Name 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name3; "Sales Cr.Memo Header"."Sell-to Customer Name 3")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name4; "Sales Cr.Memo Header"."Sell-to Customer Name 4")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Address; "Sales Cr.Memo Header"."Sell-to Address")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Address2; "Sales Cr.Memo Header"."Sell-to Address 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Postcode; "Sales Cr.Memo Header"."Sell-to Post Code")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_City; "Sales Cr.Memo Header"."Sell-to City")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesCrMemoHeader; "Sales Cr.Memo Header"."Bill-to Customer No.")
                    {
                    }
                    column(PostDate_SalesCrMemoHeader; FORMAT("Sales Cr.Memo Header"."Posting Date"))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesCrMemoHeader; "Sales Cr.Memo Header"."VAT Registration No.")
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(AppliedToText; AppliedToText)
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesCrMemoHeader; "Sales Cr.Memo Header"."Your Reference")
                    {
                    }
                    column(SalesCrMemoHeader_DueDate; FORMAT("Sales Cr.Memo Header"."Due Date", 0, 4))
                    {
                    }
                    column(SalesCrMemoHeader_DueDateCaption; "Sales Cr.Memo Header".FIELDCAPTION("Due Date"))
                    {
                    }
                    column(SalesCrMemoHeader_ExternalDocumentNo; "Sales Cr.Memo Header"."External Document No.")
                    {
                    }
                    column(SalesCrMemoHeader_ExternalDocumentNoCaption; "Sales Cr.Memo Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(DocDate_SalesCrMemoHeader; FORMAT("Sales Cr.Memo Header"."Document Date", 0, 4))
                    {
                    }
                    column(PriceInclVAT_SalesCrMemoHeader; "Sales Cr.Memo Header"."Prices Including VAT")
                    {
                    }
                    column(ReturnOrderNoText; ReturnOrderNoText)
                    {
                    }
                    column(ReturnOrdNo_SalesCrMemoHeader; "Sales Cr.Memo Header"."Return Order No.")
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text006, ''))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PricesInclVATYesNo; FORMAT("Sales Cr.Memo Header"."Prices Including VAT"))
                    {
                    }
                    column(VATBaseDiscPrc_SalesCrMemoLine; "Sales Cr.Memo Header"."VAT Base Discount %")
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(No1_SalesCrMemoHeaderCptn; No1_SalesCrMemoHeaderCptnLbl)
                    {
                    }
                    column(SalesCrMemoHeaderPostDtCptn; SalesCrMemoHeaderPostDtCptnLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(CompanyInfoHomePageCaption; CompanyInfoHomePageCaptionLbl)
                    {
                    }
                    column(CompanyInfoEmailCaption; CompanyInfoEmailCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesCrMemoHeaderCaption; SalesCrMemoHeader_BilltoCustomerNoCaptionLbl)
                    {
                    }
                    column(PriceInclVAT_SalesCrMemoHeaderCaption; "Sales Cr.Memo Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(OrderTitle; OrderTitle)
                    {
                    }
                    column(OrderTitleNo; OrderTitleNo)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }
                    column(PrepayCreditMemo; "Sales Cr.Memo Header"."Prepayment Credit Memo")
                    {
                    }
                    column(AssociationName; AssociationName)
                    {
                    }
                    column(Currenytxt; Text009) { }
                    column(CurrenyCode; 'EUR') { }
                    column(CollecInvCode; Cust."Combine Shipment Code") { }
                    column(CollecInvtxt; CollecInvtxt) { }
                    column(Lieferantennummer; Cust."Our Account No.") { }
                    column(OurAccounttxt; OurAccountCaption) { }
                    column(LeistungsdatumCpttxt; LeistungsdatumCpt) { }
                    column(SELLTOADDRESSCPTtxt; SELLTOADDRESSCPT) { }
                    //column(CustAssociationSortCode; CustAssociationSortCode) { }
                    column(UserName; User."Full Name") { }
                    column(Transportreasoncpt; TransportReasonLbl) { }
                    column(TransportReason; TransportReasontxt) { }
                    column(UserCpt; userlbl)
                    {
                    }
                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FINDSET then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1, %2 %3', DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            NAIExtPreText.RESET;
                            NAIExtPreText.SETRANGE("Table ID", DATABASE::"Sales Cr.Memo Header");
                            NAIExtPreText.SETRANGE("Document No.", "Sales Cr.Memo Header"."No.");
                            NAIExtPreText.SETRANGE("Assigned for", NAIExtPreText."Assigned for"::"Posted Credit Memo");
                            NAIExtPreText.SETRANGE("Text Position", NAIExtPreText."Text Position"::Pretext);
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(114), "Print Attribute" = CONST(true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }
                    }
                    dataitem("Sales Cr.Memo Line"; "Sales Cr.Memo Line")
                    {
                        DataItemLink = "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING("Document No.", "Line No.");
                        column(LineAmt_SalesCrMemoLine; "Line Amount")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(Desc_SalesCrMemoLine; Description)
                        {
                        }
                        column(Desc_SalesCrMemoLine2; "Description 2")
                        {
                        }
                        column(Desc_SalesCrMemoLine3; "Description 3")
                        {
                        }
                        column(No_SalesCrMemoLine; "No.")
                        {
                        }
                        column(Qty_SalesCrMemoLine; Quantity)
                        {
                        }
                        column(UOM_SalesCrMemoLine; "Unit of Measure")
                        {
                        }
                        column(UnitPrice_SalesCrMemoLine; "Unit Price")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 2;
                        }
                        column(Disc_SalesCrMemoLine; "Line Discount %")
                        {
                        }
                        column(VATIdentif_SalesCrMemoLine; "VAT Identifier")
                        {
                        }
                        column(PostedReceiptDate; FORMAT(PostedReceiptDate))
                        {
                        }
                        column(Type_SalesCrMemoLine; FORMAT(Type))
                        {
                        }
                        column(NNCTotalLineAmt; NNC_TotalLineAmount)
                        {
                        }
                        column(NNCTotalAmtInclVat; NNC_TotalAmountInclVat)
                        {
                        }
                        column(NNCTotalInvDiscAmt_SalesCrMemoLine; NNC_TotalInvDiscAmount)
                        {
                        }
                        column(NNCTotalAmt; NNC_TotalAmount)
                        {
                        }
                        column(InvDiscAmt_SalesCrMemoLine; -"Inv. Discount Amount")
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(Amt_SalesCrMemoLine; Amount)
                        {
                            AutoFormatExpression = GetCurrencyCode;
                            AutoFormatType = 1;
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(VATAmtLineVATAmtTxt; VATAmountLine.VATAmountText)
                        {
                        }
                        column(LineAmtInvDiscAmt_SalesCrMemoLine; -("Line Amount" - "Inv. Discount Amount" - "Amount Including VAT"))
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(LineNo_SalesCrMemoLine; "Line No.")
                        {
                        }
                        column(UnitPrice_SalesCrMemoLineCaption; UnitPriceCptnLbl)
                        {
                        }
                        column(Amount_SalesCrMemoLineCaption; AmountCptnLbl)
                        {
                        }
                        column(PostedReceiptDateCptn; PostedReceiptDateCptnLbl)
                        {
                        }
                        column(InvDiscAmt_SalesCrMemoLineCptn; InvDiscAmt_SalesCrMemoLineCptnLbl)
                        {
                        }
                        column(SubtotalCptn; SubtotalCptnLbl)
                        {
                        }
                        column(LineAmtInvDiscAmt_SalesCrMemoLineCptn; LineAmtInvDiscAmt_SalesCrMemoLineCptnLbl)
                        {
                        }
                        column(Desc_SalesCrMemoLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(No_SalesCrMemoLineCaption; FIELDCAPTION("No."))
                        {
                        }
                        column(Qty_SalesCrMemoLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(UOM_SalesCrMemoLineCaption; UOM_SalesCrMemoLineCaption)
                        {
                        }
                        column(VATIdentif_SalesCrMemoLineCaption; FIELDCAPTION("VAT Identifier"))
                        {
                        }
                        column(PositionNo_SalesCrMemoLine; "Position No.")
                        {
                        }
                        column(QuantityWp_SalesCrMemoLine; 0.0)
                        {
                        }
                        column(PositionNo_SalesCrMemoLineCaption; SalesCrMemoLine_PositionNoCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesCrMemoLineCaption; '')
                        {
                        }
                        column(LastCrMemoLineNo; LastCrMemoLineNo)
                        {
                        }
                        column(ShowArchivLine; ShowArchivLine)
                        {
                        }
                        column(Text5244801; Text5244801)
                        {
                        }
                        column(DiscountCaption; DiscountCaptionLbl)
                        {
                        }
                        column(LineDis_SalesCrMemoLine; "Line Discount %")
                        {
                        }
                        column(TextForCrMemo; "Text Credit Memos")
                        {
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(QuantityInBacklog; ABS(QuantityInBacklog))
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        dataitem(ValueEntry2; "Value Entry")
                        {
                            DataItemLink = "Document No." = FIELD("Document No."), "Document Line No." = FIELD("Line No.");
                            DataItemTableView = WHERE("Document Type" = CONST("Sales Credit Memo"));
                            column(ItemSerialNo; ItemSerialNo)
                            {
                            }
                            column(ItemLotNo; ItemLotNo)
                            {
                            }
                            column(ItemExpirationDate; FORMAT(ItemExpirationDate))
                            {
                            }
                            column(ItemQuantity; ABS(ItemQuantity))
                            {
                            }

                            trigger OnAfterGetRecord();
                            var
                                ItemLedgerEntry2: Record "Item Ledger Entry";
                            begin
                                // >>> 2111-34284 25-11-2015 SMS
                                ItemSerialNo := '';
                                ItemLotNo := '';
                                ItemExpirationDate := 0D;
                                ItemQuantity := 0;
                                if ItemLedgerEntry2.GET(ValueEntry2."Item Ledger Entry No.") then begin
                                    ItemSerialNo := ItemLedgerEntry2."Serial No.";
                                    ItemLotNo := ItemLedgerEntry2."Lot No.";
                                    ItemExpirationDate := ItemLedgerEntry2."Expiration Date";
                                    ItemQuantity := ItemLedgerEntry2.Quantity;
                                end;
                                // <<< 2111-34284 25-11-2015 SMS
                            end;
                        }
                        dataitem("Sales Shipment Buffer"; "Integer")
                        {
                            DataItemTableView = SORTING(Number);

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then
                                    SalesShipmentBuffer.FIND('-')
                                else
                                    SalesShipmentBuffer.NEXT;
                            end;

                            trigger OnPreDataItem();
                            begin
                                SETRANGE(Number, 1, SalesShipmentBuffer.COUNT);
                            end;
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText_DimensionLoop2; DimText)
                            {
                            }
                            column(LineDimCptn; LineDimCptnLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FIND('-') then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Cr.Memo Line"."Dimension Set ID");
                            end;
                        }
                        dataitem("Sales Header Archive"; "Sales Header Archive")
                        {
                            DataItemTableView = SORTING("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
                            column(Text5244802_OrderNo; Text5244802 + "Sales Header Archive"."No.")
                            {
                            }
                            column(SalesHeaderArchive_DocumentType; "Document Type")
                            {
                            }
                            column(SalesHeade_Archive_No; "No.")
                            {
                            }
                            column(SalesHeaderArchive_DocNoOccurrence; "Doc. No. Occurrence")
                            {
                            }
                            column(SalesHeaderArchive_VersionNo; "Version No.")
                            {
                            }
                            dataitem("Sales Line Archive"; "Sales Line Archive")
                            {
                                DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No."), "Doc. No. Occurrence" = FIELD("Doc. No. Occurrence"), "Version No." = FIELD("Version No.");
                                DataItemTableView = SORTING("Document Type", "Document No.", "Doc. No. Occurrence", "Version No.", "Line No.");
                                column(SalesLineArchive_No; "No.")
                                {
                                }
                                column(SalesLineArchive_Description; Description)
                                {
                                }
                                column(SalesLineArchive_Quantity; Quantity)
                                {
                                }
                                column(SalesLineArchive_UnitofMeasure; "Unit of Measure")
                                {
                                }
                                column(SalesLineArchive_Type; Type)
                                {
                                }
                                column(SalesLineArchive_PositionNo; "Position No.")
                                {
                                }
                                column(SalesLineArchive_DocumentType; "Document Type")
                                {
                                }
                                column(SalesLineArchive_DocumentNo; "Document No.")
                                {
                                }
                                column(SalesLineArchive_DocNoOccurrence; "Doc. No. Occurrence")
                                {
                                }
                                column(SalesLineArchive_VersionNo; "Version No.")
                                {
                                }
                                column(SalesLineArchive_LineNo; "Line No.")
                                {
                                }
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 15-02-2017 RM
                                if not ShowArchivLine and (LastCrMemoLineNo <> "Sales Cr.Memo Line"."Line No.") then
                                    CurrReport.BREAK;
                                SETRANGE("Attached to Prepayment No.", "Sales Cr.Memo Header"."No.");
                                // <<< 2111-49001 15-02-2017 RM
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source No." = FIELD("Document No."), "Source Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Cr.Memo Line";
                            DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(115), "Print Attribute" = CONST(true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                            SalesCrMemoLineRel: Record "Sales Cr.Memo Line";
                            QuantityDeliv: Decimal;
                        begin
                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // <<< 2111-47914 21-09-2016 MMA
                            // >>> 2111-34284 24-11-2015 SMS
                            ItemTariffNo := '';
                            if "Sales Cr.Memo Line".Type = "Sales Cr.Memo Line".Type::Item then begin
                                // >>> 2111-37329 28-07-2016 MK
                                //ItemRec.GET("Sales Cr.Memo Line"."No.");
                                //ItemTariffNo := ItemRec."Tariff No.";
                                if ItemRec.GET("Sales Cr.Memo Line"."No.") then
                                    ItemTariffNo := ItemRec."Tariff No.";
                                // <<< 2111-37329 28-07-2016 MK
                                // >>> 2111-47914 21-09-2016 MMA
                                GetItemCrossReference;
                                // <<< 2111-47914 21-09-2016 MMA
                            end;

                            // >>> 2111-62373 01-06-2018 RM
                            //QuantityInBacklog := 0;
                            //QuantityDeliv := 0;
                            //SalesCrMemoLineRel.RESET;
                            //SalesCrMemoLineRel.SETRANGE("Order No.","Sales Cr.Memo Line"."Order No.");
                            //SalesCrMemoLineRel.SETRANGE("Order Line No.","Sales Cr.Memo Line"."Order Line No.");
                            //IF SalesCrMemoLineRel.FINDSET THEN BEGIN
                            //  REPEAT
                            //    QuantityDeliv += SalesCrMemoLineRel.Quantity;
                            //  UNTIL SalesCrMemoLineRel.NEXT = 0;
                            //  QuantityInBacklog := "Sales Cr.Memo Line".Quantity - QuantityDeliv;
                            //END;
                            // <<< 2111-62373 01-06-2018 RM
                            // <<< 2111-34284 24-11-2015 SMS

                            NNC_TotalLineAmount += "Line Amount";
                            NNC_TotalAmountInclVat += "Amount Including VAT";
                            NNC_TotalInvDiscAmount += "Inv. Discount Amount";
                            NNC_TotalAmount += Amount;

                            // >>> 2111-49001 15-02-2017 RM
                            if ShowArchivLine then begin
                                if LastCrMemoLineNo = "Sales Cr.Memo Line"."Line No." then begin
                                    "Sales Cr.Memo Line".Description := Text5244801;
                                end;
                            end;
                            // <<< 2111-49001 15-02-2017 RM

                            SalesShipmentBuffer.DELETEALL;
                            PostedReceiptDate := 0D;
                            if Quantity <> 0 then
                                PostedReceiptDate := FindPostedShipmentDate;

                            // >>> 2111-34722 25-01-2016 MMA
                            //IF (Type = Type::"G/L Account") AND (NOT ShowInternalInfo) THEN
                            if (Type = Type::"G/L Account") and (not ShowInternalInfo) and (not ShowGLAccount) then
                                // <<< 2111-34722 25-01-2016 MMA
                                "No." := '';

                            VATAmountLine.INIT;
                            VATAmountLine."VAT Identifier" := "VAT Identifier";
                            VATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                            VATAmountLine."Tax Group Code" := "Tax Group Code";
                            VATAmountLine."VAT %" := "VAT %";
                            VATAmountLine."VAT Base" := Amount;
                            VATAmountLine."Amount Including VAT" := "Amount Including VAT";
                            VATAmountLine."Line Amount" := "Line Amount";
                            if "Allow Invoice Disc." then
                                VATAmountLine."Inv. Disc. Base Amount" := "Line Amount";
                            VATAmountLine."Invoice Discount Amount" := "Inv. Discount Amount";
                            VATAmountLine."VAT Clause Code" := "VAT Clause Code";
                            VATAmountLine.InsertLine;
                        end;

                        trigger OnPreDataItem();
                        var
                            "---NAI---": Integer;
                            SalesCrMemoLine: Record "Sales Cr.Memo Line";
                        begin
                            VATAmountLine.DELETEALL;
                            SalesShipmentBuffer.RESET;
                            SalesShipmentBuffer.DELETEALL;
                            FirstValueEntryNo := 0;
                            MoreLines := FIND('+');
                            while MoreLines and (Description = '') and ("No." = '') and (Quantity = 0) and (Amount = 0) do
                                MoreLines := NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SETRANGE("Line No.", 0, "Line No.");
                            CurrReport.CREATETOTALS(Amount, "Amount Including VAT", "Inv. Discount Amount");

                            // >>> 2111-49001 15-02-2017 RM
                            LastCrMemoLineNo := 0;
                            if ShowArchivLine then begin
                                "Sales Cr.Memo Line".SETFILTER(Type, '<>%1', "Sales Cr.Memo Line".Type::" ");
                                SalesCrMemoLine.RESET;
                                SalesCrMemoLine.COPYFILTERS("Sales Cr.Memo Line");
                                if SalesCrMemoLine.FINDLAST then begin
                                    LastCrMemoLineNo := SalesCrMemoLine."Line No.";
                                end;
                            end;
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                    dataitem(VATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmtLineVATBase; VATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmt; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineLineAmt; VATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; VATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvoiceDiscAmt; VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVAT; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATAmtSpecificationCptn; VATAmtSpecificationCptnLbl)
                        {
                        }
                        column(VATAmtLineInvDiscBaseAmtCptn; VATAmtLineInvDiscBaseAmtCptnLbl)
                        {
                        }
                        column(VATAmtLineLineAmtCptn; VATAmtLineLineAmtCptnLbl)
                        {
                        }
                        column(VATAmtLineInvoiceDiscAmtCptn; VATAmtLineInvoiceDiscAmtCptnLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            if VATAmountLine.GetTotalVATAmount = 0 then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(
                              VATAmountLine."Line Amount", VATAmountLine."Inv. Disc. Base Amount",
                              VATAmountLine."Invoice Discount Amount", VATAmountLine."VAT Base", VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATClauseEntryCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATClauseVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATClauseCode; VATAmountLine."VAT Clause Code")
                        {
                        }
                        column(VATClauseDescription; VATClause.Description)
                        {
                        }
                        column(VATClauseDescription2; VATClause."Description 2")
                        {
                        }
                        column(VATClauseAmount; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATClausesCaption; VATClausesCap)
                        {
                        }
                        column(VATClauseVATIdentifierCaption; VATIdentifierCaptionLbl)
                        {
                        }
                        column(VATClauseVATAmtCaption; VATAmountCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                            if not VATClause.GET(VATAmountLine."VAT Clause Code") then
                                CurrReport.SKIP;
                            VATClause.TranslateDescription("Sales Cr.Memo Header"."Language Code");
                        end;

                        trigger OnPreDataItem();
                        begin
                            CLEAR(VATClause);
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATCounterLCY; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercent; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATIdentifier_VATCounterLCY; VATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);

                            VALVATBaseLCY := ROUND(VATAmountLine."VAT Base" / "Sales Cr.Memo Header"."Currency Factor");
                            VALVATAmountLCY := ROUND(VATAmountLine."VAT Amount" / "Sales Cr.Memo Header"."Currency Factor");
                        end;

                        trigger OnPreDataItem();
                        begin
                            if (not GLSetup."Print VAT specification in LCY") or
                               ("Sales Cr.Memo Header"."Currency Code" = '') or
                               (VATAmountLine.GetTotalVATAmount = 0)
                            then
                                CurrReport.BREAK;

                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VALVATBaseLCY, VALVATAmountLCY);

                            if GLSetup."LCY Code" = '' then
                                VALSpecLCYHeader := Text008 + Text009
                            else
                                VALSpecLCYHeader := Text008 + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Cr.Memo Header"."Posting Date", "Sales Cr.Memo Header"."Currency Code", 1);
                            CalculatedExchRate := ROUND(1 / "Sales Cr.Memo Header"."Currency Factor" * CurrExchRate."Exchange Rate Amount", 0.000001);
                            VALExchRate := STRSUBSTNO(Text010, CalculatedExchRate, CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(Total; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesCrMemoHeader; "Sales Cr.Memo Header"."Sell-to Customer No.")
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShiptoAddressCaption; ShiptoAddressCaptionLbl)
                        {
                        }
                        column(SelltoCustNo_SalesCrMemoHeaderCaption; "Sales Cr.Memo Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowShippingAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 15-02-2017 RM
                            NAIExtPostText.RESET;
                            NAIExtPostText.SETRANGE("Table ID", DATABASE::"Sales Cr.Memo Header");
                            NAIExtPostText.SETRANGE("Document No.", "Sales Cr.Memo Header"."No.");
                            NAIExtPostText.SETRANGE("Assigned for", NAIExtPostText."Assigned for"::"Posted Credit Memo");
                            NAIExtPostText.SETRANGE("Text Position", NAIExtPostText."Text Position"::Posttext);
                            // <<< 2111-49001 15-02-2017 RM
                        end;
                    }
                }

                trigger OnAfterGetRecord();
                begin
                    CurrReport.PAGENO := 1;
                    if Number > 1 then begin
                        CopyText := Text004;
                        OutputNo += 1;
                    end;

                    NNC_TotalLineAmount := 0;
                    NNC_TotalAmountInclVat := 0;
                    NNC_TotalInvDiscAmount := 0;
                    NNC_TotalAmount := 0;
                end;

                trigger OnPostDataItem();
                begin
                    if not CurrReport.PREVIEW then
                        SalesCrMemoCountPrinted.RUN("Sales Cr.Memo Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                "---NAI---": Integer;
                SalesHeaderArchive: Record "Sales Header Archive";
                SalesInvoiceHeader: Record "Sales Invoice Header";
                GeneralReportMgmt: Codeunit "General Report Management";
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");

                CompanyInfo.GET;

                // >>> 2111-49001 15-02-2017 RM
                ShowArchivLine := false;
                if "Prepayment Credit Memo" then begin
                    SalesHeaderArchive.RESET;
                    SalesHeaderArchive.SETRANGE("Attached to Prepayment No.", "Sales Cr.Memo Header"."No.");
                    if SalesHeaderArchive.FINDFIRST then begin
                        ShowArchivLine := true;
                    end;
                end;

                if "Applies-to Doc. Type" = "Applies-to Doc. Type"::Invoice then begin
                    if SalesInvoiceHeader.GET("Applies-to Doc. No.") then begin
                        OrderNo := SalesInvoiceHeader."Order No.";
                    end;
                end;

                if "Prepayment Credit Memo" then begin
                    OrderTitle := FIELDCAPTION("Prepayment Order No.");
                    OrderTitleNo := "Prepayment Order No.";
                end else begin
                    OrderTitle := OrderCaption;
                    OrderTitleNo := OrderNo;
                end;
                // <<< 2111-49001 15-02-2017 RM

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 15-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, MAXSTRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 15-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 15-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 15-02-2017 RM
                end;

                // >>> 15.10.19 MMC OP
                User.RESET;
                User.SETRANGE("User Name", "Sales Cr.Memo Header"."User ID");
                IF NOT User.FINDFIRST THEN
                    User.INIT;
                //<<< 15.10.19 MMC OP

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                if "Return Order No." = '' then
                    ReturnOrderNoText := ''
                else
                    ReturnOrderNoText := FIELDCAPTION("Return Order No.");

                // >>> 2111-49001 15-02-2017 RM
                if ShowOfficeSalesPerson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 15-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 15-02-2017 RM
                end;
                // <<< 2111-49001 15-02-2017 RM

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                if "VAT Registration No." = '' then
                    VATNoText := ''
                else
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                if "Currency Code" = '' then begin
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text007, GLSetup."LCY Code");
                end else begin
                    TotalText := STRSUBSTNO(Text001, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text007, "Currency Code");
                end;
                FormatAddr.SalesCrMemoBillTo(CustAddr, "Sales Cr.Memo Header");
                if "Applies-to Doc. No." = '' then
                    AppliedToText := ''
                else
                    AppliedToText := STRSUBSTNO(Text003, "Applies-to Doc. Type", "Applies-to Doc. No.");

                FormatAddr.SalesCrMemoShipTo(ShipToAddr, CustAddr, "Sales Cr.Memo Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                for i := 1 to ARRAYLEN(ShipToAddr) do
                    if ShipToAddr[i] <> CustAddr[i] then
                        ShowShippingAddr := true;

                if LogInteraction then
                    if not CurrReport.PREVIEW then
                        if "Bill-to Contact No." <> '' then
                            SegManagement.LogDocument(
                              6, "No.", 0, 0, DATABASE::Contact, "Bill-to Contact No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '')
                        else
                            SegManagement.LogDocument(
                              6, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '');

                // >>> 2111-23178 31-07-2014 UQ
                ShowDiscount := IsDiscount("Sales Cr.Memo Header");
                // <<< 2111-23178 31-07-2014 UQ

                // >>> 2111-62084 09-11-2017 AM
                //AssociationName := '';
                //CustAssociationSortCode := '';
                //if Cust.GET("Bill-to Customer No.") then begin
                //    if Association.GET(Association.Type::Customer, Cust."OPP Association No.") then begin
                //        AssociationName := Association.Name;
                //        CustAssociationSortCode := Cust."OPP Association Sort Code";
                //    end;
                //end;
                // <<< 2111-62084 09-11-2017 AM

                // >>> 2111-62234 13-03-2018 RM
                ShowSellToInfo := not GeneralReportMgmt.SalesCrMemoHeaderSellToEqualsBillTo("Sales Cr.Memo Header");
                // <<< 2111-62234 13-03-2018 RM
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                    }
                    field(ShowGLAccount; ShowGLAccount)
                    {
                        CaptionML = DEU = 'Sachkonto anzeigen',
                                    ENU = 'Show G/L Account';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                    }
                    field(ShowOfficeSalesPerson; ShowOfficeSalesPerson)
                    {
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
        end;

        trigger OnOpenPage();
        begin
            LogInteraction := SegManagement.FindInteractTmplCode(6) <> '';
            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        GLSetup.GET;
    end;

    trigger OnPreReport();
    begin
        if not CurrReport.USEREQUESTPAGE then
            InitLogInteraction;
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'Total %1', ENU = 'Total %1', ITA = 'Totale %1';
        Text002: TextConst DEU = 'Total %1 inkl. MwSt.', ENU = 'Total %1 Incl. VAT', ITA = 'Importo %1 totale incl. IVA';
        Text003: TextConst DEU = '(Ausgleich für %1 %2)', ENU = '(Applies to %1 %2)';
        Text004: TextConst DEU = 'KOPIE', ENU = 'COPY';
        Text005: TextConst DEU = 'Rechnungskorrektur %1 %2', ENU = 'Credit Note %1 %2', ITA = 'Nota di credito n. %1 %2';
        Text006: TextConst DEU = 'Seite %1', ENU = 'Page %1';
        Text007: TextConst DEU = 'Total %1 ohne MwSt.', ENU = 'Total %1 Excl. VAT', ITA = 'Importo %1 totale escl. IVA';
        GLSetup: Record "General Ledger Setup";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo2: Record "Company Information";
        VATAmountLine: Record "VAT Amount Line" temporary;
        VATClause: Record "VAT Clause";
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        Language: Record Language;
        SalesShipmentBuffer: Record "Sales Shipment Buffer" temporary;
        CurrExchRate: Record "Currency Exchange Rate";
        User: Record User;
        SalesCrMemoCountPrinted: Codeunit "Sales Cr. Memo-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        RespCenter: Record "Responsibility Center";
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        ReturnOrderNoText: Text[80];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        AppliedToText: Text[40];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        LogInteraction: Boolean;
        FirstValueEntryNo: Integer;
        PostedReceiptDate: Date;
        NextEntryNo: Integer;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        Text008: TextConst DEU = 'MwSt.-Betrag Spezifikation in ', ENU = 'VAT Amount Specification in ';
        Text009: TextConst DEU = 'Landeswährung', ENU = 'Local Currency', ITA = 'Valuta';
        Text010: TextConst DEU = 'Wechselkurs: %1/%2', ENU = 'Exchange rate: %1/%2';
        VALSpecLCYHeader: Text[80];
        VALExchRate: Text[50];
        CalculatedExchRate: Decimal;
        OutputNo: Integer;
        NNC_TotalLineAmount: Decimal;
        NNC_TotalAmountInclVat: Decimal;
        NNC_TotalInvDiscAmount: Decimal;
        NNC_TotalAmount: Decimal;
        [InDataSet]
        LogInteractionEnable: Boolean;
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.', ITA = 'Ns. num. di partita IVA';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        No1_SalesCrMemoHeaderCptnLbl: TextConst DEU = 'Rechnungskorrekturnr.', ENU = 'Credit Memo No.';
        SalesCrMemoHeaderPostDtCptnLbl: TextConst DEU = 'Buchungsdatum', ENU = 'Posting Date', ITA = 'Data della registrazione';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        UnitPriceCptnLbl: TextConst DEU = 'VK-Preis', ENU = 'Unit Price', ITA = 'Prezzo unit.';
        DiscountCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Discount %', ITA = 'Sconto %';
        AmountCptnLbl: TextConst DEU = 'Betrag', ENU = 'Amount', ITA = 'Importo';
        PostedReceiptDateCptnLbl: TextConst DEU = 'Gebuchtes Rücksendungsdatum', ENU = 'Posted Return Receipt Date';
        InvDiscAmt_SalesCrMemoLineCptnLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Invoice Discount Amount', ITA = 'sovrapprezzo';
        SubtotalCptnLbl: TextConst DEU = 'Zwischensumme', ENU = 'Subtotal';
        LineAmtInvDiscAmt_SalesCrMemoLineCptnLbl: TextConst DEU = 'Skonto auf MwSt.', ENU = 'Payment Discount on VAT';
        VATClausesCap: TextConst DEU = 'MwSt.-Klausel', ENU = 'VAT Clause';
        LineDimCptnLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        VATAmtLineVATCptnLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %';
        VATAmtLineVATBaseCptnLbl: TextConst DEU = 'MwSt.-Bemessungsgrundlage', ENU = 'VAT Base';
        VATAmtLineVATAmtCptnLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        VATAmtSpecificationCptnLbl: TextConst DEU = 'MwSt.-Betrag - Spezifikation', ENU = 'VAT Amount Specification';
        VATAmtLineVATIdentifierCptnLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        VATAmtLineInvDiscBaseAmtCptnLbl: TextConst DEU = 'Rechnungsrab.-Bem.grundlage', ENU = 'Invoice Discount Base Amount';
        VATAmtLineLineAmtCptnLbl: TextConst DEU = 'Zeilenbetrag', ENU = 'Line Amount';
        VATAmtLineInvoiceDiscAmtCptnLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Invoice Discount Amount';
        TotalCptnLbl: TextConst DEU = 'Gesamt', ENU = 'Total';
        ShiptoAddressCaptionLbl: TextConst DEU = 'Lieferadresse', ENU = 'Ship-to Address';
        CompanyInfoEmailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        CompanyInfoHomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date', ITA = 'Data';
        SalesCrMemoLineDiscCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Discount %', ITA = 'Sconto %';
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        Text011: TextConst DEU = 'Rechnungskorrektur %1 %2', ENU = 'Prepmt. Credit Memo %1 %2';
        OrderCaption: TextConst DEU = 'Auftragsnr.', ENU = 'Order No.';
        OrderNo: Code[20];
        Text5000000: TextConst DEU = 'der %1', ENU = 'of %1';
        Text5244801: TextConst DEU = 'Abschlagsrechnungskorrektur zu: ', ENU = 'Prepayment Credit Note to: ';
        Text5244802: TextConst DEU = 'Auftrag Nr. ', ENU = 'Sales Order No. ';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        SalesCrMemoHeader_BilltoCustomerNoCaptionLbl: TextConst DEU = 'Kundennr.', ENU = 'Customer No.', ITA = 'Vs codice cliente';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        SalesCrMemoLine_PositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.';
        VATAmountCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        VATIdentifierCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        UOM_SalesCrMemoLineCaption: TextConst DEU = 'Einheit', ENU = 'Unit', ITA = 'Unità di misura';
        "---NAI---": Integer;
        Cust: Record Customer;
        //Association: Record "OPP Association";
        ShowArchivLine: Boolean;
        ShowDiscount: Boolean;
        [InDataSet]
        ShowGLAccount: Boolean;
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ';
        CollecInvtxt: TextConst DEU = 'Sammelrech.Code', ENU = 'Combine Shp.Code', ITA = 'Codice della fattura cumulativa';
        OurAccountCaption: TextConst DEU = 'Lieferantennummer', ENU = 'Account No.', ITA = 'Ns cod. fornitore';
        LeistungsdatumCpt: TextConst DEU = 'Leistungsdatum', ENU = 'Delivery Date', ITA = 'Data della prestazione';
        SELLTOADDRESSCPT: TextConst DEU = 'Besteller', ENU = 'Purchaser', ITA = 'Cliente';
        UserLbl: TextConst DEU = 'Sachbearbeiter', ENU = 'Executive Officer', ITA = 'Addetto';
        TransportReasonLbl: TextConst DEU = 'Transportgrund', ENU = 'Transport Reason', ITA = 'Causale Del Transporto';
        TransportReasontxt: TextConst DEU = 'Verakuf', ENU = 'Sales', ITA = 'Reso';
        ShowOfficeSalesPerson: Boolean;
        LastCrMemoLineNo: Integer;
        CompanyAddressLine: Text[160];
        OrderTitle: Text[30];
        OrderTitleNo: Code[20];
        ItemTariffNo: Code[20];
        QuantityInBacklog: Decimal;
        ItemSerialNo: Code[20];
        ItemLotNo: Code[20];
        ItemExpirationDate: Date;
        ItemQuantity: Decimal;
        CustomerItemNo: Code[20];
        AssociationName: Text[50];
        //CustAssociationSortCode: Code[20];
        ShowSellToInfo: Boolean;

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractTmplCode(6) <> '';
    end;

    procedure FindPostedShipmentDate(): Date;
    var
        ReturnReceiptHeader: Record "Return Receipt Header";
        SalesShipmentBuffer2: Record "Sales Shipment Buffer" temporary;
    begin
        NextEntryNo := 1;
        if "Sales Cr.Memo Line"."Return Receipt No." <> '' then
            if ReturnReceiptHeader.GET("Sales Cr.Memo Line"."Return Receipt No.") then
                exit(ReturnReceiptHeader."Posting Date");
        if "Sales Cr.Memo Header"."Return Order No." = '' then
            exit("Sales Cr.Memo Header"."Posting Date");

        case "Sales Cr.Memo Line".Type of
            "Sales Cr.Memo Line".Type::Item:
                GenerateBufferFromValueEntry("Sales Cr.Memo Line");
            "Sales Cr.Memo Line".Type::"G/L Account", "Sales Cr.Memo Line".Type::Resource,
          "Sales Cr.Memo Line".Type::"Charge (Item)", "Sales Cr.Memo Line".Type::"Fixed Asset":
                GenerateBufferFromShipment("Sales Cr.Memo Line");
            "Sales Cr.Memo Line".Type::" ":
                exit(0D);
        end;

        SalesShipmentBuffer.RESET;
        SalesShipmentBuffer.SETRANGE("Document No.", "Sales Cr.Memo Line"."Document No.");
        SalesShipmentBuffer.SETRANGE("Line No.", "Sales Cr.Memo Line"."Line No.");

        if SalesShipmentBuffer.FIND('-') then begin
            SalesShipmentBuffer2 := SalesShipmentBuffer;
            if SalesShipmentBuffer.NEXT = 0 then begin
                SalesShipmentBuffer.GET(
                  SalesShipmentBuffer2."Document No.", SalesShipmentBuffer2."Line No.", SalesShipmentBuffer2."Entry No.");
                SalesShipmentBuffer.DELETE;
                exit(SalesShipmentBuffer2."Posting Date");
            end;
            SalesShipmentBuffer.CALCSUMS(Quantity);
            if SalesShipmentBuffer.Quantity <> "Sales Cr.Memo Line".Quantity then begin
                SalesShipmentBuffer.DELETEALL;
                exit("Sales Cr.Memo Header"."Posting Date");
            end;
        end else
            exit("Sales Cr.Memo Header"."Posting Date");
    end;

    procedure GenerateBufferFromValueEntry(SalesCrMemoLine2: Record "Sales Cr.Memo Line");
    var
        ValueEntry: Record "Value Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        TotalQuantity: Decimal;
        Quantity: Decimal;
    begin
        TotalQuantity := SalesCrMemoLine2."Quantity (Base)";
        ValueEntry.SETCURRENTKEY("Document No.");
        ValueEntry.SETRANGE("Document No.", SalesCrMemoLine2."Document No.");
        ValueEntry.SETRANGE("Posting Date", "Sales Cr.Memo Header"."Posting Date");
        ValueEntry.SETRANGE("Item Charge No.", '');
        ValueEntry.SETFILTER("Entry No.", '%1..', FirstValueEntryNo);
        if ValueEntry.FIND('-') then
            repeat
                if ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") then begin
                    if SalesCrMemoLine2."Qty. per Unit of Measure" <> 0 then
                        Quantity := ValueEntry."Invoiced Quantity" / SalesCrMemoLine2."Qty. per Unit of Measure"
                    else
                        Quantity := ValueEntry."Invoiced Quantity";
                    AddBufferEntry(
                      SalesCrMemoLine2,
                      -Quantity,
                      ItemLedgerEntry."Posting Date");
                    TotalQuantity := TotalQuantity - ValueEntry."Invoiced Quantity";
                end;
                FirstValueEntryNo := ValueEntry."Entry No." + 1;
            until (ValueEntry.NEXT = 0) or (TotalQuantity = 0);
    end;

    procedure GenerateBufferFromShipment(SalesCrMemoLine: Record "Sales Cr.Memo Line");
    var
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        SalesCrMemoLine2: Record "Sales Cr.Memo Line";
        ReturnReceiptHeader: Record "Return Receipt Header";
        ReturnReceiptLine: Record "Return Receipt Line";
        TotalQuantity: Decimal;
        Quantity: Decimal;
    begin
        TotalQuantity := 0;
        SalesCrMemoHeader.SETCURRENTKEY("Return Order No.");
        SalesCrMemoHeader.SETFILTER("No.", '..%1', "Sales Cr.Memo Header"."No.");
        SalesCrMemoHeader.SETRANGE("Return Order No.", "Sales Cr.Memo Header"."Return Order No.");
        if SalesCrMemoHeader.FIND('-') then
            repeat
                SalesCrMemoLine2.SETRANGE("Document No.", SalesCrMemoHeader."No.");
                SalesCrMemoLine2.SETRANGE("Line No.", SalesCrMemoLine."Line No.");
                SalesCrMemoLine2.SETRANGE(Type, SalesCrMemoLine.Type);
                SalesCrMemoLine2.SETRANGE("No.", SalesCrMemoLine."No.");
                SalesCrMemoLine2.SETRANGE("Unit of Measure Code", SalesCrMemoLine."Unit of Measure Code");
                if SalesCrMemoLine2.FIND('-') then
                    repeat
                        TotalQuantity := TotalQuantity + SalesCrMemoLine2.Quantity;
                    until SalesCrMemoLine2.NEXT = 0;
            until SalesCrMemoHeader.NEXT = 0;

        ReturnReceiptLine.SETCURRENTKEY("Return Order No.", "Return Order Line No.");
        ReturnReceiptLine.SETRANGE("Return Order No.", "Sales Cr.Memo Header"."Return Order No.");
        ReturnReceiptLine.SETRANGE("Return Order Line No.", SalesCrMemoLine."Line No.");
        ReturnReceiptLine.SETRANGE("Line No.", SalesCrMemoLine."Line No.");
        ReturnReceiptLine.SETRANGE(Type, SalesCrMemoLine.Type);
        ReturnReceiptLine.SETRANGE("No.", SalesCrMemoLine."No.");
        ReturnReceiptLine.SETRANGE("Unit of Measure Code", SalesCrMemoLine."Unit of Measure Code");
        ReturnReceiptLine.SETFILTER(Quantity, '<>%1', 0);

        if ReturnReceiptLine.FIND('-') then
            repeat
                if "Sales Cr.Memo Header"."Get Return Receipt Used" then
                    CorrectShipment(ReturnReceiptLine);
                if ABS(ReturnReceiptLine.Quantity) <= ABS(TotalQuantity - SalesCrMemoLine.Quantity) then
                    TotalQuantity := TotalQuantity - ReturnReceiptLine.Quantity
                else begin
                    if ABS(ReturnReceiptLine.Quantity) > ABS(TotalQuantity) then
                        ReturnReceiptLine.Quantity := TotalQuantity;
                    Quantity :=
                      ReturnReceiptLine.Quantity - (TotalQuantity - SalesCrMemoLine.Quantity);

                    SalesCrMemoLine.Quantity := SalesCrMemoLine.Quantity - Quantity;
                    TotalQuantity := TotalQuantity - ReturnReceiptLine.Quantity;

                    if ReturnReceiptHeader.GET(ReturnReceiptLine."Document No.") then
                        AddBufferEntry(
                          SalesCrMemoLine,
                          -Quantity,
                          ReturnReceiptHeader."Posting Date");
                end;
            until (ReturnReceiptLine.NEXT = 0) or (TotalQuantity = 0);
    end;

    procedure CorrectShipment(var ReturnReceiptLine: Record "Return Receipt Line");
    var
        SalesCrMemoLine: Record "Sales Cr.Memo Line";
    begin
        SalesCrMemoLine.SETCURRENTKEY("Return Receipt No.", "Return Receipt Line No.");
        SalesCrMemoLine.SETRANGE("Return Receipt No.", ReturnReceiptLine."Document No.");
        SalesCrMemoLine.SETRANGE("Return Receipt Line No.", ReturnReceiptLine."Line No.");
        if SalesCrMemoLine.FIND('-') then
            repeat
                ReturnReceiptLine.Quantity := ReturnReceiptLine.Quantity - SalesCrMemoLine.Quantity;
            until SalesCrMemoLine.NEXT = 0;
    end;

    procedure AddBufferEntry(SalesCrMemoLine: Record "Sales Cr.Memo Line"; QtyOnShipment: Decimal; PostingDate: Date);
    begin
        SalesShipmentBuffer.SETRANGE("Document No.", SalesCrMemoLine."Document No.");
        SalesShipmentBuffer.SETRANGE("Line No.", SalesCrMemoLine."Line No.");
        SalesShipmentBuffer.SETRANGE("Posting Date", PostingDate);
        if SalesShipmentBuffer.FIND('-') then begin
            SalesShipmentBuffer.Quantity := SalesShipmentBuffer.Quantity - QtyOnShipment;
            SalesShipmentBuffer.MODIFY;
            exit;
        end;

        with SalesShipmentBuffer do begin
            INIT;
            "Document No." := SalesCrMemoLine."Document No.";
            "Line No." := SalesCrMemoLine."Line No.";
            "Entry No." := NextEntryNo;
            Type := SalesCrMemoLine.Type;
            "No." := SalesCrMemoLine."No.";
            Quantity := -QtyOnShipment;
            "Posting Date" := PostingDate;
            INSERT;
            NextEntryNo := NextEntryNo + 1;
        end;
    end;

    local procedure DocumentCaption(): Text[250];
    begin
        if "Sales Cr.Memo Header"."Prepayment Credit Memo" then
            exit(Text011);
        exit(Text005);
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    local procedure IsDiscount(SalesCrMemoHeaderDisc: Record "Sales Cr.Memo Header"): Boolean;
    var
        SalesCrMemoLineDisc: Record "Sales Cr.Memo Line";
    begin
        // >>> 2111-23178 31-07-2014 UQ
        SalesCrMemoLineDisc.SETRANGE("Document No.", SalesCrMemoHeaderDisc."No.");
        SalesCrMemoLineDisc.SETFILTER("Line Discount %", '<>0');
        exit(not SalesCrMemoLineDisc.ISEMPTY);
        // <<< 2111-23178 31-07-2014 UQ
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-47914 21-09-2016 MMA
        with "Sales Cr.Memo Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-47914 21-09-2016 MMA
    end;
}

