report 50003 "MMC Blanket Sales Order AR"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report/Blanket Sales Order NAI.rdl';

    CaptionML = DEU = 'Verkauf Rahmenauftrag',
                ENU = 'Blanket Sales Order';

    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            DataItemTableView = SORTING("Document Type", "No.") WHERE("Document Type" = CONST("Blanket Order"));
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Rahmenauftrag',
                                     ENU = 'Blanket Sales Order';
            column(ShipmentMethodDescription; ShipmentMethod.Description)
            {
            }
            column(PaymentTermsDescription; PaymentTerms.Description)
            {
            }
            column(No_SalesHeader; "No.")
            {
            }
            column(PaymentTermsCaption; PaymentTermsCaptionLbl)
            {
            }
            column(ShipmentMethodCaption; ShipmentMethodCaptionLbl)
            {
            }
            column(InvDiscAmountCaption; InvDiscAmountCaptionLbl)
            {
            }
            column(VATPercentCaption; VATPercentCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmountCaption; VATAmountCaptionLbl)
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }
            column(LanguageCode; "Language Code")
            {
                Description = '2111-62185 30-01-2018 BD';
            }
            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(BlanketSalesOrderCopyText; STRSUBSTNO(Text004, "Sales Header"."No.", CopyText))
                    {
                    }
                    column(Text5000000_SalesHeaderNo; STRSUBSTNO(Text5000000, "Sales Header"."No."))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesHeader; "Sales Header"."Bill-to Customer No.")
                    {
                    }
                    column(DocDate_SalesHeader; FORMAT("Sales Header"."Document Date", 0, 4))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesHeader; "Sales Header"."VAT Registration No.")
                    {
                    }
                    column(ShipmentDate_SalesHeader; FORMAT("Sales Header"."Shipment Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesHeader; "Sales Header"."Your Reference")
                    {
                    }
                    column(SalesHeaderExternalDocumentNo; "Sales Header"."External Document No.")
                    {
                    }
                    column(SalesHeaderExternalDocumentNoCaption; "Sales Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(PricesIncVAT_SalesHeader; "Sales Header"."Prices Including VAT")
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text005, ''))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PricesIncVAT1_SalesHeader; FORMAT("Sales Header"."Prices Including VAT"))
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(AmountCaption; AmountCaptionLbl)
                    {
                    }
                    column(UnitPriceCaption; UnitPriceCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(BlanketSalesOrderNoCaption; BlanketSalesOrderNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoHomePageCaption; CompanyInfoHomePageCaptionLbl)
                    {
                    }
                    column(CompanyInfoEmailCaption; CompanyInfoEmailCaptionLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesHeaderCaption; SalesHeaderBilltoCustomerNoCaptionLbl)
                    {
                    }
                    column(PricesIncVAT_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }

                    column(ShipmentCode; "Sales Header"."Shipping Agent Code") { }
                    column(ShipmentCodeCpt; ShimentCodetxt) { }
                    column(Validto; "Sales Header"."Valid To") { }
                    column(validtocpt; validtotxt) { }
                    column(Lieferantennummer; Cust."Our Account No.") { }
                    column(OurAccounttxt; OurAccountCaption) { }
                    column(Alwaysuse; Alwaysusetxt) { }
                    column(UserName; User."Full Name") { }
                    column(UserCpt; userlbl) { }
                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FINDSET then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1, %2 %3', DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Extended Text - Sales")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.") WHERE("Assigned for" = CONST("Blanket Order"), "Text Position" = CONST(Pretext));
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(36), "Print Attribute" = CONST(true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }
                    }
                    dataitem("Sales Line"; "Sales Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                        trigger OnPreDataItem();
                        begin
                            CurrReport.BREAK;
                        end;
                    }
                    dataitem(RoundLoop; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(SalesLineTypeInt; SalesLineTypeInt)
                        {
                        }
                        column(VATBaseDisc_SalesHeader; "Sales Header"."VAT Base Discount %")
                        {
                        }
                        column(TotalSalesInvDiscAmount; TotalSalesInvDiscAmount)
                        {
                        }
                        column(TotalSalesLineAmount; TotalSalesLineAmount)
                        {
                        }
                        column(LineNo_SalesLine; "Sales Line"."Line No.")
                        {
                        }
                        column(SalesLineDescription; SalesLine.Description)
                        {
                        }
                        column(No_SalesLine; "Sales Line"."No.")
                        {
                        }
                        column(No_SalesLineCaption; "Sales Line".FIELDCAPTION("No."))
                        {
                        }
                        column(DescriptionRL_SalesLine; "Sales Line".Description)
                        {
                        }
                        column(Quantity_SalesLine; "Sales Line".Quantity)
                        {
                        }
                        column(UnitofMeasure_SalesLine; "Sales Line"."Unit of Measure")
                        {
                        }
                        column(LineAmountRL_SalesLine; "Sales Line"."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(UnitPrice_SalesLine; "Sales Line"."Unit Price")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 2;
                        }
                        column(ShipmentDate_SalesLine; FORMAT("Sales Line"."Shipment Date"))
                        {
                            AutoFormatType = 1;
                        }
                        column(VATIdentifier_SalesLine; "Sales Line"."VAT Identifier")
                        {
                        }
                        column(SalesLineInvDiscountAmt; -SalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Line"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(SalesLineLnAmtInvDiscAmt; SalesLine."Line Amount" - SalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmountText; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(VATAmount; VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(SlLnLnAmtInvDiscAmtVATAmt; SalesLine."Line Amount" - SalesLine."Inv. Discount Amount" + VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATDiscountAmount; -VATDiscountAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseAmount; VATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(PaymentDiscountCaption; PaymentDiscountCaptionLbl)
                        {
                        }
                        column(DescriptionRL_SalesLineCaption; "Sales Line".FIELDCAPTION(Description))
                        {
                        }
                        column(Quantity_SalesLineCaption; "Sales Line".FIELDCAPTION(Quantity))
                        {
                        }
                        column(UnitofMeasure_SalesLineCaption; "Sales Line".FIELDCAPTION("Unit of Measure"))
                        {
                        }
                        column(VATIdentifier_SalesLineCaption; "Sales Line".FIELDCAPTION("VAT Identifier"))
                        {
                        }
                        column(PositionNo_SalesLine; "Sales Line"."Position No.")
                        {
                        }
                        column(QuantityWp_SalesLine; 0.0)
                        {
                        }
                        column(SAV; LineAmtVar) { }
                        column(QtyAB; QtyABvar) { }
                        column(UPV; UnitPriceVar) { }
                        column(LDV; LineDiscVar) { }
                        column(Sales_Line___Line_Discount___Caption; Sales_Line___Line_Discount___CaptionLbl)
                        {
                        }
                        column(PositionNo_SalesLineCaption; SalesLinePositionNoCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesLineCaption; '')
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(AsmInfoExistsForLine; AsmInfoExistsForLine)
                        {
                        }
                        column(SalesLine_OutstandingQuantity; "Sales Line"."Outstanding Quantity")
                        {
                        }
                        column(SalesLine_OutstandingQuantityCaption; "Sales Line".FIELDCAPTION("Outstanding Quantity"))
                        {
                            Description = '2111-62234 14-03-2018 RM';
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        column(PlannedDelweekyear; DeliveryWeekYear) { }
                        column(ExpectedWeekNumberCaption; ExpectedWeekNumberCaptionLbl) { }
                        dataitem("Reservation Entry"; "Reservation Entry")
                        {
                            DataItemLink = "Source ID" = FIELD("Document No."), "Source Ref. No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = WHERE("Source Type" = CONST(37));
                            column(ResExpirationDate; FORMAT("Reservation Entry"."Expiration Date"))
                            {
                            }
                            column(ResSerialNo; "Reservation Entry"."Serial No.")
                            {
                            }
                            column(ResLotNo; "Reservation Entry"."Lot No.")
                            {
                            }
                            column(ResQuantity; ABS("Reservation Entry".Quantity))
                            {
                            }
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText1; DimText)
                            {
                            }
                            column(LineDimensionsCaption; LineDimensionsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FIND('-') then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(AsmLoop; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(KitPosNo; '')
                            {
                            }
                            column(AsmLineType; AsmLine.Type)
                            {
                            }
                            column(AsmLineNo; BlanksForIndent + AsmLine."No.")
                            {
                            }
                            column(AsmLineDescription; BlanksForIndent + AsmLine.Description)
                            {
                            }
                            column(AsmLineQuantity; AsmLine.Quantity)
                            {
                            }
                            column(AsmLineUOMText; GetUnitOfMeasureDescr(AsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                // >>> 2111-49001 16-02-2017 RM
                                if Number = 1 then
                                    AsmLine.FINDSET
                                else
                                    AsmLine.NEXT;
                                // <<< 2111-49001 16-02-2017 RM
                            end;

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 16-02-2017 RM
                                if not AsmInfoExistsForLine then
                                    CurrReport.BREAK;

                                AsmLine.SETRANGE("Document Type", AsmHeader."Document Type");
                                AsmLine.SETRANGE("Document No.", AsmHeader."No.");
                                SETRANGE(Number, 1, AsmLine.COUNT);
                                // <<< 2111-49001 16-02-2017 RM
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("Document No."), "Source Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(37), "Print Attribute" = CONST(true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                        begin
                            if Number = 1 then
                                SalesLine.FIND('-')
                            else
                                SalesLine.NEXT;
                            "Sales Line" := SalesLine;

                            // >>> 2111-49001 16-02-2017 RM
                            AsmInfoExistsForLine := SalesLine.AsmToOrderExists(AsmHeader);
                            // <<< 2111-49001 16-02-2017 RM

                            if not "Sales Header"."Prices Including VAT" and
                               (SalesLine."VAT Calculation Type" = SalesLine."VAT Calculation Type"::"Full VAT")
                            then
                                SalesLine."Line Amount" := 0;

                            if (SalesLine.Type = SalesLine.Type::"G/L Account") and (not ShowInternalInfo) then
                                "Sales Line"."No." := '';

                            SalesLineTypeInt := SalesLine.Type;
                            TotalSalesLineAmount += SalesLine."Line Amount";
                            TotalSalesInvDiscAmount += SalesLine."Inv. Discount Amount";
                            //>>> 02.10.19   MMC OP

                            begin
                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    UnitPriceVar := '' else
                                    UnitPriceVar := Format("Sales Line"."Unit Price");

                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    LineDiscVar := '' else
                                    LineDiscVar := Format("Sales Line"."Line Discount %");

                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    LineAmtVar := '' else
                                    LineAmtVar := Format("Sales Line"."Line Amount");

                                if "Sales Line"."Assembly BOM exploded" = true then
                                    LineDiscVar := '';


                                if "Sales Line"."Assembly BOM exploded" = true then
                                    LineAmtVar := FORMAT(("Sales Line".Quantity * "Sales Line"."Unit Price"), 0, '<Precision,2><sign><Integer Thousand><Decimals,3>');


                                if "Sales Line".Description = 'SET Promotion' then
                                    QtyABvar := Format("Sales Line".Quantity) else
                                    QtyABvar := Format("Sales Line".Quantity);


                            end;

                            if "Sales Line".Type = 2 then begin
                                DeliveryWeek := DATE2DWY("Sales Line"."Planned Delivery Date", 2);
                                DeliveryYear := DATE2DWY("Sales Line"."Planned Delivery Date", 3);
                                DeliveryYearShort := FORMAT(DeliveryYear);
                                DeliveryYearShort := DELSTR(DeliveryYearShort, 1, 2);
                                DeliveryWeekYear := STRSUBSTNO(TEXTDWY, DeliveryWeek, DeliveryYearShort);
                            END;


                            //<<< 02.10.19   MMC OP

                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // <<< 2111-47914 21-09-2016 MMA

                            // >>> 2111-34284 24-11-2015 SMS
                            ItemTariffNo := '';
                            if "Sales Line".Type = "Sales Line".Type::Item then begin
                                ItemRec.GET("Sales Line"."No.");
                                ItemTariffNo := ItemRec."Tariff No.";
                                // >>> 2111-47914 21-09-2016 MMA
                                GetItemCrossReference;
                                // <<< 2111-47914 21-09-2016 MMA
                            end;
                            // <<< 2111-34284 24-11-2015 SMS
                        end;

                        trigger OnPostDataItem();
                        begin
                            SalesLine.DELETEALL;
                        end;

                        trigger OnPreDataItem();
                        begin
                            MoreLines := SalesLine.FIND('+');
                            while MoreLines and (SalesLine.Description = '') and (SalesLine."Description 2" = '') and
                                  (SalesLine."No." = '') and (SalesLine.Quantity = 0) and
                                  (SalesLine.Amount = 0)
                            do
                                MoreLines := SalesLine.NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SalesLine.SETRANGE("Line No.", 0, SalesLine."Line No.");
                            SETRANGE(Number, 1, SalesLine.COUNT);
                            CurrReport.CREATETOTALS(SalesLine."Line Amount", SalesLine."Inv. Discount Amount");

                            TotalSalesLineAmount := 0;
                            TotalSalesInvDiscAmount := 0;
                        end;
                    }
                    dataitem(VATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmountLineVATBase; VATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineVATAmount; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineLineAmount; VATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; VATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscountAmt; VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmountLineVAT; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATAmtSpecificationCaption; VATAmtSpecificationCaptionLbl)
                        {
                        }
                        column(VATIdentifierCaption; VATIdentifierCaptionLbl)
                        {
                        }
                        column(InvDiscBaseAmountCaption; InvDiscBaseAmountCaptionLbl)
                        {
                        }
                        column(LineAmountCaption; LineAmountCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            if VATAmount = 0 then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(
                              VATAmountLine."Line Amount", VATAmountLine."Inv. Disc. Base Amount",
                              VATAmountLine."Invoice Discount Amount", VATAmountLine."VAT Base", VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATCounterLCY; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercentage; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifierVCL; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATIdentifierVCLCaption; VATIdentifierVCLCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                            VALVATBaseLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  WORKDATE, "Sales Header"."Currency Code", VATAmountLine."VAT Base", "Sales Header"."Currency Factor"));
                            VALVATAmountLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  WORKDATE, "Sales Header"."Currency Code", VATAmountLine."VAT Amount", "Sales Header"."Currency Factor"));
                        end;

                        trigger OnPreDataItem();
                        begin
                            if (not GLSetup."Print VAT specification in LCY") or
                               ("Sales Header"."Currency Code" = '') or
                               (VATAmountLine.GetTotalVATAmount = 0)
                            then
                                CurrReport.BREAK;

                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VALVATBaseLCY, VALVATAmountLCY);

                            if GLSetup."LCY Code" = '' then
                                VALSpecLCYHeader := Text007 + Text008
                            else
                                VALSpecLCYHeader := Text007 + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency(WORKDATE, "Sales Header"."Currency Code", 1);
                            VALExchRate := STRSUBSTNO(Text009, CurrExchRate."Relational Exch. Rate Amount", CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(Total; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesHeader; "Sales Header"."Sell-to Customer No.")
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShiptoAddressCaption; ShiptoAddressCaptionLbl)
                        {
                        }
                        column(SelltoCustNo_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowShippingAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Extended Text - Sales")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.") WHERE("Assigned for" = CONST("Blanket Order"), "Text Position" = CONST(Posttext));
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }
                    }
                }

                trigger OnAfterGetRecord();
                var
                    SalesPost: Codeunit "Sales-Post";
                begin
                    CLEAR(SalesLine);
                    CLEAR(SalesPost);
                    SalesLine.DELETEALL;
                    VATAmountLine.DELETEALL;
                    SalesPost.GetSalesLines("Sales Header", SalesLine, 0);
                    SalesLine.CalcVATAmountLines(0, "Sales Header", SalesLine, VATAmountLine);
                    SalesLine.UpdateVATOnLines(0, "Sales Header", SalesLine, VATAmountLine);
                    VATAmount := VATAmountLine.GetTotalVATAmount;
                    VATBaseAmount := VATAmountLine.GetTotalVATBase;
                    VATDiscountAmount :=
                      VATAmountLine.GetTotalVATDiscount("Sales Header"."Currency Code", "Sales Header"."Prices Including VAT");
                    TotalAmountInclVAT := VATAmountLine.GetTotalAmountInclVAT;

                    if Number > 1 then begin
                        CopyText := Text003;
                        OutputNo += 1;
                    end;
                    CurrReport.PAGENO := 1;
                end;

                trigger OnPostDataItem();
                begin
                    if not CurrReport.PREVIEW then
                        SalesCountPrinted.RUN("Sales Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);

                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");

                CompanyInfo.GET;

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 16-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, MAXSTRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 16-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 16-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 16-02-2017 RM
                end;

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                // >>> 2111-49001 16-02-2017 RM
                if ShowOfficeSalesperson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 16-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 16-02-2017 RM
                end;
                // <<< 2111-49001 16-02-2017 RM
                //>>> 01.10.19 MMC OP
                if not Cust.GET("Bill-to Customer No.") then
                    CLEAR(Cust);
                User.RESET;
                User.SETRANGE("User Name", "Sales Header"."Assigned User ID");
                IF NOT User.FINDFIRST THEN
                    User.INIT;
                //<<< 01.10.19 MMC OP

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                if "VAT Registration No." = '' then
                    VATNoText := ''
                else
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                if "Currency Code" = '' then begin
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006, GLSetup."LCY Code");
                end else begin
                    TotalText := STRSUBSTNO(Text001, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006, "Currency Code");
                end;
                FormatAddr.SalesHeaderBillTo(CustAddr, "Sales Header");

                if "Payment Terms Code" = '' then
                    PaymentTerms.INIT
                else begin
                    PaymentTerms.GET("Payment Terms Code");
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                end;
                if "Shipment Method Code" = '' then
                    ShipmentMethod.INIT
                else begin
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                end;

                FormatAddr.SalesHeaderShipTo(ShipToAddr, CustAddr, "Sales Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                for i := 1 to ARRAYLEN(ShipToAddr) do
                    if ShipToAddr[i] <> CustAddr[i] then
                        ShowShippingAddr := true;

                if not CurrReport.PREVIEW then begin

                    if LogInteraction then begin
                        CALCFIELDS("No. of Archived Versions");
                        if "Bill-to Contact No." <> '' then
                            SegManagement.LogDocument(
                              2, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Contact, "Bill-to Contact No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.")
                        else
                            SegManagement.LogDocument(
                              2, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Customer, "Bill-to Customer No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.");
                    end;
                end;
            end;

            trigger OnPreDataItem();
            var
                "---NAI---": Integer;
                PdfReportMgmt: Codeunit "NAI - Pdf Report Mgmt.";
            begin
                // >>> 2111-49001 16-02-2017 RM
                AsmInfoExistsForLine := false;
                // <<< 2111-49001 16-02-2017 RM

                // >>> 2111-30349 27-03-2015 RW
                if FINDFIRST then begin
                    PdfReportMgmt.CheckDocumentArchiving("Print as PDF", 1, "Document Type", ArchiveDocument);
                    "Print as PDF" := false;
                    MODIFY;
                end;
                // <<< 2111-30349 27-03-2015 RW
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                    }
                    field(ShowOfficeSalesperson; ShowOfficeSalesperson)
                    {
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
        end;

        trigger OnOpenPage();
        begin
            LogInteraction := SegManagement.FindInteractTmplCode(2) <> '';

            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        GLSetup.GET;
        SalesSetup.GET;
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'Total %1', ENU = 'Total %1';
        Text002: TextConst DEU = 'Total %1 inkl. MwSt.', ENU = 'Total %1 Incl. VAT';
        Text003: TextConst DEU = 'KOPIE', ENU = ' COPY';
        Text004: TextConst DEU = 'Rahmenauftrag %1 %2', ENU = 'Blanket Sales Order %1 %2', ITA = 'Conferma ordine a chiamata %1 %2';
        Text005: TextConst DEU = 'Seite %1', ENU = 'Page %1';
        Text006: TextConst DEU = 'Total %1 ohne MwSt.', ENU = 'Total %1 Excl. VAT';
        GLSetup: Record "General Ledger Setup";
        Cust: Record Customer;
        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        VATAmountLine: Record "VAT Amount Line" temporary;
        SalesLine: Record "Sales Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        Language: Record Language;
        RespCenter: Record "Responsibility Center";
        CurrExchRate: Record "Currency Exchange Rate";
        User: Record User;
        SalesCountPrinted: Codeunit "Sales-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        SalesLineTypeInt: Integer;
        OutputNo: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        LogInteraction: Boolean;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        TotalSalesLineAmount: Decimal;
        TotalSalesInvDiscAmount: Decimal;
        VALSpecLCYHeader: Text[80];
        VALExchRate: Text[50];
        Text007: TextConst DEU = 'MwSt.-Betrag Spezifikation in ', ENU = 'VAT Amount Specification in ';
        Text008: TextConst DEU = 'Landeswährung', ENU = 'Local Currency';
        Text009: TextConst DEU = 'Wechselkurs: %1/%2', ENU = 'Exchange rate: %1/%2';
        ArchiveDocument: Boolean;
        [InDataSet]
        LogInteractionEnable: Boolean;
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.', ITA = 'Ns. num. di partita IVA';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        ShipmentDateCaptionLbl: TextConst DEU = 'Warenausg.-Datum', ENU = 'Shipment Date';
        BlanketSalesOrderNoCaptionLbl: TextConst DEU = 'Rahmenauftragsnr.', ENU = 'Blanket Sales Order No.', ITA = 'Num.ordine a chiamata';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        UnitPriceCaptionLbl: TextConst DEU = 'VK-Preis', ENU = 'Unit Price', ITA = 'prezzo unit.';
        AmountCaptionLbl: TextConst DEU = 'Betrag', ENU = 'Amount', ITA = 'Importo';
        InvDiscAmountCaptionLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Invoice Discount Amount';
        SubtotalCaptionLbl: TextConst DEU = 'Zwischensumme', ENU = 'Subtotal';
        PaymentDiscountCaptionLbl: TextConst DEU = 'Skonto auf MwSt.', ENU = 'Payment Discount on VAT';
        LineDimensionsCaptionLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        VATPercentCaptionLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %';
        VATBaseCaptionLbl: TextConst DEU = 'MwSt.-Bemessungsgrundlage', ENU = 'VAT Base';
        VATAmountCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        VATAmtSpecificationCaptionLbl: TextConst DEU = 'MwSt.-Betrag - Spezifikation', ENU = 'VAT Amount Specification';
        VATIdentifierCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        InvDiscBaseAmountCaptionLbl: TextConst DEU = 'Rechnungsrab.-Bem.grundlage', ENU = 'Invoice Discount Base Amount';
        LineAmountCaptionLbl: TextConst DEU = 'Zeilenbetrag', ENU = 'Line Amount';
        TotalCaptionLbl: TextConst DEU = 'Gesamt', ENU = 'Total';
        VATIdentifierVCLCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        PaymentTermsCaptionLbl: TextConst DEU = 'Zahlungsbedingungen', ENU = 'Payment Terms', ITA = 'Cond. di pag.';
        ShipmentMethodCaptionLbl: TextConst DEU = 'Lieferbedingung', ENU = 'Shipment Method', ITA = 'Mod. di consegna';
        ShiptoAddressCaptionLbl: TextConst DEU = 'Lieferadresse', ENU = 'Ship-to Address', ITA = 'Indrizzo di consegna';
        "---NAI---": Integer;
        CompanyInfo2: Record "Company Information";
        AsmHeader: Record "Assembly Header";
        AsmLine: Record "Assembly Line";
        AsmInfoExistsForLine: Boolean;
        ShowOfficeSalesperson: Boolean;
        ItemTariffNo: Code[20];
        CustomerItemNo: Code[20];
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        CompanyInfoHomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        CompanyInfoEmailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date', ITA = 'Data';
        Text5000000: TextConst DEU = 'der Rahmenauftragsbestätigung %1', ENU = 'of Blanket Sales Order Confirmation %1';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        SalesHeaderBilltoCustomerNoCaptionLbl: TextConst DEU = 'Kundennr.', ENU = 'Customer No.', ITA = 'Vs codice cliente';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        SalesLinePositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.';
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ';
        CompanyAddressLine: Text[160];
        ShimentCodetxt: TextConst DEU = 'Transportart', ENU = 'Shipment Code', ITA = 'Forma di Transporto';
        validtotxt: TextConst DEU = 'Gültig bis:', ENU = 'Valid to:', ITA = 'Validità fino al:';
        OurAccountCaption: TextConst DEU = 'Lieferantennummer', ENU = 'Account No.', ITA = 'Ns cod. fornitore';
        TEXTDWY: TextConst DEU = '%1.%2', ENU = '%1.%2', ITA = '%1.%2';
        ExpectedWeekNumberCaptionLbl: TextConst DEU = 'LW.JJ', ENU = 'DW.YY', ITA = 'sett.cons.';

        Alwaysusetxt: TextConst DEU = '(bitte immer angeben)', ENU = '(please always indicate)', ITA = '(da indicare)';
        UserLbl: TextConst DEU = 'Sachbearbeiter', ENU = 'Executive Officer', ITA = 'Addetto';

        DeliveryWeek: Integer;
        DeliveryYear: Integer;
        DeliveryYearShort: Text[10];
        DeliveryWeekYear: Text[10];
        LineAmtVar: text[20];
        LineDiscVar: text[20];
        UnitPriceVar: text[20];
        QtyABvar: Text[20];
        Sales_Line___Line_Discount___CaptionLbl: TextConst DEU = 'Rab. %', ENU = 'Disc. %', ITA = 'sconto %';
        UPV: Text[20];
        LDV: Text[20];
        SAV: Text[20];

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewArchiveDocument: Boolean; NewLogInteraction: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[10];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        if not UnitOfMeasure.GET(UOMCode) then
            exit(UOMCode);
        exit(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        exit(PADSTR('', 2, ' '));
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-47914 21-09-2016 MMA
        with "Sales Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-47914 21-09-2016 MMA
    end;
}

