report 50004 "MMC Sales - Shipment AR"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report/Sales - Shipment NAI.rdl';

    CaptionML = DEU = 'Verkauf - Lieferschein',
                ENU = 'Sales - Shipment';

    dataset
    {
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING ("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Geb. Verkaufslieferung',
                                     ENU = 'Posted Sales Shipment';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(SumNetWeightCaption; SumNetWeightLbl)
            {
            }
            column(SumGrossWeightCaption; SumGrossWeightLbl)
            {
            }
            column(QuantityPackageCaption; QuantityPackageLbl)
            {
            }
            column(QuantityPalletLabel; QuantityPalletLbl)
            {
            }
            column(ShowItemTrackBarCode; ShowItemTrackBarCode)
            {
            }
            column(TotalNetWeight; TotalNetWeight)
            {
            }
            column(TotalGrossWeight; TotalGrossWeight)
            {
            }
            column(LanguageCode; "Language Code")
            {
            }
            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING (Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING (Number) WHERE (Number = CONST (1));
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(SalesShptCopyText; STRSUBSTNO(Text002, LSNO, CopyText))
                    {
                    }
                    column(Text5000000_SalesShptHeaderNo; STRSUBSTNO(Text5000000, "Sales Shipment Header"."No."))
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text003, ''))
                    {
                    }
                    column(ShipToAddr1; ShipToAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(ShipToAddr2; ShipToAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(ShipToAddr3; ShipToAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(ShipToAddr4; ShipToAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(ShipToAddr5; ShipToAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(ShipToAddr6; ShipToAddr[6])
                    {
                    }
                    column(SellTo_Name; "Sales Shipment Header"."Sell-to Customer Name")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name2; "Sales Shipment Header"."Sell-to Customer Name 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name3; "Sales Shipment Header"."Sell-to Customer Name 3")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SellTo_Name4; "Sales Shipment Header"."Sell-to Customer Name 4")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SellTo_Address; "Sales Shipment Header"."Sell-to Address")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Address2; "Sales Shipment Header"."Sell-to Address 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Postcode; "Sales Shipment Header"."Sell-to Post Code")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_City; "Sales Shipment Header"."Sell-to City")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(SelltoCustNo_SalesShptHeader; "Sales Shipment Header"."Sell-to Customer No.")
                    {
                    }
                    column(DocDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Document Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(VATRegNo_SalesShptHeader; "Sales Shipment Header"."VAT Registration No.")
                    {
                    }
                    column(VATRegNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("VAT Registration No."))
                    {
                    }
                    column(PostedWhseShipHeader_TrackingNumber; '')
                    {
                    }
                    column(PostedWhseShipHeader_TrackingNumberCaption; '')
                    {
                    }
                    column(SalesShptHeader_ExternalDocumentNo; "Sales Shipment Header"."External Document No.")
                    {
                    }
                    column(SalesShptHeader_ExternalDocumentNoCaption; "Sales Shipment Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesShptHeader; "Sales Shipment Header"."Your Reference")
                    {
                    }
                    column(ShipToAddr7; ShipToAddr[7])
                    {
                    }
                    column(ShipToAddr8; ShipToAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(ShptDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Shipment Date"))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(AccountNoCaption; AccountNoCaptionLbl)
                    {
                    }
                    column(ShipmentNoCaption; ShipmentNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(CompanyInfoHomePageCaption; CompanyInfoHomePageCaptionLbl)
                    {
                    }
                    column(CompanyInfoEmailCaption; CompanyInfoEmailCaptionLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(SelltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoGLN; CompanyInfo.GLN)
                    {
                        IncludeCaption = true;
                    }
                    column(Text004; Text004)
                    {
                    }
                    column(Signature; SignatureLbl)
                    {
                    }
                    column(ShowPackingList; false)
                    {
                    }
                    column(ShowPalettAssignmentToItem; false)
                    {
                    }
                    column(PackList; '')
                    {
                    }
                    column(PaletteninformationenCaption; '')
                    {
                    }
                    column(Kistennr_Caption; '')
                    {
                    }
                    column(SalesShipmentLineItemsNoCaption; '')
                    {
                    }
                    column(SalesShipmentLineItemsDescriptionCaption; '')
                    {
                    }
                    column(SalesShipmentLineItemsQuantityCaption; '')
                    {
                    }
                    column(SalesShipmentLineItemsUnitOfMeasureCaption; '')
                    {
                    }
                    column(GroupPalettTrackingSpecBufferSerialNoCaption; '')
                    {
                    }
                    column(OrderNo_SalesShptHeader; "Sales Shipment Header"."Order No.")
                    {
                    }
                    column(OrderNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Order No."))
                    {
                    }
                    column(SumNetWeight; SumNetWeight)
                    {
                    }
                    column(SumGrossWeight; SumGrossWeight)
                    {
                    }
                    column(QuantityPackage; QuantityPackage)
                    {
                    }
                    column(QuantityPallet; QuantityPallet)
                    {
                    }
                    column(ShowNetWeight; ShowNetWeight)
                    {
                    }
                    column(ShowGrossWeight; ShowGrossWeight)
                    {
                    }
                    column(SalesShipmentHeader_OrderDate; "Sales Shipment Header"."Order Date")
                    {
                    }
                    column(SenderGLNLbl; SenderGLNLbl)
                    {
                    }
                    column(ShipmentGLNLbl; ShipmentGLNlbl)
                    {
                    }
                    column(InvoiceGLNLbl; InvoiceGLNLbl)
                    {
                    }
                    column(ShipmentGLN; ShipmentGLN)
                    {
                    }
                    column(InvoiceGLN; InvoiceGLN)
                    {
                    }
                    column(SalesInvoiceNoLbl; SalesInvoiceNoLbl)
                    {
                    }
                    column(SenderLbl; SenderLbl)
                    {
                    }
                    column(OrderDateLbl; OrderDateLbl)
                    {
                    }
                    column(ShipmentAddressLbl; ShipmentAddressLbl)
                    {
                    }
                    column(ShipmentDate; "Sales Shipment Header"."Shipment Date")
                    {
                    }
                    column(OrderDate; Format("Sales Shipment Header"."Order Date"))
                    {
                    }
                    column(DocumentDate; "Sales Shipment Header"."Document Date")
                    {
                    }
                    column(ReferenceLbl; ReferenceLbl)
                    {
                    }
                    column(ShippingAgentCode; "Sales Shipment Header"."Shipping Agent Code")
                    {
                    }
                    column(ShippingAgentCode_Caption; "Sales Shipment Header".FIELDCAPTION("Shipping Agent Code"))
                    {
                    }
                    column(PackageTrackingNo; "Sales Shipment Header"."Package Tracking No.")
                    {
                    }
                    column(PackageTrackingNo_Caption; "Sales Shipment Header".FIELDCAPTION("Package Tracking No."))
                    {
                    }
                    column(ShowSellToInformation; ShowSellToInformation)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(Commission; "Sales Shipment Header".Commission)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(CommissionCaption; "Sales Shipment Header".FIELDCAPTION(Commission))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonCaption; "Sales Shipment Header".FIELDCAPTION("Office Sales Person"))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonName; OfficeSalesperson.Name)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonPhoneNo; OfficeSalesperson."Phone No.")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonEMail; OfficeSalesperson."E-Mail")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(Pageno_; CurrReport.PAGENO) { }
                    column(Lieferantennummer; Cust."Our Account No.") { }
                    column(BarcodeLS; '*' + LSNO + '*') { }
                    column(LiefCpt; LiefLbl) { }
                    column(ShipmentCode; "Sales Shipment Header"."Shipping Agent Code") { }
                    column(ShipmentCodeCpt; ShimentCodetxt) { }
                    column(ShipmentMethodCaption; ShipmentMethodCaptionLbl) { }
                    column(ShipmentMethodDescription; ShipmentMethod.Description) { }
                    column(StockCodeCpt; StockCodeLbl) { }
                    column(StockCode; "Sales Shipment Line"."Location Code") { }
                    column(CustItemNo; '*' + CustomerItemNo + '*') { }
                    column(UserName; USER."Full Name") { }
                    column(Transportreasoncpt; TransportReasonLbl) { }
                    column(TransportReason; TransportReasontxt) { }
                    column(UserCpt; userlbl) { }





                    dataitem(TempSalesInvLine; "Sales Invoice Line")
                    {
                        DataItemTableView = SORTING ("Document No.", "Line No.");
                        UseTemporary = true;
                        column(SalesInvoiceHeaderNo; TempSalesInvLine."Document No.")
                        {
                        }
                    }
                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING (Number) WHERE (Number = FILTER (1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FINDSET then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 - %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1; %2 - %3', DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING ("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 22-02-2017 RM
                            NAIExtPreText.RESET;
                            NAIExtPreText.SETRANGE("Table ID", DATABASE::"Sales Shipment Header");
                            NAIExtPreText.SETRANGE("Document No.", "Sales Shipment Header"."No.");
                            NAIExtPreText.SETRANGE("Assigned for", NAIExtPreText."Assigned for"::Shipment);
                            NAIExtPreText.SETRANGE("Text Position", NAIExtPreText."Text Position"::Pretext);
                            // <<< 2111-49001 22-02-2017 RM
                        end;
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source No." = FIELD ("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING ("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE ("Source Table ID" = CONST (110), "Print Attribute" = CONST (true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }
                    }
                    dataitem("Sales Shipment Line"; "Sales Shipment Line")
                    {
                        DataItemLink = "Document No." = FIELD ("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING ("Document No.", "Line No.");
                        column(Description_SalesShptLine; Description)
                        {
                        }
                        column(Description_SalesShptLine2; "Description 2")
                        {
                        }
                        column(Description_SalesShptLine3; "Description 3")
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(ShowCorrectionLines; ShowCorrectionLines)
                        {
                        }
                        column(Type_SalesShptLine; FORMAT(Type, 0, 2))
                        {
                        }
                        column(AsmHeaderExists; AsmHeaderExists)
                        {
                        }
                        column(DocumentNo_SalesShptLine; "Document No.")
                        {
                        }
                        column(LinNo; LinNo)
                        {
                        }
                        column(Qty_SalesShptLine; Quantity)
                        {
                        }
                        column(Quantity_txt; Quantity_txt)
                        {
                        }
                        column(UnitPrice_txt; UnitPrice_txt)
                        {
                        }
                        column(QtyBacklog_txt; QtyBacklog_txt)
                        {
                        }
                        column(UOM_SalesShptLine; "Unit of Measure")
                        {
                        }
                        column(No_SalesShptLine; "No.")
                        {
                        }
                        column(LineNo_SalesShptLine; "Line No.")
                        {
                        }
                        column(Description_SalesShptLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(Qty_SalesShptLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(UOM_SalesShptLineCaption; UOM_SalesShptLineCaptionLbl)
                        {
                        }
                        column(No_SalesShptLineCaption; FIELDCAPTION("No."))
                        {
                        }
                        column(PositionNo_SalesShptLine; "Position No.")
                        {
                        }
                        column(QuantityWp_SalesShptLine; 0.0)
                        {
                        }
                        column(PositionNo_SalesShptLineCaption; SalesShptLinePositionNoCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesShptLineCaption; '')
                        {
                        }
                        column(HideInvoiceLine; HideInvoiceLine)
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(QuantityInBacklog; QuantityInBacklog)
                        {
                        }
                        column(OrderQuantity_SalesShptLine; "Order Quantity")
                        {
                        }
                        column(BacklogQuantity_SalesShptLine; "Backlog Quantity")
                        {
                        }
                        column(OrderQuantity_SalesShptLineCaption; FIELDCAPTION("Order Quantity"))
                        {
                        }
                        column(BacklogQuantity_SalesShptLineCaption; FIELDCAPTION("Backlog Quantity"))
                        {
                        }
                        column(CountryOfOrigin_SalesShptLine; "Country/Region of Origin Code")
                        {
                        }
                        dataitem(ItemLedgerEntry; "Item Ledger Entry")
                        {
                            DataItemLink = "Document No." = FIELD ("Document No."), "Document Line No." = FIELD ("Line No.");
                            DataItemTableView = SORTING ("Document No.", "Document Type", "Document Line No.");
                            column(ItemSerialNo; ItemLedgerEntry."Serial No.")
                            {
                            }
                            column(ItemLotNr; ItemLedgerEntry."Lot No.")
                            {
                            }
                            column(ItemExpirationDate; FORMAT(ItemLedgerEntry."Expiration Date"))
                            {
                            }
                            column(ItemQuantity; ABS(ItemLedgerEntry.Quantity))
                            {
                            }
                            column(ItemSerialNoCaption; SerialNoCaptionLbl)
                            {
                            }
                            column(ItemLotNoCaption; LotNoCaptionLbl)
                            {
                            }
                            column(ItemExpDateCaption; ExpirationDateCaptionLbl)
                            {
                            }
                            column(ItemQtyCaption; QuantityCaptionLbl)
                            {
                            }
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING (Number) WHERE (Number = FILTER (1 ..));
                            column(DimText1; DimText)
                            {
                            }
                            column(LineDimensionsCaption; LineDimensionsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FINDSET then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 - %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1; %2 - %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;
                            end;
                        }
                        dataitem(DisplayAsmInfo; "Integer")
                        {
                            DataItemTableView = SORTING (Number);
                            column(KitPosNo; '')
                            {
                            }
                            column(PostedAsmLineItemNo; BlanksForIndent + PostedAsmLine."No.")
                            {
                            }
                            column(PostedAsmLineDescription; BlanksForIndent + PostedAsmLine.Description)
                            {
                            }
                            column(PostedAsmLineQuantity; PostedAsmLine.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(PostedAsmLineUOMCode; GetUnitOfMeasureDescr(PostedAsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then
                                    PostedAsmLine.FINDSET
                                else
                                    PostedAsmLine.NEXT;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not DisplayAssemblyInformation then
                                    CurrReport.BREAK;

                                if not AsmHeaderExists then
                                    CurrReport.BREAK;

                                PostedAsmLine.SETRANGE("Document No.", PostedAsmHeader."No.");
                                SETRANGE(Number, 1, PostedAsmLine.COUNT);
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source No." = FIELD ("Document No."), "Source Line No." = FIELD ("Line No.");
                            DataItemLinkReference = "Sales Shipment Line";
                            DataItemTableView = SORTING ("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE ("Source Table ID" = CONST (111), "Print Attribute" = CONST (true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }
                        }
                        dataitem(PalettItemTrackingGroup; "Integer")
                        {
                            DataItemTableView = SORTING (Number);
                            column(GroupPalettTrackingSpecBuffer__Quantity__Base__; 0.0)
                            {
                            }
                            column(GroupPalettTrackingSpecBuffer__Palett_No__; '')
                            {
                            }
                            column(GroupPalettTrackingSpecBuffer__Entry_No__; 0)
                            {
                            }
                            column(TrackingSpecBuffer__Palett_No__Caption1; '')
                            {
                            }
                            column(PalettItemTrackingGroup_Number; Number)
                            {
                            }
                            dataitem(PalettItemTracking; "Integer")
                            {
                                DataItemTableView = SORTING (Number);
                                column(Palett_TrackingSpecBuffer_Serial_No__; '')
                                {
                                }
                                column(Palett_TrackingSpecBuffer_Palett_No__; '')
                                {
                                }
                                column(Palett_TrackingSpecBuffer__Quantity__Base__; 0.0)
                                {
                                }
                                column(Palett_TrackingSpecBuffer__Entry_No__; 0)
                                {
                                }
                                column(TrackingSpecBuffer__Palett_No__Caption; '')
                                {
                                }
                                column(PalettItemTracking_Number; Number)
                                {
                                }

                                trigger OnPreDataItem();
                                begin
                                    // >>> 2111-49001 22-02-2017 RM
                                    CurrReport.BREAK;
                                    // <<< 2111-49001 22-02-2017 RM
                                end;
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 22-02-2017 RM
                                CurrReport.BREAK;
                                // <<< 2111-49001 22-02-2017 RM
                            end;
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                            SalesShipmentLineRel: Record "Sales Shipment Line";
                            QuantityDeliv: Decimal;
                        begin
                            LinNo := "Line No.";
                            if not ShowCorrectionLines and Correction then
                                CurrReport.SKIP;
                            // >>> 2111-34284 23-11-2015 SMS
                            ItemTariffNo := '';
                            if "Sales Shipment Line".Type = "Sales Shipment Line".Type::Item then begin
                                // >>> 2111-37329 28-07-2016 MK
                                // ItemRec.GET("Sales Shipment Line"."No.");
                                // ItemTariffNo := ItemRec."Tariff No.";
                                if ItemRec.GET("Sales Shipment Line"."No.") then
                                    ItemTariffNo := ItemRec."Tariff No.";
                                // <<< 2111-37329 28-07-2016 MK
                            end;

                            QuantityInBacklog := 0;
                            QuantityDeliv := 0;

                            // >>> 2111-35647 25-02-2016 RM
                            if (Type <> Type::" ") and ("Order No." <> '') then begin
                                // <<< 2111-35647 25-02-2016 RM
                                SalesShipmentLineRel.RESET;
                                // >>> 2111-35647 25-02-2016 RM
                                SalesShipmentLineRel.SETCURRENTKEY("Order No.", "Order Line No.", "Posting Date");
                                // <<< 2111-35647 25-02-2016 RM
                                SalesShipmentLineRel.SETRANGE("Order No.", "Sales Shipment Line"."Order No.");
                                SalesShipmentLineRel.SETRANGE("Order Line No.", "Sales Shipment Line"."Order Line No.");
                                SalesShipmentLineRel.SETFILTER("Posting Date", '>=%1', "Sales Shipment Line"."Posting Date");
                                if SalesShipmentLineRel.FINDSET then begin
                                    repeat
                                        QuantityDeliv += SalesShipmentLineRel.Quantity;
                                    until SalesShipmentLineRel.NEXT = 0;
                                    QuantityInBacklog := "Sales Shipment Line"."Order Quantity" - QuantityDeliv;
                                end;
                                // >>> 2111-35647 25-02-2016 RM
                            end;
                            // <<< 2111-35647 25-02-2016 RM
                            // <<< 2111-34284 23-11-2015 SMS

                            DimSetEntry2.SETRANGE("Dimension Set ID", "Dimension Set ID");
                            if DisplayAssemblyInformation then
                                AsmHeaderExists := AsmToShipmentExists(PostedAsmHeader);

                            // >>> 2111-34612 27-11-2015 SMS
                            if Type <> Type::" " then begin
                                if (Quantity = 0) and NAISetup."Suppress 0-Qty-Pos. On Shpt." then begin
                                    SkipInvLine := true;
                                    InvLineNoToSkip := "Line No.";
                                    HideInvoiceLine := true;
                                end else begin
                                    SkipInvLine := false;
                                    HideInvoiceLine := false;
                                end;
                            end;

                            if Type = Type::" " then begin
                                if SkipInvLine and ("Attached to Line No." = InvLineNoToSkip) then begin
                                    HideInvoiceLine := true;
                                    CurrReport.SKIP;
                                end;
                            end;
                            // <<< 2111-34612 27-11-2015 SMS

                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // <<< 2111-47914 21-09-2016 MMA
                            // >>> 2111-34604 27-11-2015 SMS
                            if "Sales Shipment Line".Type = "Sales Shipment Line".Type::Item then begin
                                GetItemCrossReference;
                            end;
                            // <<< 2111-34604 27-11-2015 SMS

                            // >>> 2111-34887 23-12-2015 FS
                            if Type <> Type::" " then begin
                                Quantity_txt := COPYSTR(FORMAT(Quantity, 0, 0), 1, MAXSTRLEN(Quantity_txt));
                                UnitPrice_txt := COPYSTR(FORMAT("Unit Price", 0, 0), 1, MAXSTRLEN(UnitPrice_txt));
                                // >>> 2111-62392 06-06-2018 BD
                                //QtyBacklog_txt := COPYSTR(FORMAT(QuantityInBacklog,0,0), 1, MAXSTRLEN(QtyBacklog_txt));
                                QtyBacklog_txt := COPYSTR(FORMAT("Backlog Quantity", 0, 0), 1, MAXSTRLEN(QtyBacklog_txt));
                                // <<< 2111-62392 06-06-2018 BD
                            end else begin
                                Quantity_txt := '';
                                UnitPrice_txt := '';
                                QtyBacklog_txt := '';
                            end;
                            // <<< 2111-34887 23-12-2015 FS
                        end;

                        trigger OnPostDataItem();
                        var
                            "---NAI---": Integer;
                            GeneralReportManagement: Codeunit "General Report Management";
                        begin
                            // Item Tracking:
                            if ShowLotSN then begin
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(true);
                                TrackingSpecCount := ItemTrackingDocMgt.RetrieveDocumentItemTracking(TrackingSpecBuffer, "Sales Shipment Header"."No.", DATABASE::"Sales Shipment Header", 0);
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(false);

                                // >>> 2111-51001 17-08-2017 MW
                                GeneralReportManagement.FillExpirationDateInTrackSpec(TrackingSpecBuffer);
                                // <<< 2111-51001 17-08-2017 MW
                            end;
                        end;

                        trigger OnPreDataItem();
                        begin
                            MoreLines := FIND('+');
                            while MoreLines and (Description = '') and ("No." = '') and (Quantity = 0) do
                                MoreLines := NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SETRANGE("Line No.", 0, "Line No.");
                        end;
                    }
                    dataitem(Total; "Integer")
                    {
                        DataItemTableView = SORTING (Number) WHERE (Number = CONST (1));
                    }
                    dataitem(Total2; "Integer")
                    {
                        DataItemTableView = SORTING (Number) WHERE (Number = CONST (1));
                        column(BilltoCustNo_SalesShptHeader; "Sales Shipment Header"."Bill-to Customer No.")
                        {
                        }
                        column(CustAddr1; CustAddr[1])
                        {
                        }
                        column(CustAddr2; CustAddr[2])
                        {
                        }
                        column(CustAddr3; CustAddr[3])
                        {
                        }
                        column(CustAddr4; CustAddr[4])
                        {
                        }
                        column(CustAddr5; CustAddr[5])
                        {
                        }
                        column(CustAddr6; CustAddr[6])
                        {
                        }
                        column(CustAddr7; CustAddr[7])
                        {
                        }
                        column(CustAddr8; CustAddr[8])
                        {
                        }
                        column(BilltoAddressCaption; BilltoAddressCaptionLbl)
                        {
                        }
                        column(SelltoAddressCaption; SelltoAddressCaptionLbl) { }
                        column(BilltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Bill-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowCustAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(ItemTrackingLine; "Integer")
                    {
                        DataItemTableView = SORTING (Number);
                        column(TrackingSpecBufferNo; TrackingSpecBuffer."Item No.")
                        {
                        }
                        column(TrackingSpecBufferDesc; TrackingSpecBuffer.Description)
                        {
                        }
                        column(TrackingSpecBufferLotNo; TrackingSpecBuffer."Lot No.")
                        {
                        }
                        column(TrackingSpecBufferSerNo; TrackingSpecBuffer."Serial No.")
                        {
                        }
                        column(TrackingSpecBufferQty; TrackingSpecBuffer."Quantity (Base)")
                        {
                        }
                        column(TrackingSpecBufferExpDate; TrackingSpecBuffer."Expiration Date")
                        {
                        }
                        column(ShowTotal; ShowTotal)
                        {
                        }
                        column(ShowGroup; ShowGroup)
                        {
                        }
                        column(QuantityCaption; QuantityCaptionLbl)
                        {
                        }
                        column(SerialNoCaption; SerialNoCaptionLbl)
                        {
                        }
                        column(LotNoCaption; LotNoCaptionLbl)
                        {
                        }
                        column(DescriptionCaption; DescriptionCaptionLbl)
                        {
                        }
                        column(NoCaption; NoCaptionLbl)
                        {
                        }
                        column(ExpDateCaption; ExpirationDateCaptionLbl)
                        {
                        }
                        column(TrackingSpecBufferNoBarcode; TrackingSpecBufferNoBarcode)
                        {
                        }
                        column(TrackingSpecBufferLotNoBarcode; TrackingSpecBufferLotNoBarcode)
                        {
                        }
                        column(TrackingSpecBufferSerNoBarcode; TrackingSpecBufferSerNoBarcode)
                        {
                        }
                        dataitem(TotalItemTracking; "Integer")
                        {
                            DataItemTableView = SORTING (Number) WHERE (Number = CONST (1));
                            column(Quantity1; TotalQty)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then
                                TrackingSpecBuffer.FINDSET
                            else
                                TrackingSpecBuffer.NEXT;

                            ShowTotal := false;
                            if ItemTrackingAppendix.IsStartNewGroup(TrackingSpecBuffer) then
                                ShowTotal := true;

                            ShowGroup := false;
                            if (TrackingSpecBuffer."Source Ref. No." <> OldRefNo) or
                               (TrackingSpecBuffer."Item No." <> OldNo)
                            then begin
                                OldRefNo := TrackingSpecBuffer."Source Ref. No.";
                                OldNo := TrackingSpecBuffer."Item No.";
                                TotalQty := 0;
                            end else
                                ShowGroup := true;
                            TotalQty += TrackingSpecBuffer."Quantity (Base)";

                            // >>> 2111-37250 26-08-2016 SMS
                            TrackingSpecBufferNoBarcode := FORMAT(StartABC) + FORMAT(FNC1) + FORMAT(TrackingSpecBuffer."Item No.");
                            TrackingSpecBufferNoBarcode := TrackingSpecBufferNoBarcode + FORMAT(CalcCheckDigitCODE128(TrackingSpecBufferNoBarcode)) + FORMAT(Stop);

                            TrackingSpecBufferLotNoBarcode := FORMAT(StartABC) + FORMAT(FNC1) + FORMAT(TrackingSpecBuffer."Lot No.");
                            TrackingSpecBufferLotNoBarcode := TrackingSpecBufferLotNoBarcode + FORMAT(CalcCheckDigitCODE128(TrackingSpecBufferLotNoBarcode)) + FORMAT(Stop);

                            TrackingSpecBufferSerNoBarcode := FORMAT(StartABC) + FORMAT(FNC1) + FORMAT(TrackingSpecBuffer."Serial No.");
                            TrackingSpecBufferSerNoBarcode := TrackingSpecBufferSerNoBarcode + FORMAT(CalcCheckDigitCODE128(TrackingSpecBufferSerNoBarcode)) + FORMAT(Stop);
                            // <<< 2111-37250 26-08-2016 SMS
                        end;

                        trigger OnPreDataItem();
                        begin
                            if TrackingSpecCount = 0 then
                                CurrReport.BREAK;
                            CurrReport.NEWPAGE;
                            SETRANGE(Number, 1, TrackingSpecCount);
                            TrackingSpecBuffer.SETCURRENTKEY("Source ID", "Source Type", "Source Subtype", "Source Batch Name",
                              "Source Prod. Order Line", "Source Ref. No.");

                            // >>> 2111-37250 26-08-2016 SMS
                            FNC1 := 207;
                            StartABC := 209;
                            Stop := 211;
                            // <<< 2111-37250 26-08-2016 SMS
                        end;
                    }
                    dataitem(SalesShipmentLineItems1; "Sales Shipment Line")
                    {
                        DataItemLink = "Document No." = FIELD ("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING ("Document No.", "Line No.") WHERE (Type = CONST (Item), Quantity = FILTER (<> 0));
                        column(SalesShipmentLineItems1__No__; '')
                        {
                        }
                        column(SalesShipmentLineItems1_Description; '')
                        {
                        }
                        column(SalesShipmentLineItems1__Unit_of_Measure_; '')
                        {
                        }
                        column(SalesShipmentLineItems1_Quantity; 0.0)
                        {
                        }
                        column(SalesShipmentLineItems1__No__Caption; '')
                        {
                        }
                        column(SalesShipmentLineItems1_DescriptionCaption; '')
                        {
                        }
                        column(Kistennr_Caption_1; '')
                        {
                        }
                        column(SalesShipmentLineItems1__Unit_of_Measure_Caption; '')
                        {
                        }
                        column(SalesShipmentLineItems1_QuantityCaption; '')
                        {
                        }
                        column(SalesShipmentLineItems1_Document_No_; '')
                        {
                        }
                        column(SalesShipmentLineItems1_Line_No_; 0)
                        {
                        }
                        dataitem(PalettList; "Integer")
                        {
                            DataItemTableView = SORTING (Number);
                            column(GroupPalettTrackingSpecBuffer__PalettNo1; '')
                            {
                            }
                            column(GroupPalettTrackingSpecBuffer__Quantity__Base___Control1; 0.0)
                            {
                            }
                            column(SalesShipmentLineItems1__Unit_of_Measure__Control1; '')
                            {
                            }
                            column(PalettList_Number; Number)
                            {
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 22-02-2017 RM
                                CurrReport.BREAK;
                                // <<< 2111-49001 22-02-2017 RM
                            end;
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 22-02-2017 RM
                            CurrReport.BREAK;
                            // <<< 2111-49001 22-02-2017 RM
                        end;
                    }
                    dataitem(SalesShipmentLineItems2; "Sales Shipment Line")
                    {
                        DataItemLink = "Document No." = FIELD ("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING ("Document No.", "Line No.") WHERE (Type = CONST (Item), Quantity = FILTER (<> 0));

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 22-02-2017 RM
                            CurrReport.BREAK;
                            // >>> 2111-49001 22-02-2017 RM
                        end;
                    }
                    dataitem(PackingList; "Integer")
                    {
                        DataItemTableView = SORTING (Number);
                        column(PalettNoText; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Item_No__PL; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer_DescriptionPL; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Quantity__Base__PL; 0.0)
                        {
                        }
                        column(Item__Base_Unit_of_Measure_PL; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Serial_No__PL; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBufferPalettNoPL; '')
                        {
                        }
                        column(PalettNoTextCaption; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Item_No__Caption; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer_DescriptionCaption; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Quantity__Base___Control1119479047Caption; '')
                        {
                        }
                        column(Item__Base_Unit_of_Measure_Caption; '')
                        {
                        }
                        column(GroupPalettTrackingSpecBuffer__Serial_No__Caption; '')
                        {
                        }
                        column(PackingList_Number; Number)
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 22-02-2017 RM
                            CurrReport.BREAK;
                            // <<< 2111-49001 22-02-2017 RM
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Ext. Text - Posted Sales")
                    {
                        DataItemTableView = SORTING ("Table ID", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 22-02-2017 RM
                            NAIExtPostText.RESET;
                            NAIExtPostText.SETRANGE("Table ID", DATABASE::"Sales Shipment Header");
                            NAIExtPostText.SETRANGE("Document No.", "Sales Shipment Header"."No.");
                            NAIExtPostText.SETRANGE("Assigned for", NAIExtPostText."Assigned for"::Shipment);
                            NAIExtPostText.SETRANGE("Text Position", NAIExtPostText."Text Position"::Posttext);
                            // <<< 2111-49001 22-02-2017 RM
                        end;
                    }

                    trigger OnPreDataItem();
                    begin
                        // Item Tracking:
                        if ShowLotSN then begin
                            TrackingSpecCount := 0;
                            OldRefNo := 0;
                            ShowGroup := false;
                        end;
                    end;
                }

                trigger OnAfterGetRecord();
                begin
                    if Number > 1 then begin
                        CopyText := Text001;
                        OutputNo += 1;
                    end;
                    CurrReport.PAGENO := 1;
                    TotalQty := 0;           // Item Tracking
                end;

                trigger OnPostDataItem();
                begin
                    if not CurrReport.PREVIEW then
                        ShptCountPrinted.RUN("Sales Shipment Header");
                end;

                trigger OnPreDataItem();
                var
                    "---NAI---": Integer;
                    ShippingAgent: Record "Shipping Agent";
                begin
                    NoOfLoops := 1 + ABS(NoOfCopies);

                    // >>> 2111-36124 22-04-2016 SMS
                    if ("Sales Shipment Header"."Shipping Agent Code" <> '') and ShippingAgent.GET("Sales Shipment Header"."Shipping Agent Code") then begin
                        NoOfLoops += ShippingAgent."Number of Shipment Copies";
                    end;
                    // <<< 2111-36124 22-04-2016 SMS

                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                "---NAI---": Integer;
                ShiptoAddress: Record "Ship-to Address";
                ShipBillCustomer: Record Customer;
                SalesShipmentLine: Record "Sales Shipment Line";
                GeneralSalesManagement: Codeunit "General Sales Management";
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");

                // >>> 2111-49001 22-02-2017 RM
                CompanyInfo.GET;
                // <<< 2111-49001 22-02-2017 RM

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 22-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, MAXSTRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 22-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 22-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 22-02-2017 RM
                end;
                begin
                    if "Sales Shipment Header"."Logistician Shipment No." <> '' then
                        LSNO := "Sales Shipment Header"."Logistician Shipment No." else
                        LSNO := "Sales Shipment Header"."No.";
                end;

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                // >>> 2111-49001 22-02-2017 RM
                if ShowOfficeSalesperson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 22-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 22-02-2017 RM
                end;
                // <<< 2111-49001 22-02-2017 RM
                // >>> 14.10.2019 MMC OP
                if not Cust.GET("Bill-to Customer No.") then
                    CLEAR(Cust);

                User.RESET;
                User.SETRANGE("User Name", "User ID");
                IF NOT User.FINDFIRST THEN
                    User.INIT;
                //<<< 14.10.2019 MMC OP
                // >>> 2111-62379 20-06-2018 BD
                if "Office Sales Person" <> '' then begin
                    OfficeSalesperson.GET("Office Sales Person");
                end else begin
                    OfficeSalesperson.INIT;
                end;
                // <<< 2111-62379 20-06-2018 BD

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                FormatAddr.SalesShptShipTo(ShipToAddr, "Sales Shipment Header");

                FormatAddr.SalesShptBillTo(CustAddr, ShipToAddr, "Sales Shipment Header");
                ShowCustAddr := true;  //<<< changed 14.10.19 MMC OP
                for i := 1 to ARRAYLEN(CustAddr) do
                    if CustAddr[i] <> ShipToAddr[i] then
                        ShowCustAddr := true;



                if LogInteraction then
                    if not CurrReport.PREVIEW then
                        SegManagement.LogDocument(
                          5, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');


                if "Shipment Method Code" = '' then
                    ShipmentMethod.INIT
                else begin
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                end;

                // >>> 2111-34381 28-12-2015 DB
                ShippingInformationMgt.GetExtendedShippingInfo("Sales Shipment Header", SumNetWeight, SumGrossWeight, ShowNetWeight, ShowGrossWeight);
                ShippingInformationMgt.GetExtendedQuantityInfo("Sales Shipment Header", QuantityPackage, QuantityPallet);
                // <<< 2111-34381 28-12-2015 DB

                // >>> 2111-49109 17-02-2017 MW
                ShipmentGLN := '';
                InvoiceGLN := '';

                GeneralSalesManagement.GetSalesInvLineFromShipmentHeader("Sales Shipment Header", TempSalesInvLine);

                if "Sales Shipment Header"."Ship-to Code" <> '' then begin
                    if ShiptoAddress.GET("Sales Shipment Header"."Sell-to Customer No.", "Sales Shipment Header"."Ship-to Code") then begin
                        ShipmentGLN := ShiptoAddress.GLN;
                    end;

                end else begin
                    if ShipBillCustomer.GET("Sales Shipment Header"."Sell-to Customer No.") then begin
                        ShipmentGLN := ShipBillCustomer.GLN;
                    end;
                end;

                if "Sales Shipment Header"."Bill-to Customer No." <> '' then begin
                    if ShipBillCustomer.GET("Sales Shipment Header"."Bill-to Customer No.") then begin
                        InvoiceGLN := ShipBillCustomer.GLN;
                    end;
                end;
                // <<< 2111-49109 17-02-2017 MW

                // >>> 2111-51102 01-08-2017 MW
                // >>> 2111-51624 02-08-2017 MA
                TotalNetWeight := 0;
                TotalGrossWeight := 0;
                // <<< 2111-51624 02-08-2017 MA
                with SalesShipmentLine do begin
                    SETRANGE("Document No.", "Sales Shipment Header"."No.");
                    if FINDSET then begin
                        repeat
                            if Type = Type::Item then begin
                                // >>> 2111-51624 02-08-2017 MA
                                //TotalNetWeight += TotalNetWeight + "Net Weight" * Quantity;
                                //TotalGrossWeight += TotalGrossWeight + "Gross Weight" * Quantity;
                                TotalNetWeight += "Net Weight" * Quantity;
                                TotalGrossWeight += "Gross Weight" * Quantity;
                                // <<< 2111-51624 02-08-2017 MA
                            end;
                        until NEXT = 0;
                    end;
                end;
                // <<< 2111-51102 01-08-2017 MW

                // >>> 2111-62379 20-06-2018 BD
                if ("Sell-to Customer Name" <> "Bill-to Name") or
                   ("Sell-to Customer Name 2" <> "Bill-to Name 2") or
                   ("Sell-to Customer Name 3" <> "Bill-to Name 3") or
                   ("Sell-to Customer Name 4" <> "Bill-to Name 4") or
                   ("Sell-to Address" <> "Bill-to Address") or
                   ("Sell-to Address 2" <> "Bill-to Address 2") or
                   ("Sell-to City" <> "Bill-to City") or
                   ("Sell-to Post Code" <> "Bill-to Post Code")
                then begin
                    ShowSellToInformation := true;
                end;
                // <<< 2111-62379 20-06-2018 BD
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        CaptionML = DEU = 'Korrekturzeilen anzeigen',
                                    ENU = 'Show Correction Lines';
                    }
                    field(ShowLotSN; ShowLotSN)
                    {
                        CaptionML = DEU = 'Chargennr./Seriennr. Anhang anzeigen',
                                    ENU = 'Show Serial/Lot Number Appendix';
                    }
                    field(DisplayAsmInfo; DisplayAssemblyInformation)
                    {
                        CaptionML = DEU = 'Montage Komponenten anzeigen',
                                    ENU = 'Show Assembly Components';
                    }
                    field(ShowOfficeSalesperson; ShowOfficeSalesperson)
                    {
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
        end;

        trigger OnOpenPage();
        var
            NAISetup: Record "NAI - Setup";
        begin
            InitLogInteraction;
            LogInteractionEnable := LogInteraction;
            // >>> 2111-36930 21-06-2016 MMA
            if NAISetup.GET then begin
                ShowLotSN := NAISetup."Print Tracking On Shipment";
            end;
            // <<< 2111-36930 21-06-2016 MMA
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET;
        SalesSetup.GET;

        // >>> 2111-37250 26-08-2016 SMS
        if NAISetup.GET then begin
            ShowItemTrackBarCode := NAISetup."Print Tracking On Shipment BC";
        end;
        // <<< 2111-37250 26-08-2016 SMS
    end;

    trigger OnPreReport();
    begin
        if not CurrReport.USEREQUESTPAGE then
            InitLogInteraction;

        AsmHeaderExists := false;
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'KOPIE', ENU = 'COPY';
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo2: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        Language: Record Language;
        TrackingSpecBuffer: Record "Tracking Specification" temporary;
        PostedAsmHeader: Record "Posted Assembly Header";
        PostedAsmLine: Record "Posted Assembly Line";
        ShptCountPrinted: Codeunit "Sales Shpt.-Printed";
        SegManagement: Codeunit SegManagement;
        ItemTrackingDocMgt: Codeunit "Item Tracking Doc. Management";
        RespCenter: Record "Responsibility Center";
        ItemTrackingAppendix: Report "Item Tracking Appendix";
        ShipmentMethod: record "Shipment Method";
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text[80];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        OutputNo: Integer;
        NoOfLoops: Integer;
        TrackingSpecCount: Integer;
        OldRefNo: Integer;
        OldNo: Code[20];
        CopyText: Text[30];
        ShowCustAddr: Boolean;
        i: Integer;
        FormatAddr: Codeunit "Format Address";
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        ShowTotal: Boolean;
        ShowGroup: Boolean;
        TotalQty: Decimal;
        [InDataSet]
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        LinNo: Integer;
        Text002: TextConst DEU = 'Lieferschein %1 %2', ENU = 'Shipment %1 %2', ITA = 'DDT n. %1 %2';
        ItemTrackingAppendixCaptionLbl: TextConst DEU = 'Artikelverfolgung - Anhang', ENU = 'Item Tracking - Appendix';
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        VATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.';
        AccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        ShipmentNoCaptionLbl: TextConst DEU = 'Lieferungsnr.', ENU = 'Shipment No.';
        ShipmentDateCaptionLbl: TextConst DEU = 'Warenausg.-Datum', ENU = 'Shipment Date', ITA = 'Data spedizione';
        CompanyInfoHomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        CompanyInfoEmailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date', ITA = 'Data documento';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        LineDimensionsCaptionLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        BilltoAddressCaptionLbl: TextConst DEU = 'Rechnungsadresse', ENU = 'Bill-to Address';
        SelltoAddressCaptionLbl: TextConst DEU = 'Rechnungsadresse', ENU = 'Bill-to Address', ITA = 'Indirizzo Di Fatturazione';
        QuantityCaptionLbl: TextConst DEU = 'Menge', ENU = 'Quantity';
        SerialNoCaptionLbl: TextConst DEU = 'Seriennr.', ENU = 'Serial No.';
        LotNoCaptionLbl: TextConst DEU = 'Chargennr.', ENU = 'Lot No.';
        DescriptionCaptionLbl: TextConst DEU = 'Beschreibung', ENU = 'Description';
        NoCaptionLbl: TextConst DEU = 'Nr.', ENU = 'No.';
        "---NAI---": Integer;
        NAISetup: Record "NAI - Setup";
        OfficeSalesperson: Record "Salesperson/Purchaser";
        ShippingInformationMgt: Codeunit "Shipping Information Mgt.";
        CompanyAddressLine: Text[160];
        [InDataSet]
        ShowOfficeSalesperson: Boolean;
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        Text003: TextConst DEU = 'Seite %1', ENU = 'Page %1', ITA = 'Pagina %1';
        Text004: TextConst DEU = 'Ware ordnungsgemäß und vollständig erhalten:', ENU = 'Received goods correctly and completely:';
        Text5000000: TextConst DEU = 'der Lieferung %1', ENU = 'of Shipment %1';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Lieferantennr:', ENU = 'Account No.', ITA = 'Numero fornitore';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        SalesShptLinePositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.', ITA = 'Pos.n.';
        SignatureLbl: TextConst DEU = 'Unterschrift Kunde', ENU = 'Signature Customer';
        UOM_SalesShptLineCaptionLbl: TextConst DEU = 'Einheit', ENU = 'Unit', FRA = 'Unité', NLD = 'Eenheid', ITA = 'Unita di misura';
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ', ITA = 'Il tuo numero di articolo';
        SumNetWeightLbl: TextConst DEU = 'Summe Netto-Gewicht', ENU = 'Sum Net Weight';
        SumGrossWeightLbl: TextConst DEU = 'Summe Brutto-Gewicht', ENU = 'Sum Gross Weight';
        QuantityPackageLbl: TextConst DEU = 'Anzahl Packstücke', ENU = 'Quantity Package';
        QuantityPalletLbl: TextConst DEU = 'Anzahl Paletten', ENU = 'Quantity Pallet';
        SenderGLNLbl: TextConst DEU = 'Absender GLN', ENU = 'Sender GLN';
        ShipmentGLNlbl: TextConst DEU = 'WE-GLN', ENU = 'DP-GLN';
        InvoiceGLNLbl: TextConst DEU = 'RG-GLN', ENU = 'IV-GLN';
        SalesInvoiceNoLbl: TextConst DEU = 'Rechnungsnummer', ENU = 'Invoice Number';
        SenderLbl: TextConst DEU = 'Absender', ENU = 'Sender';
        OrderDateLbl: TextConst DEU = 'Auftragsdatum', ENU = 'Order Date', ITA = 'Data ordine';
        ShipmentAddressLbl: TextConst DEU = 'Lieferanschrift', ENU = 'Shipment Address';
        ReferenceLbl: TextConst DEU = 'Referenz', ENU = 'Reference';
        OrgLbl: TextConst DEU = 'Orginal', ENU = 'Original', ITA = 'Originale';
        LIEFLbl: TextConst DEU = 'Lieferschein', ENU = 'Delivery Note', ITA = 'Bolla di consegna';
        ShimentCodetxt: TextConst DEU = 'Transportart', ENU = 'Shipment Code', ITA = 'Forma di Transporto';
        ShipmentMethodCaptionLbl: TextConst DEU = 'Lieferbedingung', ENU = 'Shipment Method', ITA = 'Mod. di consegna';
        StockCodeLbl: TextConst DEU = 'Lagerortcode', ENU = 'Stock', ITA = 'Deposito';
        UserLbl: TextConst DEU = 'Sachbearbeiter', ENU = 'Executive Officer', ITA = 'Addetto';
        TransportReasonLbl: TextConst DEU = 'Transportgrund', ENU = 'Transport Reason', ITA = 'Causale Del Transporto';
        TransportReasontxt: TextConst DEU = 'Verakuf', ENU = 'Sales', ITA = 'Vendita';
        ItemTariffNo: Code[20];
        LSNO: Text[20];
        QuantityInBacklog: Decimal;
        SkipInvLine: Boolean;
        InvLineNoToSkip: Integer;
        HideInvoiceLine: Boolean;
        CustomerItemNo: Code[20];
        SumNetWeight: Decimal;
        SumGrossWeight: Decimal;
        ShowNetWeight: Boolean;
        ShowGrossWeight: Boolean;
        QuantityPackage: Integer;
        QuantityPallet: Integer;
        ShowItemTrackBarCode: Boolean;
        FNC1: Char;
        StartABC: Char;
        Stop: Char;
        CheckNo: Char;
        CheckSum: Integer;
        INo: Integer;
        TrackingSpecBufferNoBarcode: Text;
        TrackingSpecBufferLotNoBarcode: Text;
        TrackingSpecBufferSerNoBarcode: Text;
        ShipmentGLN: Text;
        InvoiceGLN: Text;
        Quantity_txt: Text[15];
        UnitPrice_txt: Text[15];
        QtyBacklog_txt: Text[15];
        TotalNetWeight: Decimal;
        TotalGrossWeight: Decimal;
        ExpirationDateCaptionLbl: TextConst DEU = 'Ablaufdatum', ENU = 'Expiration Date';
        ShowSellToInformation: Boolean;
        Cust: Record Customer;
        USER: record User;


    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractTmplCode(5) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[10];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        if not UnitOfMeasure.GET(UOMCode) then
            exit(UOMCode);
        exit(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        exit(PADSTR('', 2, ' '));
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-34604 27-11-2015 SMS
        with "Sales Shipment Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-34604 27-11-2015 SMS
    end;

    local procedure CalcCheckDigitCODE128(data: Text): Char;
    var
        Text50000: TextConst DEU = '%1 ist kein gültiges Code 128 Zeichen!', ENU = '% 1 is not a valid code 128 characters!';
    begin
        // >>> 2111-37250 26-08-2016 SMS
        CheckSum := data[1] - 105;
        for INo := 1 to (STRLEN(data) - 1) do begin
            CheckNo := data[INo + 1];
            if (CheckNo >= 32) and (CheckNo <= 126) then begin
                CheckSum := CheckSum + INo * (CheckNo - 32);
            end else begin
                if (CheckNo >= 200) and (CheckNo <= 211) then begin
                    CheckSum := CheckSum + INo * (CheckNo - 105);
                end else begin
                    ERROR(Text50000, CheckNo);
                end;
            end;
        end;

        if (CheckSum mod 103 >= 0) and (CheckSum mod 103 <= 94) then begin
            CheckNo := CheckSum mod 103 + 32;
        end else begin
            if (CheckSum mod 103 >= 95) and (CheckSum mod 103 <= 106) then begin
                CheckNo := CheckSum mod 103 + 105;
            end else begin
                ERROR(Text50000, CheckNo);
            end;
        end;

        //CheckNo := (CheckSum MOD 103) + 32;
        exit(CheckNo);
        // <<< 2111-37250 26-08-2016 SMS
    end;
}

