report 50005 "MMC Sales - Quote NAI"
{
    // version NAVW17.00,NAI11.82.00

    // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  f a v e o   G m b H  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    // ~                                                               ~
    // ~                          www.faveo.de                         ~
    // ~                                                               ~
    // ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
    // Issue      Date       Dev. Change
    // 2111-22712 17-02-2014 RM   NAI-Pre-/Posttexts added
    // 2111-22942 28-02-2014 UQ   Discount reimplemented
    // 2111-23178 01-08-2014 UQ   Dynamic display of discounts
    // 2111-30322 24-02-2015 LK   "Page No." moved from Header to Footer
    // 2111-30322 04-03-2015 RW   Value of Text5000000 corrected
    // 2111-30349 27-03-2015 RW   Check for archiving added (Sales Header - OnPreDataItem)
    // 2111-31305 17-07-2015 MMA  Object moved from 50.000
    // 2111-33655 09-09-2015 RM   Print Unit Price of Alternative Items
    // 2111-33698 09-09-2015 RM   Print Discount Caption for Quotes with Line Discounts
    // 2111-31249 11-09-2015 SMS  Layout revised
    // 2111-34284 24-11-2015 SMS  Dataset expanded
    // 2111-36480 09-06-2016 MW   Field added: "Sales Header"."Quote Valid To"
    // 2111-36574 21-06-2016 MMA  Trigger modified: RequestPage - OnOpenPage
    // 2111-47914 21-09-2016 MMA  Item Cross Reference-Line added
    // 2111-48374 14-12-2016 RM   DataItems added for Attributes
    // 2111-49001 14-02-2017 RM   SOV replaced by NAI
    // 2111-49614 10-04-2017 MMA  Trigger modified: RoundLoop - OnAfterGetRecord()
    // 2111-49973 16-05-2017 RM   Correction of Subtotal & Invoice Discount Amount
    // 2111-51240 19-07-2017 RM   Logic of ZeroPricedLines reversed
    // 2111-51609 22-09-2017 MY   Dataset expanded: "Description 2" and "Description 3" added
    // 2111-62165 04-01-2018 MA   Fields added (DataItem PageLoop): "Sell-to ..."
    // 2111-62185 30-01-2018 BD   Fields added:
    //                            - "Sales Header"."Language Code"
    //                            - DueDate_SalesHeader
    // 2111-62234 12-03-2018 RW   Font modified: "PostText" (Arial -> Segoe UI)
    //                            Dataset expanded:
    //                            - "Sales Header"."Sell-to Customer Name 3"
    //                            - "Sales Header"."Sell-to Customer Name 4"
    //                            - "Sales Header"."Sell-to Post Code" instead of "Ship-to Post Code"
    //                            - ShowSellToInfo
    // 2111-62118 06-09-2018 SMS  Trigger modified: "Sales Header" - OnPostDataItem
    // 2111-62406 22-05-2019 AC   Layout changed: Position No. for Text Lines
    // 2111-62832 25-06-2019 AC   Trigger modified:
    //                            - Sales Header - OnAfterGetRecord()
    //                            - RoundLoop - OnAfterGetRecord()
    DefaultLayout = RDLC;
    RDLCLayout = './Report/Sales - Quote NAI.rdl';

    CaptionML = DEU = 'Verkauf - Angebot',
                ENU = 'Sales - Quote';

    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            DataItemTableView = SORTING("Document Type", "No.") WHERE("Document Type" = CONST(Quote));
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Verkaufsangebot',
                                     ENU = 'Sales Quote';
            column(DisplayAssemblyInfo; DisplayAssemblyInfo)
            {
            }
            column(ShowZeroPricedLines; not SuppressZeroPricedLines)
            {
            }
            column(DocType_SalesHeader; "Document Type")
            {
            }
            column(No_SalesHeader; "No.")
            {
            }
            column(SalesLineVATIdentifierCaption; SalesLineVATIdentifierCaptionLbl)
            {
            }
            column(PaymentTermsDescriptionCaption; PaymentTermsDescriptionCaptionLbl)
            {
            }
            column(ShipmentMethodDescriptionCaption; ShipmentMethodDescriptionCaptionLbl)
            {
            }
            column(CompanyInfoHomePageCaption; CompanyInfoHomePageCaptionLbl)
            {
            }
            column(CompanyInfoEmailCaption; CompanyInfoEmailCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(SalesLineAllowInvoiceDiscCaption; SalesLineAllowInvoiceDiscCaptionLbl)
            {
            }
            column(ShowDiscount; ShowDiscount)
            {
            }
            column(LanguageCode; "Sales Header"."Language Code")
            {
            }
            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(PaymentTermsDescription; PaymentTerms.Description)
                    {
                    }
                    column(ShipmentMethodDescription; ShipmentMethod.Description)
                    {
                    }
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(SalesCopyText; STRSUBSTNO(Text004, "Sales Header"."No.", CopyText))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(ShowSellToInformation; ShowSellToInfo)
                    {
                        Description = '2111-62234 12-03-2018 RM';
                    }
                    column(SellTo_Name; "Sales Header"."Sell-to Customer Name")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name2; "Sales Header"."Sell-to Customer Name 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name3; "Sales Header"."Sell-to Customer Name 3")
                    {
                        Description = '2111-62234 12-03-2018 RM';
                    }
                    column(SellTo_Name4; "Sales Header"."Sell-to Customer Name 4")
                    {
                        Description = '2111-62234 12-03-2018 RM';
                    }
                    column(SellTo_Address; "Sales Header"."Sell-to Address")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Address2; "Sales Header"."Sell-to Address 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Postcode; "Sales Header"."Sell-to Post Code")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_City; "Sales Header"."Sell-to City")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SalesHeaderExternalDocumentNo; "Sales Header"."External Document No.")
                    {
                    }
                    column(SalesHeaderExternalDocumentNoCaption; "Sales Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesHeader; "Sales Header"."Bill-to Customer No.")
                    {
                    }
                    column(DocDate_SalesHeader; FORMAT("Sales Header"."Document Date", 0, 4))
                    {
                    }
                    column(DueDate_SalesHeader; FORMAT("Sales Header"."Due Date", 0, 4))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesHeader; "Sales Header"."VAT Registration No.")
                    {
                    }
                    column(ShipmentDate_SalesHeader; FORMAT("Sales Header"."Shipment Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(No1_SalesHeader; "Sales Header"."No.")
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesHeader; "Sales Header"."Your Reference")
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(PricesIncludingVAT_SalesHdr; "Sales Header"."Prices Including VAT")
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text005, ''))
                    {
                    }
                    column(Text5000000_SalesHeaderNo; STRSUBSTNO(Text5000000, "Sales Header"."No."))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(UPV; UnitPriceVar) { }
                    column(LDV; LineDiscVar) { }
                    column(SAV; LineAmtVar) { }
                    column(PricesInclVATYesNo_SalesHdr; FORMAT("Sales Header"."Prices Including VAT"))
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankNameCaption; CompanyInfoBankNameCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(SalesHeaderShipmentDateCaption; SalesHeaderShipmentDateCaptionLbl)
                    {
                    }
                    column(SalesHeaderNoCaption; SalesHeaderNoCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesHeaderCaption; SalesHeaderBilltoCustomerNoCaptionLbl)
                    {
                    }
                    column(PricesIncludingVAT_SalesHdrCaption; "Sales Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(Sales_Header___VAT_Registration_No__Caption; "Sales Header".FIELDCAPTION("VAT Registration No."))
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }
                    column(Sales_Header___Quote_Valid_To; Format("Sales Header"."Quote Valid To"))
                    {
                    }
                    column(UserCpt; userlbl) { }
                    column(UserName; User."Full Name") { }
                    column(ShipmentCode; "Sales Header"."Shipping Agent Code") { }
                    column(ShipmentCodeCpt; ShipmentCodetxt) { }
                    column(CurrenyCode; 'EUR') { }
                    column(Currenytxt; Currencycpt) { }
                    column(validtocpt; validtotxt) { }
                    column(Lieferantennummer; Cust."Our Account No.") { }
                    column(OurAccounttxt; OurAccountCaption) { }
                    column(QuoteCpt; QuoteLbl) { }
                    column(QuoteCpt2; QuoteLbl2) { }
                    column(QuoteCpt3; QuoteLbl3) { }


                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; DimensionLoop1.Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FINDSET then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1, %2 %3', DimText, DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Extended Text - Sales")
                    {
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-22712 17-02-2014 RM
                            NAIExtPreText.RESET;
                            NAIExtPreText.SETRANGE("Document Type", "Sales Header"."Document Type");
                            NAIExtPreText.SETRANGE("Document No.", "Sales Header"."No.");
                            NAIExtPreText.SETRANGE("Assigned for", NAIExtPreText."Assigned for"::Quote);
                            NAIExtPreText.SETRANGE("Text Position", NAIExtPreText."Text Position"::Pretext);
                            // <<< 2111-22712 17-02-2014 RM
                        end;
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(36), "Print Attribute" = CONST(true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49973 16-05-2017 RM
                            if COUNT = 0 then
                                CurrReport.BREAK;
                            // <<< 2111-49973 16-05-2017 RM
                        end;
                    }
                    dataitem("Sales Line"; "Sales Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                        trigger OnPreDataItem();
                        begin
                            CurrReport.BREAK;
                        end;
                    }
                    dataitem(RoundLoop; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(LineAmt_SalesLine; SalesLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(No_SalesLine; "Sales Line"."No.")
                        {
                        }
                        column(Desc_SalesLine; "Sales Line".Description)
                        {
                        }
                        column(Desc_SalesLine2; SalesLine."Description 2")
                        {
                        }
                        column(Desc_SalesLine3; SalesLine."Description 3")
                        {
                        }
                        column(Quantity_SalesLine; "Sales Line".Quantity)
                        {
                        }
                        column(UnitofMeasure_SalesLine; "Sales Line"."Unit of Measure")
                        {
                        }
                        column(LineAmt1_SalesLine; "Sales Line"."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(UnitPrice_SalesLine; "Sales Line"."Unit Price")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 2;
                        }
                        column(LineDiscount_SalesLine; "Sales Line"."Line Discount %")
                        {
                        }
                        column(AllowInvoiceDisc_SalesLine; "Sales Line"."Allow Invoice Disc.")
                        {
                            IncludeCaption = false;
                        }
                        column(VATIdentifier_SalesLine; "Sales Line"."VAT Identifier")
                        {
                        }
                        column(Type_SalesLine; FORMAT("Sales Line".Type))
                        {
                        }
                        column(LineNo_SalesLine; "Sales Line"."Line No.")
                        {
                        }
                        column(AllowInvoiceDisYesNo; FORMAT("Sales Line"."Allow Invoice Disc."))
                        {
                        }
                        column(InvDiscountAmount_SalesLine; -SalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(DiscountAmt_SalesLine; SalesLine."Line Amount" - SalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmtTxt; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(VATAmount; VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATDiscountAmount; -VATDiscountAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseAmount; VATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(UnitPriceCaption; UnitPriceCaptionLbl)
                        {
                        }
                        column(SalesLineLineDiscCaption; SalesLineLineDiscCaptionLbl)
                        {
                        }
                        column(AmountCaption; AmountCaptionLbl)
                        {
                        }
                        column(SalesLineInvDiscAmtCaption; SalesLineInvDiscAmtCaptionLbl)
                        {
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(VATDiscountAmountCaption; VATDiscountAmountCaptionLbl)
                        {
                        }
                        column(No_SalesLineCaption; "Sales Line".FIELDCAPTION("No."))
                        {
                        }
                        column(Desc_SalesLineCaption; "Sales Line".FIELDCAPTION(Description))
                        {
                        }
                        column(Quantity_SalesLineCaption; "Sales Line".FIELDCAPTION(Quantity))
                        {
                        }
                        column(UnitofMeasure_SalesLineCaption; UnitofMeasure_SalesLineCaptionLbl)
                        {
                        }
                        column(PositionNo_SalesLine; "Sales Line"."Position No.")
                        {
                        }
                        column(QuantityWp_SalesLine; 0.0)
                        {
                        }
                        column(PositionNo_SalesLineCaption; SalesLinePositionNoCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesLineCaption; '')
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(AsmInfoExistsForLine; AsmInfoExistsForLine)
                        {
                        }
                        column(Sales_Line___Line_Discount___Caption; Sales_Line___Line_Discount___CaptionLbl)
                        {
                        }
                        column(Sales_Line___Line_Discount___; "Sales Line"."Line Discount %")
                        {
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        column(SubTotal; SubTotal)
                        {
                        }
                        column(InvDiscAmount; InvDiscAmount * -1)
                        {
                        }
                        column(PlannedDelweekyear; DeliveryWeekYear) { }
                        column(ExpectedWeekNumberCaption; ExpectedWeekNumberCaptionLbl) { }
                        dataitem("Reservation Entry"; "Reservation Entry")
                        {
                            DataItemLink = "Source ID" = FIELD("Document No."), "Source Ref. No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = WHERE("Source Type" = CONST(37));
                            column(ResExpirationDate; FORMAT("Reservation Entry"."Expiration Date"))
                            {
                            }
                            column(ResSerialNo; "Reservation Entry"."Serial No.")
                            {
                            }
                            column(ResLotNo; "Reservation Entry"."Lot No.")
                            {
                            }
                            column(ResQuantity; ABS("Reservation Entry".Quantity))
                            {
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49973 16-05-2017 RM
                                if COUNT = 0 then
                                    CurrReport.BREAK;
                                // <<< 2111-49973 16-05-2017 RM
                            end;
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText_DimnLoop2; DimText)
                            {
                            }
                            column(LineDimensionsCaption; LineDimensionsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FINDSET then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(AsmLoop; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(KitPosNo; '')
                            {
                            }
                            column(AsmLineType; AsmLine.Type)
                            {
                            }
                            column(AsmLineNo; BlanksForIndent + AsmLine."No.")
                            {
                            }
                            column(AsmLineDescription; BlanksForIndent + AsmLine.Description)
                            {
                            }
                            column(AsmLineQuantity; AsmLine.Quantity)
                            {
                            }
                            column(AsmLineUOMText; GetUnitOfMeasureDescr(AsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                // >>> 2111-49001 14-02-2017 RM
                                if Number = 1 then
                                    AsmLine.FINDSET
                                else
                                    AsmLine.NEXT;
                                // <<< 2111-49001 14-02-2017 RM
                            end;



                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49001 14-02-2017 RM
                                if not (DisplayAssemblyInfo and AsmInfoExistsForLine) then begin
                                    CurrReport.BREAK;
                                end;

                                AsmLine.SETRANGE("Document Type", AsmHeader."Document Type");
                                AsmLine.SETRANGE("Document No.", AsmHeader."No.");
                                SETRANGE(Number, 1, AsmLine.COUNT);
                                // <<< 2111-49001 14-02-2017 RM
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("Document No."), "Source Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(37), "Print Attribute" = CONST(true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }

                            trigger OnPreDataItem();
                            begin
                                // >>> 2111-49973 16-05-2017 RM
                                if COUNT = 0 then
                                    CurrReport.BREAK;
                                // <<< 2111-49973 16-05-2017 RM
                            end;
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                        begin
                            if Number = 1 then
                                SalesLine.FIND('-')
                            else
                                SalesLine.NEXT;
                            "Sales Line" := SalesLine;

                            // >>> 2111-49973 16-05-2017 RM
                            // >>> 2111-51240 19-07-2017 RM
                            //IF NOT ShowZeroPricedLines AND ("Sales Line"."Unit Price" = 0) THEN BEGIN
                            // >>> 2111-62832 25-06-2019 AC
                            //IF SuppressZeroPricedLines AND ("Sales Line"."Unit Price" = 0) THEN BEGIN
                            if SuppressZeroPricedLines and GeneralSalesManagement.CheckAttachedToLineChanged(SalesLine) then begin
                                LineToSkip := GeneralSalesManagement.CheckLineToSkip(SalesLine);
                            end;
                            if LineToSkip then begin
                                // <<< 2111-62832 25-06-2019 AC
                                // <<< 2111-51240 19-07-2017 RM
                                CurrReport.SKIP;
                            end;
                            // <<< 2111-49973 16-05-2017 RM

                            //>>> 02.10.19   MMC OP
                            if "Sales Line".Type = 2 then begin
                                DeliveryWeek := DATE2DWY("Sales Line"."Planned Delivery Date", 2);
                                DeliveryYear := DATE2DWY("Sales Line"."Planned Delivery Date", 3);
                                DeliveryYearShort := FORMAT(DeliveryYear);
                                DeliveryYearShort := DELSTR(DeliveryYearShort, 1, 2);
                                DeliveryWeekYear := STRSUBSTNO(TEXTDWY, DeliveryWeek, DeliveryYearShort);
                            END;

                            //<<< 02.10.19   MMC OP

                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // >>> 2111-47914 21-09-2016 MMA

                            // >>> 2111-49001 14-02-2017 RM
                            if DisplayAssemblyInfo then begin
                                AsmInfoExistsForLine := SalesLine.AsmToOrderExists(AsmHeader);
                            end;
                            // <<< 2111-49001 14-02-2017 RM

                            if not "Sales Header"."Prices Including VAT" and
                               (SalesLine."VAT Calculation Type" = SalesLine."VAT Calculation Type"::"Full VAT")
                            then
                                SalesLine."Line Amount" := 0;

                            if (SalesLine.Type = SalesLine.Type::"G/L Account") and (not ShowInternalInfo) then
                                "Sales Line"."No." := '';

                            // >>> 2111-33655 09-09-2015 RM
                            if "Sales Line"."Alternative Item" then begin
                                "Sales Line"."Unit Price" := "Sales Line"."Unit Price Alternative Item";
                            end;
                            // <<< 2111-33655 09-09-2015 RM

                            begin
                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    UnitPriceVar := '' else
                                    UnitPriceVar := Format("Sales Line"."Unit Price");

                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    LineDiscVar := '' else
                                    LineDiscVar := Format("Sales Line"."Line Discount %");

                                if "Sales Line"."Assembly Item Line No." > 0 then
                                    LineAmtVar := '' else
                                    LineAmtVar := Format("Sales Line"."Line Amount");

                                if "Sales Line"."Assembly BOM exploded" = true then
                                    LineDiscVar := '';



                                if "Sales Line"."Assembly BOM exploded" = true then
                                    LineAmtVar := FORMAT(("Sales Line".Quantity * "Sales Line"."Unit Price"), 0, '<Precision,2><sign><Integer Thousand><Decimals,3>');


                            end;

                            // >>> 2111-34284 24-11-2015 SMS
                            ItemTariffNo := '';
                            if "Sales Line".Type = "Sales Line".Type::Item then begin
                                // >>> 2111-49614 10-04-2017 MMA
                                //ItemRec.GET("Sales Line"."No.");
                                if ItemRec.GET("Sales Line"."No.") then
                                    // <<< 2111-49614 10-04-2017 MMA
                                    ItemTariffNo := ItemRec."Tariff No.";
                                // >>> 2111-47914 21-09-2016 MMA
                                GetItemCrossReference;
                                // <<< 2111-47914 21-09-2016 MMA
                            end;
                            // <<< 2111-34284 24-11-2015 SMS
                        end;

                        trigger OnPostDataItem();
                        begin
                            SalesLine.DELETEALL;
                        end;

                        trigger OnPreDataItem();
                        begin
                            MoreLines := SalesLine.FIND('+');
                            while MoreLines and (SalesLine.Description = '') and (SalesLine."Description 2" = '') and
                                  (SalesLine."No." = '') and (SalesLine.Quantity = 0) and
                                  (SalesLine.Amount = 0)
                            do
                                MoreLines := SalesLine.NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SalesLine.SETRANGE("Line No.", 0, SalesLine."Line No.");
                            SETRANGE(Number, 1, SalesLine.COUNT);
                            CurrReport.CREATETOTALS(SalesLine."Line Amount", SalesLine."Inv. Discount Amount");
                        end;
                    }
                    dataitem(VATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATBase_VATAmtLine; VATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmt_VATAmtLine; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(LineAmount_VATAmtLine; VATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(InvDiscBaseAmt_VATAmtLine; VATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(InvoiceDiscAmt_VATAmtLine; VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VAT_VATAmtLine; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATIdentifier_VATAmtLine; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATAmountLineVATCaption; VATAmountLineVATCaptionLbl)
                        {
                        }
                        column(VATBaseCaption; VATBaseCaptionLbl)
                        {
                        }
                        column(VATAmtCaption; VATAmtCaptionLbl)
                        {
                        }
                        column(VATAmountSpecificationCaption; VATAmountSpecificationCaptionLbl)
                        {
                        }
                        column(LineAmtCaption; LineAmtCaptionLbl)
                        {
                        }
                        column(InvoiceDiscBaseAmtCaption; InvoiceDiscBaseAmtCaptionLbl)
                        {
                        }
                        column(InvoiceDiscAmtCaption; InvoiceDiscAmtCaptionLbl)
                        {
                        }
                        column(TotalCaption; TotalCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            if VATAmount = 0 then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(
                              VATAmountLine."Line Amount", VATAmountLine."Inv. Disc. Base Amount",
                              VATAmountLine."Invoice Discount Amount", VATAmountLine."VAT Base", VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATCounterLCY; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATCtrl_VATAmtLine; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATIdentifierCtrl_VATAmtLine; VATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);

                            VALVATBaseLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  "Sales Header"."Order Date", "Sales Header"."Currency Code",
                                  VATAmountLine."VAT Base", "Sales Header"."Currency Factor"));
                            VALVATAmountLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  "Sales Header"."Order Date", "Sales Header"."Currency Code",
                                  VATAmountLine."VAT Amount", "Sales Header"."Currency Factor"));
                        end;

                        trigger OnPreDataItem();
                        begin
                            if (not GLSetup."Print VAT specification in LCY") or
                               ("Sales Header"."Currency Code" = '') or
                               (VATAmountLine.GetTotalVATAmount = 0)
                            then
                                CurrReport.BREAK;

                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VALVATBaseLCY, VALVATAmountLCY);

                            if GLSetup."LCY Code" = '' then
                                VALSpecLCYHeader := Text008 + Text009
                            else
                                VALSpecLCYHeader := Text008 + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Header"."Order Date", "Sales Header"."Currency Code", 1);
                            VALExchRate := STRSUBSTNO(Text010, CurrExchRate."Relational Exch. Rate Amount", CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(Total; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesHeader; "Sales Header"."Sell-to Customer No.")
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShiptoAddressCaption; ShiptoAddressCaptionLbl)
                        {
                        }
                        column(SelltoCustNo_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowShippingAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Extended Text - Sales")
                    {
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-22712 17-02-2014 RM
                            NAIExtPostText.RESET;
                            NAIExtPostText.SETRANGE("Document Type", "Sales Header"."Document Type");
                            NAIExtPostText.SETRANGE("Document No.", "Sales Header"."No.");
                            NAIExtPostText.SETRANGE("Assigned for", NAIExtPostText."Assigned for"::Quote);
                            NAIExtPostText.SETRANGE("Text Position", NAIExtPostText."Text Position"::Posttext);
                            // <<< 2111-22712 17-02-2014 RM
                        end;
                    }
                }

                trigger OnAfterGetRecord();
                var
                    SalesPost: Codeunit "Sales-Post";
                begin
                    CLEAR(SalesLine);
                    CLEAR(SalesPost);
                    SalesLine.DELETEALL;
                    VATAmountLine.DELETEALL;
                    SalesPost.GetSalesLines("Sales Header", SalesLine, 0);
                    // >>> 2111-49973 16-05-2017 RM
                    CalcSubTotalAndInvDiscAmount(SalesLine);
                    // <<< 2111-49973 16-05-2017 RM
                    SalesLine.CalcVATAmountLines(0, "Sales Header", SalesLine, VATAmountLine);
                    SalesLine.UpdateVATOnLines(0, "Sales Header", SalesLine, VATAmountLine);
                    VATAmount := VATAmountLine.GetTotalVATAmount;
                    VATBaseAmount := VATAmountLine.GetTotalVATBase;
                    VATDiscountAmount :=
                      VATAmountLine.GetTotalVATDiscount("Sales Header"."Currency Code", "Sales Header"."Prices Including VAT");
                    TotalAmountInclVAT := VATAmountLine.GetTotalAmountInclVAT;

                    if Number > 1 then begin
                        CopyText := Text003;
                        OutputNo += 1;
                    end;
                    CurrReport.PAGENO := 1;
                end;

                trigger OnPostDataItem();
                begin
                    if Print then
                        SalesCountPrinted.RUN("Sales Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);

                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                "Sell-to Country": Text[50];
                "---NAI---": Integer;
                GeneralReportMgmt: Codeunit "General Report Management";
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");
                // >>> 2111-49001 14-02-2017 RM
                CompanyInfo.GET;
                // <<< 2111-49001 14-02-2017 RM

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 14-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, MAXSTRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 14-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 14-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 14-02-2017 RM
                end;

                // >>> 15.10.19 MMC OP

                if not Cust.GET("Bill-to Customer No.") then
                    CLEAR(Cust);
                User.RESET;
                User.SETRANGE("User Name", "Sales Header"."Assigned User ID");
                IF NOT User.FINDFIRST THEN
                    User.INIT;
                //<<< 15.10.19 MMC OP

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                // >>> 2111-49001 14-02-2017 RM
                if ShowOfficeSalesPerson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 14-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 14-02-2017 RM
                end;
                // <<< 2111-49001 14-02-2017 RM

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                if "VAT Registration No." = '' then
                    VATNoText := ''
                else
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                if "Currency Code" = '' then begin
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006, GLSetup."LCY Code");
                end else begin
                    TotalText := STRSUBSTNO(Text001, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006, "Currency Code");
                end;
                FormatAddr.SalesHeaderBillTo(CustAddr, "Sales Header");

                if "Payment Terms Code" = '' then
                    PaymentTerms.INIT
                else begin
                    PaymentTerms.GET("Payment Terms Code");
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                end;
                if "Shipment Method Code" = '' then
                    ShipmentMethod.INIT
                else begin
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                end;

                if Country.GET("Sell-to Country/Region Code") then
                    "Sell-to Country" := Country.Name;
                FormatAddr.SalesHeaderShipTo(ShipToAddr, CustAddr, "Sales Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                for i := 1 to ARRAYLEN(ShipToAddr) do
                    if (ShipToAddr[i] <> CustAddr[i]) and (ShipToAddr[i] <> '') and (ShipToAddr[i] <> "Sell-to Country") then
                        ShowShippingAddr := true;

                if Print then begin
                    if ArchiveDocument then
                        ArchiveManagement.StoreSalesDocument("Sales Header", LogInteraction);

                    if LogInteraction then begin
                        CALCFIELDS("No. of Archived Versions");
                        if "Bill-to Contact No." <> '' then
                            SegManagement.LogDocument(
                              1, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Contact, "Bill-to Contact No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.")
                        else
                            SegManagement.LogDocument(
                              1, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Customer, "Bill-to Customer No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.");
                    end;
                end;
                MARK(true);

                // >>> 2111-23178 31-07-2014 UQ
                ShowDiscount := IsDiscount("Sales Header");
                // <<< 2111-23178 31-07-2014 UQ

                // >>> 2111-62234 12-03-2018 RM
                ShowSellToInfo := not GeneralReportMgmt.SalesHeaderSellToEqualsBillTo("Sales Header");
                // <<< 2111-62234 12-03-2018 RM

                // >>> 2111-62832 26-06-2019 AC
                GeneralSalesManagement.ClearAttachToVariables;
                // <<< 2111-62832 26-06-2019 AC
            end;

            trigger OnPostDataItem();
            var
                ToDo: Record "To-do";
            begin
                MARKEDONLY := true;
                COMMIT;
                CurrReport.LANGUAGE := GLOBALLANGUAGE;
                if FIND('-') and ToDo.WRITEPERMISSION then
                    if Print and (NoOfRecords = 1) then;
                //if CONFIRM(Text007) then;
                // >>> 2111-62118 06-09-2018 SMS
                //CreateTodo;
                //CreateTask;
                // <<< 2111-62118 06-09-2018 SMS
            end;

            trigger OnPreDataItem();
            var
                "---NAI---": Integer;
                PdfReportMgmt: Codeunit "NAI - Pdf Report Mgmt.";
            begin
                NoOfRecords := COUNT;
                Print := Print or not CurrReport.PREVIEW;

                // >>> 2111-49001 14-02-2017 RM
                AsmInfoExistsForLine := false;
                // <<< 2111-49001 14-02-2017 RM

                // >>> 2111-30349 27-03-2015 RW
                if FINDFIRST then begin
                    PdfReportMgmt.CheckDocumentArchiving("Print as PDF", 1, "Document Type", ArchiveDocument);
                    "Print as PDF" := false;
                    MODIFY;
                end;
                // <<< 2111-30349 27-03-2015 RW
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                        ToolTipML = DEU = 'Gibt an, wie viele Kopien des Belegs gedruckt werden sollen.',
                                    ENU = 'Specifies how many copies of the document to print.';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                        ToolTipML = DEU = 'Gibt an, ob der gedruckte Bericht Informationen anzeigen soll, die nur für die interne Verwendung sind.',
                                    ENU = 'Specifies if you want the printed report to show information that is only for internal use.';
                    }
                    field(ArchiveDocument; ArchiveDocument)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Beleg archivieren',
                                    ENU = 'Archive Document';
                        ToolTipML = DEU = 'Gibt an, ob der Beleg nach dem Drucken archiviert wird.',
                                    ENU = 'Specifies if the document is archived after you print it.';

                        trigger OnValidate();
                        begin
                            if not ArchiveDocument then
                                LogInteraction := false;
                        end;
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ToolTipML = DEU = 'Gibt an, ob die Serviceangebote erfasst werden sollen, die als Aktivitäten ausgedruckt und zur Tabelle "Aktivitätenprotokollposten" hinzugefügt werden sollen.',
                                    ENU = 'Specifies if you want to record the service quotes that you want to print as interactions and add them to the Interaction Log Entry table.';

                        trigger OnValidate();
                        begin
                            if LogInteraction then
                                ArchiveDocument := ArchiveDocumentEnable;
                        end;
                    }
                    field(ShowAssemblyComponents; DisplayAssemblyInfo)
                    {
                        ApplicationArea = Assembly;
                        CaptionML = DEU = 'Montagekomponenten anzeigen',
                                    ENU = 'Show Assembly Components';
                        ToolTipML = DEU = 'Gibt an, ob der Bericht Informationen zu Komponenten enthalten soll, die in verknüpften Montageaufträgen verwendet wurden, die den/die verkauften Artikel bereitstellten.',
                                    ENU = 'Specifies if you want the report to include information about components that were used in linked assembly orders that supplied the item(s) being sold.';
                    }
                    field(ShowOfficeSalesPerson; ShowOfficeSalesPerson)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                        ToolTipML = DEU = 'Gibt an, ob der Beleg den Verkäufer aus dem Innendienst anzeigt.',
                                    ENU = 'Specifies if you want the printed report to show the office sales person.';
                    }
                    field(SuppressZeroPricedLines; SuppressZeroPricedLines)
                    {
                        ApplicationArea = Advanced;
                        CaptionML = DEU = 'Positionen ohne VK-Preis unterdrücken',
                                    ENU = 'Suppress Positions without Sales Price';
                        ToolTipML = DEU = 'Gibt an, ob der Beleg Positionen ohne VK-Preis druckt.',
                                    ENU = 'Specifies if you want the printed report to show positions without sales price.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
            ArchiveDocument := SalesSetup."Archive Quotes and Orders";
        end;

        trigger OnOpenPage();
        var
            "---NAI---": Integer;
            NAISetup: Record "NAI - Setup";
        begin
            // >>> 2111-36574 21-06-2016 MMA
            if NAISetup.GET then begin
                // >>> 2111-51240 19-07-2017 RM
                //ShowZeroPricedLines := NAISetup."Show Pos w/o Sales Price";
                SuppressZeroPricedLines := NAISetup."Suppress 0-Price-Pos. On Quote";
                // <<< 2111-51240 19-07-2017 RM
            end;
            // <<< 2111-36574 21-06-2016 MMA
            LogInteraction := SegManagement.FindInteractTmplCode(1) <> '';

            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        GLSetup.GET;
        CompanyInfo.GET;
        SalesSetup.GET;
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'Total %1', ENU = 'Total %1', ITA = 'Totale %1';
        Text002: TextConst DEU = 'Total %1 inkl. MwSt.', ENU = 'Total %1 Incl. VAT', ITA = 'Totale %1 incl. IVA';
        Text003: TextConst DEU = 'KOPIE', ENU = 'COPY';
        Text004: TextConst DEU = 'Angebot %1 %2', ENU = 'Quote %1 %2';
        Text005: TextConst DEU = 'Seite %1', ENU = 'Page %1';
        Text006: TextConst DEU = 'Total %1 ohne MwSt.', ENU = 'Total %1 Excl. VAT', ITA = 'Totale %1 escl. IVA';
        GLSetup: Record "General Ledger Setup";
        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo2: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        VATAmountLine: Record "VAT Amount Line" temporary;
        SalesLine: Record "Sales Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        RespCenter: Record "Responsibility Center";
        Language: Record Language;
        Country: Record "Country/Region";
        CurrExchRate: Record "Currency Exchange Rate";
        User: Record User;
        Cust: Record Customer;
        ExpectedWeekNumber: Integer;
        DeliveryWeek: Integer;
        DeliveryYear: Integer;
        DeliveryYearShort: Text[10];
        DeliveryWeekYear: Text[10];

        SalesCountPrinted: Codeunit "Sales-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        ArchiveManagement: Codeunit ArchiveManagement;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        ArchiveDocument: Boolean;
        LogInteraction: Boolean;
        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        Text007: TextConst DEU = 'Möchten Sie eine Nachfassaufgabe erstellen?', ENU = 'Do you want to create a follow-up to-do?';
        NoOfRecords: Integer;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        VALSpecLCYHeader: Text[80];
        VALExchRate: Text[50];
        Text008: TextConst DEU = 'MwSt.-Betrag Spezifikation in ', ENU = 'VAT Amount Specification in ';
        Text009: TextConst DEU = 'Landeswährung', ENU = 'Local Currency';
        Text010: TextConst DEU = 'Wechselkurs: %1/%2', ENU = 'Exchange rate: %1/%2';
        OutputNo: Integer;
        Print: Boolean;
        [InDataSet]
        ArchiveDocumentEnable: Boolean;
        [InDataSet]
        LogInteractionEnable: Boolean;
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.', ITA = 'Ns. num. di partita IVA';
        CompanyInfoBankNameCaptionLbl: TextConst DEU = 'Bank', ENU = 'Bank';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        SalesHeaderShipmentDateCaptionLbl: TextConst DEU = 'Warenausg.-Datum', ENU = 'Shipment Date';
        SalesHeaderNoCaptionLbl: TextConst DEU = 'Angebotsnr.', ENU = 'Quote No.';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        UnitPriceCaptionLbl: TextConst DEU = 'VK-Preis', ENU = 'Unit Price', ITA = 'prezzo unit.';
        SalesLineLineDiscCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Disc. %';
        AmountCaptionLbl: TextConst DEU = 'Betrag', ENU = 'Amount', ITA = 'Totale Euro';
        SalesLineVATIdentifierCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        SalesLineInvDiscAmtCaptionLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Invoice Discount Amount', ITA = 'sovrapprezzo';
        SubtotalCaptionLbl: TextConst DEU = 'Zwischensumme', ENU = 'Subtotal';
        VATDiscountAmountCaptionLbl: TextConst DEU = 'Skonto auf MwSt.', ENU = 'Payment Discount on VAT';
        LineDimensionsCaptionLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        VATAmountLineVATCaptionLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %';
        VATBaseCaptionLbl: TextConst DEU = 'MwSt.-Bemessungsgrundlage', ENU = 'VAT Base';
        VATAmtCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        VATAmountSpecificationCaptionLbl: TextConst DEU = 'MwSt.-Betrag Spezifikation', ENU = 'VAT Amount Specification';
        LineAmtCaptionLbl: TextConst DEU = 'Zeilenbetrag', ENU = 'Line Amount';
        InvoiceDiscBaseAmtCaptionLbl: TextConst DEU = 'Rechnungsrab.-Bemessungsgr.', ENU = 'Invoice Discount Base Amount';
        InvoiceDiscAmtCaptionLbl: TextConst DEU = 'Rechnungsrab.-Betrag', ENU = 'Invoice Discount Amount';
        TotalCaptionLbl: TextConst DEU = 'Gesamt', ENU = 'Total';
        PaymentTermsDescriptionCaptionLbl: TextConst DEU = 'Zahlungsbedingungen', ENU = 'Payment Terms', ITA = 'Cond. di pag.';
        ShipmentMethodDescriptionCaptionLbl: TextConst DEU = 'Lieferbedingung', ENU = 'Shipment Method', ITA = 'Mod. di consegna';
        "---NAI---": Integer;
        AsmHeader: Record "Assembly Header";
        AsmLine: Record "Assembly Line";
        DisplayAssemblyInfo: Boolean;
        AsmInfoExistsForLine: Boolean;
        ShowDiscount: Boolean;
        SuppressZeroPricedLines: Boolean;
        ItemTariffNo: Code[20];
        CustomerItemNo: Code[20];
        ShiptoAddressCaptionLbl: TextConst DEU = 'Lief. an Adresse', ENU = 'Ship-to Address', ITA = 'Indirizzo di consegna';
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ';
        ShowOfficeSalesPerson: Boolean;
        CompanyInfoHomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        CompanyInfoEmailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date', ITA = 'Data offerta';
        SalesLineAllowInvoiceDiscCaptionLbl: TextConst DEU = 'Rech.-Rabatt zulassen', ENU = 'Allow Invoice Discount';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        SalesHeaderBilltoCustomerNoCaptionLbl: TextConst DEU = 'Kundennr.', ENU = 'Customer No.', ITA = 'Vs codice cliente';
        Text5000000: TextConst DEU = 'des Angebots %1', ENU = 'of Quote %1';
        CompanyAddressLine: Text[160];
        SalesLinePositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.', ITA = 'Pos.n.';
        Sales_Line___Line_Discount___CaptionLbl: TextConst DEU = 'Rab. %', ENU = 'Disc. %', ITA = 'Sconto %';
        UnitofMeasure_SalesLineCaptionLbl: TextConst DEU = 'Einheit', ENU = 'Unit', FRA = 'Unité', NLD = 'Eenheid', ITA = 'Unità di misura';
        QuoteLbl: TextConst DEU = 'Angebotsnr.', ENU = 'Quote No.', ITA = 'Offerta n.';
        QuoteLbl2: TextConst DEU = 'ANGEBOT', ENU = 'QUOTE', ITA = 'OFFERTA';
        QuoteLbl3: TextConst DEU = '(bitte immer spezifizieren)', ENU = '(please always notice)', ITA = '(pregasi indicare come rif.)';
        UserLbl: TextConst DEU = 'Sachbearbeiter', ENU = 'Executive Officer', ITA = 'Addetto';
        ShipmentCodetxt: TextConst DEU = 'Transportart', ENU = 'Shipment Code', ITA = 'Forma di Transporto';
        Currencycpt: TextConst DEU = 'Landeswährung', ENU = 'Local Currency', ITA = 'Valuta';
        validtotxt: TextConst DEU = 'Gültig bis:', ENU = 'Valid to:', ITA = 'Validità fino al:';
        OurAccountCaption: TextConst DEU = 'Lieferantennummer', ENU = 'Account No.', ITA = 'Ns cod. fornitore';
        TEXTDWY: TextConst DEU = '%1.%2', ENU = '%1.%2', ITA = '%1.%2';
        ExpectedWeekNumberCaptionLbl: TextConst DEU = 'LW.JJ', ENU = 'DW.YY', ITA = 'sett.cons.';
        SubTotal: Decimal;
        InvDiscAmount: Decimal;
        ShowSellToInfo: Boolean;
        LineToSkip: Boolean;
        GeneralSalesManagement: Codeunit "General Sales Management";
        UPV: Text[20];
        LDV: Text[20];
        SAV: Text[20];
        UnitPriceVar: Text[20];
        LineDiscVar: Text[20];
        LineAmtVar: Text[20];



    procedure InitializeRequest(NoOfCopiesFrom: Integer; ShowInternalInfoFrom: Boolean; ArchiveDocumentFrom: Boolean; LogInteractionFrom: Boolean; PrintFrom: Boolean);
    begin
        NoOfCopies := NoOfCopiesFrom;
        ShowInternalInfo := ShowInternalInfoFrom;
        ArchiveDocument := ArchiveDocumentFrom;
        LogInteraction := LogInteractionFrom;
        Print := PrintFrom;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[10];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        if not UnitOfMeasure.GET(UOMCode) then
            exit(UOMCode);
        exit(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        exit(PADSTR('', 2, ' '));
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    procedure IsDiscount(SalesHeaderDisc: Record "Sales Header"): Boolean;
    var
        SalesLineDisc: Record "Sales Line";
    begin
        // >>> 2111-23178 31-07-2014 UQ
        SalesLineDisc.SETRANGE("Document Type", SalesHeaderDisc."Document Type");
        SalesLineDisc.SETRANGE("Document No.", SalesHeaderDisc."No.");
        SalesLineDisc.SETFILTER("Line Discount %", '<>0');
        exit(not SalesLineDisc.ISEMPTY);
        // <<< 2111-23178 31-07-2014 UQ
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-47914 21-09-2016 MMA
        with "Sales Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-47914 21-09-2016 MMA
    end;

    local procedure CalcSubTotalAndInvDiscAmount(var SalesLine: Record "Sales Line");
    begin
        // >>> 2111-49973 16-05-2017 RM
        SalesLine.CALCSUMS("Line Amount", "Inv. Discount Amount");
        SubTotal := SalesLine."Line Amount";
        InvDiscAmount := SalesLine."Inv. Discount Amount";
        // <<< 2111-49973 16-05-2017 RM
    end;
}

