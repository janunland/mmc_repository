report 50009 "MMC ReturnShipmentDoc"
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report/ReturnShipmentDoc.rdl';

    CaptionML = DEU = 'Verkauf - Retourenschein',
                ENU = 'Return Shipment Doc.';

    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            CalcFields = "Total Gross Weight", "Total Net Weight";
            DataItemTableView = SORTING("Document Type", "No.") WHERE("Document Type" = CONST(Order));
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeadingML = DEU = 'Verkaufsauftrag',
                                     ENU = 'Sales Order';
            column(DocType_SalesHeader; "Document Type")
            {
            }
            column(No_SalesHeader; "No.")
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(PaymentTermsCaption; PaymentTermsCaptionLbl)
            {
            }
            column(ShipmentMethodCaption; ShipmentMethodCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(AllowInvDiscCaption; AllowInvDiscCaptionLbl)
            {
            }
            column(ShowDiscount; ShowDiscount)
            {
            }
            column(SumNetWeightCaption; SumNetWeightLbl)
            {
            }
            column(SumGrossWeightCaption; SumGrossWeightLbl)
            {
            }
            column(ShipmentDateHeaderCaption; FIELDCAPTION("Shipment Date"))
            {
            }
            column(ShipmentDateOrWeek; ShipmentDateOrWeek)
            {
            }
            column(LanguageCode; "Language Code")
            {
            }
            column(PackageTrackingNo; "Package Tracking No.")
            {
                Description = '2111-62614 29-10-2018 CFL';
            }
            column(PackageTrackingNo_Caption; FIELDCAPTION("Package Tracking No."))
            {
            }
            column(MMCN1; MMC1) { }
            column(MMCN2; MMC2) { }
            column(MMCN3; MMC3) { }

            dataitem(CopyLoop; "Integer")
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; "Integer")
                {
                    DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                    column(CompanyInfo2Picture; CompanyInfo2.Picture)
                    {
                    }
                    column(OrderConfirmCopyCaption; STRSUBSTNO(Text004, "Sales Header"."No.", CopyText))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(ShowSellToInformation; ShowSellToInfo)
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name; "Sales Header"."Sell-to Customer Name")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name2; "Sales Header"."Sell-to Customer Name 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Name3; "Sales Header"."Sell-to Customer Name 3")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Name4; "Sales Header"."Sell-to Customer Name 4")
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SellTo_Address; "Sales Header"."Sell-to Address")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Address2; "Sales Header"."Sell-to Address 2")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_Postcode; "Sales Header"."Sell-to Post Code")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(SellTo_City; "Sales Header"."Sell-to City")
                    {
                        Description = '2111-62165 04-01-2018 MA';
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesHeader; "Sales Header"."Bill-to Customer No.")
                    {
                    }
                    column(DocDate_SalesHeader; FORMAT("Sales Header"."Document Date"))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesHeader; "Sales Header"."VAT Registration No.")
                    {
                    }
                    column(ShptDate_SalesHeader; FORMAT("Sales Header"."Shipment Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonPhoneNo; SalesPurchPerson."Phone No.")
                    {
                    }
                    column(SalesPurchPersonEMail; SalesPurchPerson."E-Mail")
                    {
                    }
                    column(SalesPurchPersonPhoneNoCaption; SalesPurchPersonPhoneNoCaptionLbl)
                    {
                    }
                    column(SalesPurchPersonEMailCaption; SalesPurchPerson.FIELDCAPTION("E-Mail"))
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourReference_SalesHeader; "Sales Header"."Your Reference")
                    {
                    }
                    column(SalesHeaderExternalDocumentNo; "Sales Header"."External Document No.")
                    {
                    }
                    column(SalesHeaderExternalDocumentNoCaption; "Sales Header".FIELDCAPTION("External Document No."))
                    {
                    }
                    column(ExpectedWeekNumber; ExpectedWeekNumberText)
                    {
                    }
                    column(ExpectedWeekNumberCaption; ExpectedWeekNumberCaptionLbl)
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(PricesInclVAT_SalesHeader; "Sales Header"."Prices Including VAT")
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text005, ''))
                    {
                    }
                    column(Text5000000_SalesHeaderNo; STRSUBSTNO(Text5000000, "Sales Header"."No."))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PaymentTermsDescription; PaymentTerms.Description)
                    {
                    }
                    column(ShipmentMethodDescription; ShipmentMethod.Description)
                    {
                    }
                    column(PricesInclVATYesNo_SalesHeader; FORMAT("Sales Header"."Prices Including VAT"))
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(AccountNoCaption; AccountNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(OrderNoCaption; OrderNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoHomePageCaption; CompanyInfoHomePageCaptionLbl)
                    {
                    }
                    column(CompanyInfoEmailCaption; CompanyInfoEmailCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesHeaderCaption; SalesHeaderBilltoCustomerNoCaptionLbl)
                    {
                    }
                    column(PricesInclVAT_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(PrintQuantityWp; false)
                    {
                    }
                    column(PrintAsPDF; false)
                    {
                    }
                    column(PrintPrepPlan; PrintPrepPlan)
                    {
                    }
                    column(CompanyAddressLine; CompanyAddressLine)
                    {
                    }
                    column(CompanyInfoPlaceofJurisdiction; '')
                    {
                    }
                    column(CompanyInfoTradingRegistrationNo; '')
                    {
                    }
                    column(CompanyInfoBankBranchNo; CompanyInfo."Bank Branch No.")
                    {
                    }
                    column(CompanyInfoCity; CompanyInfo.City)
                    {
                    }
                    column(CompanyInfoAddress; CompanyInfo.Address)
                    {
                    }
                    column(CompanyInfoPostCode; CompanyInfo."Post Code")
                    {
                    }
                    column(CompanyInfoName; CompanyInfo.Name)
                    {
                    }
                    column(CompanyInfoFaxNoCaption; CompanyInfo.FIELDCAPTION("Fax No."))
                    {
                    }
                    column(CompanyInfoVATRegNoCaption; CompanyInfoVATRegNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankAccountNoCaption; CompanyInfoBankAccountNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoTradingRegistrationNoCaption; '')
                    {
                    }
                    column(CompanyInfoBankBranchNoCaption; CompanyInfoBankBranchNoCaptionLbl)
                    {
                    }
                    column(PpmtAmount; PpmtAmount)
                    {
                    }
                    column(PpmtAmount_Caption; PpmtAmountCaptionLbl)
                    {
                    }
                    column(SalesHeader_PrepaymentType; "Sales Header"."Prepayment Type")
                    {
                    }
                    column(SalesHeader_PrepaymentPercentage; "Sales Header"."Prepayment %")
                    {
                    }
                    column(SalesHeader_PrepmtPaymentDiscountPercentage; "Sales Header"."Prepmt. Payment Discount %")
                    {
                    }
                    column(SalesHeader_PrepmtPmtDiscountDate; "Sales Header"."Prepmt. Pmt. Discount Date")
                    {
                    }
                    column(SalesHeader_PrepmtPaymentDiscountCaption; SalesHeader_PrepmtPaymentDiscountCaptionLbl)
                    {
                    }
                    column(SalesHeader_PrepmtPmtDiscountDateCaption; SalesHeader_PrepmtPmtDiscountDateCaptionLbl)
                    {
                    }
                    column(SumNetWeight; SumNetWeight)
                    {
                        AutoCalcField = true;
                    }
                    column(SumGrossWeight; SumGrossWeight)
                    {
                    }
                    column(ShowNetWeight; ShowNetWeight)
                    {
                    }
                    column(ShowGrossWeight; ShowGrossWeight)
                    {
                    }
                    column(SalesHeader_PromisedDeliveryDate; "Sales Header"."Promised Delivery Date")
                    {
                    }
                    column(SalesHeader_PromisedDeliveryDateCaption; "Sales Header".FIELDCAPTION("Promised Delivery Date"))
                    {
                        Description = '2111-62234 13-03-2018 RM';
                    }
                    column(SalesHeader_RequestedDeliveryDate; "Sales Header"."Requested Delivery Date")
                    {
                    }
                    column(PrePAmountDec; PrePAmountDec)
                    {
                    }
                    column(AssociationName; AssociationName)
                    {
                    }
                    column(CustAssociationSortCode; CustAssociationSortCode)
                    {
                    }
                    column(Commission; "Sales Header".Commission)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(CommissionCaption; "Sales Header".FIELDCAPTION(Commission))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonCaption; "Sales Header".FIELDCAPTION("Office Sales Person"))
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonName; OfficeSalesperson.Name)
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonEMail; OfficeSalesperson."E-Mail")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    column(SalesPurchOfficePersonPhoneNo; OfficeSalesperson."Phone No.")
                    {
                        Description = '2111-62379 20-06-2018 BD';
                    }
                    dataitem(DimensionLoop1; "Integer")
                    {
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1_Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not DimSetEntry1.FIND('-') then
                                    CurrReport.BREAK;
                            end else
                                if not Continue then
                                    CurrReport.BREAK;

                            CLEAR(DimText);
                            Continue := false;
                            repeat
                                OldDimText := DimText;
                                if DimText = '' then
                                    DimText := STRSUBSTNO('%1 %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                else
                                    DimText :=
                                      STRSUBSTNO(
                                        '%1, %2 %3', DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                    DimText := OldDimText;
                                    Continue := true;
                                    exit;
                                end;
                            until DimSetEntry1.NEXT = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            if not ShowInternalInfo then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPreText; "NAI Extended Text - Sales")
                    {
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PreText; Text)
                        {
                        }
                        column(PreText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 14-02-2017 RM
                            NAIExtPreText.RESET;
                            NAIExtPreText.SETRANGE("Document Type", "Sales Header"."Document Type");
                            NAIExtPreText.SETRANGE("Document No.", "Sales Header"."No.");
                            NAIExtPreText.SETRANGE("Assigned for", NAIExtPreText."Assigned for"::Order);
                            NAIExtPreText.SETRANGE("Text Position", NAIExtPreText."Text Position"::Pretext);
                            // <<< 2111-49001 14-02-2017 RM
                        end;
                    }
                    dataitem("Header Attributes"; "Attribute Value")
                    {
                        DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(36), "Print Attribute" = CONST(true));
                        column(AttributeCode_HeaderAttributes; "Attribute Code")
                        {
                        }
                        column(Description_HeaderAttributes; Description)
                        {
                        }
                        column(Value_HeaderAttributes; Value)
                        {
                        }
                    }
                    dataitem("Sales Line"; "Sales Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"), "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                        trigger OnPreDataItem();
                        begin
                            CurrReport.BREAK;
                        end;
                    }
                    dataitem(RoundLoop; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(SalesLineAmt; SalesLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(LineAmount_txt; LineAmount_txt)
                        {
                        }
                        column(Desc_SalesLine; "Sales Line".Description)
                        {
                        }
                        column(Desc_SalesLine2; SalesLine."Description 2")
                        {
                        }
                        column(Desc_SalesLine3; SalesLine."Description 3")
                        {
                        }
                        column(NNCSalesLineLineAmt; NNCSalesLineLineAmt)
                        {
                        }
                        column(NNCSalesLineInvDiscAmt; NNCSalesLineInvDiscAmt)
                        {
                        }
                        column(NNCTotalLCY; NNCTotalLCY)
                        {
                        }
                        column(NNCTotalExclVAT; NNCTotalExclVAT)
                        {
                        }
                        column(NNCVATAmt; NNCVATAmt)
                        {
                        }
                        column(NNCTotalInclVAT; NNCTotalInclVAT)
                        {
                        }
                        column(NNCPmtDiscOnVAT; NNCPmtDiscOnVAT)
                        {
                        }
                        column(NNCTotalInclVAT2; NNCTotalInclVAT2)
                        {
                        }
                        column(NNCVATAmt2; NNCVATAmt2)
                        {
                        }
                        column(NNCTotalExclVAT2; NNCTotalExclVAT2)
                        {
                        }
                        column(VATBaseDisc_SalesHeader; "Sales Header"."VAT Base Discount %")
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(No2_SalesLine; "Sales Line"."No.")
                        {
                        }
                        column(Qty_SalesLine; "Sales Line".Quantity)
                        {
                        }
                        column(Quantity_txt; Quantity_txt)
                        {
                        }
                        column(UOM_SalesLine; "Sales Line"."Unit of Measure")
                        {
                        }
                        column(UnitPrice_SalesLine; "Sales Line"."Unit Price")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 2;
                        }
                        column(UnitPrice_txt; UnitPrice_txt)
                        {
                        }
                        column(LineDisc_SalesLine; "Sales Line"."Line Discount %")
                        {
                        }
                        column(Discount_txt; Discount_txt)
                        {
                        }
                        column(LineAmt_SalesLine; "Sales Line"."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(AllowInvDisc_SalesLine; "Sales Line"."Allow Invoice Disc.")
                        {
                        }
                        column(VATIdentifier_SalesLine; "Sales Line"."VAT Identifier")
                        {
                        }
                        column(Type_SalesLine; FORMAT("Sales Line".Type))
                        {
                        }
                        column(No_SalesLine; "Sales Line"."Line No.")
                        {
                        }
                        column(AllowInvDiscountYesNo_SalesLine; FORMAT("Sales Line"."Allow Invoice Disc."))
                        {
                        }
                        column(AsmInfoExistsForLine; AsmInfoExistsForLine)
                        {
                        }
                        column(SalesLineInvDiscAmt; VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(SalsLinAmtExclLineDiscAmt; SalesLine."Line Amount" - VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(VATAmtLineVATAmtText3; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(VATAmount; VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(SalesLineAmtExclLineDisc; SalesLine."Line Amount" - VATAmountLine."Invoice Discount Amount" + VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATDiscountAmount; VATDiscountAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseAmount; VATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(DiscountPercentCaption; DiscountPercentCaptionLbl)
                        {
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(PaymentDiscountVATCaption; PaymentDiscountVATCaptionLbl)
                        {
                        }
                        column(Desc_SalesLineCaption; "Sales Line".FIELDCAPTION(Description))
                        {
                        }
                        column(No2_SalesLineCaption; NO2Lbl)
                        {
                        }
                        column(Qty_SalesLineCaption; QtyLbl)
                        {
                        }
                        column(UOM_SalesLineCaption; UOM_SalesLineCaptionLbl)
                        {
                        }
                        column(VATIdentifier_SalesLineCaption; "Sales Line".FIELDCAPTION("VAT Identifier"))
                        {
                        }
                        column(PositionNo_SalesLine; "Sales Line"."Position No.")
                        {
                        }
                        column(QuantityWp_SalesLine; 0.0)
                        {
                        }
                        column(PositionNo_SalesLineCaption; SalesLinePositionNoCaptionLbl)
                        {
                        }
                        column(QuantityWp_SalesLineCaption; '')
                        {
                        }
                        column(PrepmtPmtTermsDesc; PrepmtPaymentTerms.Description)
                        {
                        }
                        column(PrepmtPmtTermsDescCaption; PrepmtPmtTermsDescCaptionLbl)
                        {
                        }
                        column(Sales_Line___Line_Discount___Caption; Sales_Line___Line_Discount___CaptionLbl)
                        {
                        }
                        column(HideInvoiceLine; HideInvoiceLine)
                        {
                        }
                        column(CustomerItemNo; CustomerItemNo)
                        {
                        }
                        column(CustomerItemNoCaptionLbl; CustomerItemNoCaptionLbl)
                        {
                        }
                        column(SalesLine_OutstandingQuantity; "Sales Line"."Outstanding Quantity")
                        {
                        }
                        column(ItemTariffNo; ItemTariffNo)
                        {
                        }
                        column(PromisedDeliveryDate_SalesLine; "Sales Line"."Promised Delivery Date")
                        {
                            Description = '2111-51811 30-08-2017 RM';
                        }
                        column(PromisedDeliveryDate_SalesLineCaption; "Sales Line".FIELDCAPTION("Promised Delivery Date"))
                        {
                            Description = '2111-51811 30-08-2017 RM';
                        }
                        column(GTINLbl; GTINLbl)
                        {
                            Description = '2111-62114 12-12-2017 JH';
                        }
                        column(ItemGTIN; ItemGTIN)
                        {
                            Description = '2111-62114 12-12-2017 JH';
                        }
                        column(CountryOfOrigin_SalesLine; "Sales Line"."Country/Region of Origin Code")
                        {
                        }

                        column(TextITLINE01CPT; TextITLINE01) { }
                        column(TextITLINE02CPT; TextITLINE02) { }
                        column(TextITLINE03CPT; TextITLINE03) { }
                        column(TextITLINE04CPT; TextITLINE04) { }

                        dataitem("Reservation Entry"; "Reservation Entry")
                        {
                            DataItemLink = "Source ID" = FIELD("Document No."), "Source Ref. No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Source ID", "Source Ref. No.", "Source Type", "Source Subtype", "Source Batch Name", "Source Prod. Order Line", "Reservation Status", "Shipment Date", "Expected Receipt Date") WHERE("Source Type" = CONST(37));
                            column(ResExpirationDate; FORMAT("Reservation Entry"."Expiration Date"))
                            {
                            }
                            column(ResSerialNo; "Reservation Entry"."Serial No.")
                            {
                            }
                            column(ResLotNo; "Reservation Entry"."Lot No.")
                            {
                            }
                            column(ResQuantity; ABS("Reservation Entry".Quantity))
                            {
                            }
                        }
                        dataitem("Tracking Specification"; "Tracking Specification")
                        {
                            DataItemLink = "Source ID" = FIELD("Document No."), "Source Ref. No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Source ID", "Source Type", "Source Subtype", "Source Batch Name", "Source Prod. Order Line", "Source Ref. No.") WHERE("Source Type" = CONST(37));
                            column(TrackExpirationDate; FORMAT("Tracking Specification"."Expiration Date"))
                            {
                            }
                            column(TrackResSerialNo; "Tracking Specification"."Serial No.")
                            {
                            }
                            column(TrackResLotNo; "Tracking Specification"."Lot No.")
                            {
                            }
                            column(TrackResQuantity; ABS("Tracking Specification"."Quantity (Base)"))
                            {
                            }
                        }
                        dataitem(ValueEntry2; "Value Entry")
                        {
                            DataItemLink = "Document No." = FIELD("Document No."), "Document Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Item Ledger Entry No.", "Document No.", "Document Line No.");
                            column(ItemEntrySerialNo; ItemSerialNo)
                            {
                            }
                            column(ItemEntryLotNo; ItemLotNo)
                            {
                            }
                            column(ItemEntryExpirationDate; FORMAT(ItemExpirationDate))
                            {
                            }
                            column(ItemEntryQuantity; ABS(ItemQuantity))
                            {
                            }

                            trigger OnAfterGetRecord();
                            var
                                ItemLedgerEntry2: Record "Item Ledger Entry";
                            begin
                                // >>> 2111-34284 25-11-2015 SMS
                                ItemSerialNo := '';
                                ItemLotNo := '';
                                ItemExpirationDate := 0D;
                                ItemQuantity := 0;
                                if ItemLedgerEntry2.GET(ValueEntry2."Item Ledger Entry No.") then begin
                                    ItemSerialNo := ItemLedgerEntry2."Serial No.";
                                    ItemLotNo := ItemLedgerEntry2."Lot No.";
                                    ItemExpirationDate := ItemLedgerEntry2."Expiration Date";
                                    ItemQuantity := ItemLedgerEntry2.Quantity;
                                end;
                                // <<< 2111-34284 25-11-2015 SMS
                            end;
                        }
                        dataitem(DimensionLoop2; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText2; DimText)
                            {
                            }
                            column(LineDimCaption; LineDimCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not DimSetEntry2.FINDSET then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText := STRSUBSTNO('%1 %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until DimSetEntry2.NEXT = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not ShowInternalInfo then
                                    CurrReport.BREAK;

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(AsmLoop; "Integer")
                        {
                            DataItemTableView = SORTING(Number);
                            column(KitPosNo; '')
                            {
                            }
                            column(AsmLineType; AsmLine.Type)
                            {
                            }
                            column(AsmLineNo; BlanksForIndent + AsmLine."No.")
                            {
                            }
                            column(AsmLineDescription; BlanksForIndent + AsmLine.Description)
                            {
                            }
                            column(AsmLineQuantity; AsmLine.Quantity)
                            {
                            }
                            column(AsmLineUOMText; GetUnitOfMeasureDescr(AsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then
                                    AsmLine.FINDSET
                                else
                                    AsmLine.NEXT;
                            end;

                            trigger OnPreDataItem();
                            begin
                                if not DisplayAssemblyInfo then
                                    CurrReport.BREAK;
                                if not AsmInfoExistsForLine then
                                    CurrReport.BREAK;

                                AsmLine.SETRANGE("Document Type", AsmHeader."Document Type");
                                AsmLine.SETRANGE("Document No.", AsmHeader."No.");
                                SETRANGE(Number, 1, AsmLine.COUNT);
                            end;
                        }
                        dataitem("Line Attributes"; "Attribute Value")
                        {
                            DataItemLink = "Source Subtype" = FIELD("Document Type"), "Source No." = FIELD("Document No."), "Source Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Source Table ID", "Source Subtype", "Source No.", "Variant Code", "Doc. No. Occurrence", "Version No.", "Source Line No.", "Attribute Code", "Assignment Occurrence") WHERE("Source Table ID" = CONST(37), "Print Attribute" = CONST(true));
                            column(AttributeCode_LineAttributes; "Attribute Code")
                            {
                            }
                            column(Description_LineAttributes; Description)
                            {
                            }
                            column(Value_LineAttributes; Value)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        var
                            "---NAI---": Integer;
                            ItemRec: Record Item;
                        begin
                            if Number = 1 then
                                SalesLine.FIND('-')
                            else
                                SalesLine.NEXT;
                            "Sales Line" := SalesLine;

                            if DisplayAssemblyInfo then
                                AsmInfoExistsForLine := SalesLine.AsmToOrderExists(AsmHeader);

                            if not "Sales Header"."Prices Including VAT" and
                               (SalesLine."VAT Calculation Type" = SalesLine."VAT Calculation Type"::"Full VAT")
                            then
                                SalesLine."Line Amount" := 0;

                            if (SalesLine.Type = SalesLine.Type::"G/L Account") and (not ShowInternalInfo) then
                                "Sales Line"."No." := '';

                            NNCSalesLineLineAmt += SalesLine."Line Amount";
                            NNCSalesLineInvDiscAmt += SalesLine."Inv. Discount Amount";

                            NNCTotalLCY := NNCSalesLineLineAmt - NNCSalesLineInvDiscAmt;

                            NNCTotalExclVAT := NNCTotalLCY;
                            NNCVATAmt := VATAmount;
                            NNCTotalInclVAT := NNCTotalLCY - NNCVATAmt;

                            NNCPmtDiscOnVAT := -VATDiscountAmount;

                            NNCTotalInclVAT2 := TotalAmountInclVAT;

                            NNCVATAmt2 := VATAmount;
                            NNCTotalExclVAT2 := VATBaseAmount;

                            PpmtAmount := NNCTotalInclVAT2;

                            // >>> 2111-34284 24-11-2015 SMS
                            ItemTariffNo := '';
                            // >>> 2111-62114 12-12-2017 JH
                            ItemGTIN := '';
                            // <<< 2111-62114 12-12-2017 JH

                            // <<< 2111-62114 12-12-2017 JH
                            if "Sales Line".Type = "Sales Line".Type::Item then begin
                                // >>> 2111-37329 28-07-2016 MK
                                //ItemRec.GET("Sales Line"."No.");
                                //ItemTariffNo := ItemRec."Tariff No.";
                                // >>> 2111-62114 12-12-2017 JH
                                //IF ItemRec.GET("Sales Line"."No.") THEN
                                if ItemRec.GET("Sales Line"."No.") then begin
                                    // <<< 2111-62114 12-12-2017 JH
                                    ItemTariffNo := ItemRec."Tariff No.";

                                    // >>> 2111-62114 12-12-2017 JH
                                    if NAISetup."Print GTIN on SO Confirm." then begin
                                        ItemGTIN := ItemRec.GTIN;
                                    end;
                                end;
                                // <<< 2111-62114 12-12-2017 JH
                                // <<< 2111-37329 28-07-2016 MK
                            end;
                            // <<< 2111-34284 24-11-2015 SMS

                            // >>> 2111-47914 21-09-2016 MMA
                            CustomerItemNo := '';
                            // <<< 2111-47914 21-09-2016 MMA
                            // >>> 2111-34604 27-11-2015 SMS
                            if "Sales Line".Type = "Sales Line".Type::Item then begin
                                GetItemCrossReference;
                            end;
                            // <<< 2111-34604 27-11-2015 SMS

                            // >>> 2111-34887 23-12-2015 FS
                            Quantity_txt := '';
                            UnitPrice_txt := '';
                            LineAmount_txt := '';
                            Discount_txt := '';
                            if "Sales Line".Type <> "Sales Line".Type::" " then begin
                                Quantity_txt := FORMAT("Sales Line".Quantity, 0, 0);
                                UnitPrice_txt := FORMAT("Sales Line"."Unit Price", 0, 0);
                                LineAmount_txt := FORMAT("Sales Line"."Line Amount", 0, 0);
                                Discount_txt := FORMAT("Sales Line"."Line Discount %", 0, 0);
                            end else begin
                                Quantity_txt := '';
                                UnitPrice_txt := '';
                                LineAmount_txt := '';
                                Discount_txt := '';
                            end;
                            // <<< 2111-34887 23-12-2015 FS
                        end;

                        trigger OnPostDataItem();
                        begin
                            SalesLine.DELETEALL;
                        end;

                        trigger OnPreDataItem();
                        begin
                            MoreLines := SalesLine.FIND('+');
                            while MoreLines and (SalesLine.Description = '') and (SalesLine."Description 2" = '') and
                                  (SalesLine."No." = '') and (SalesLine.Quantity = 0) and
                                  (SalesLine.Amount = 0)
                            do
                                MoreLines := SalesLine.NEXT(-1) <> 0;
                            if not MoreLines then
                                CurrReport.BREAK;
                            SalesLine.SETRANGE("Line No.", 0, SalesLine."Line No.");
                            SETRANGE(Number, 1, SalesLine.COUNT);
                            CurrReport.CREATETOTALS(SalesLine."Line Amount", SalesLine."Inv. Discount Amount");
                        end;
                    }
                    dataitem(VATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmountLineVATBase; VATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmt; VATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineLineAmt; VATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; VATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscAmt; VATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercentage; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; VATAmountLine."VAT Identifier")
                        {
                        }
                        column(InvDiscBaseAmtCaption; InvDiscBaseAmtCaptionLbl)
                        {
                        }
                        column(VATIdentifierCaption; VATIdentifierCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            if VATAmount = 0 then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(
                              VATAmountLine."Line Amount", VATAmountLine."Inv. Disc. Base Amount",
                              VATAmountLine."Invoice Discount Amount", VATAmountLine."VAT Base", VATAmountLine."VAT Amount");
                        end;
                    }
                    dataitem(VATCounterLCY; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercentage2; VATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier2; VATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            VATAmountLine.GetLine(Number);
                            VALVATBaseLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  "Sales Header"."Posting Date", "Sales Header"."Currency Code", VATAmountLine."VAT Base", "Sales Header"."Currency Factor"));
                            VALVATAmountLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  "Sales Header"."Posting Date", "Sales Header"."Currency Code", VATAmountLine."VAT Amount", "Sales Header"."Currency Factor"));
                        end;

                        trigger OnPreDataItem();
                        begin
                            if (not GLSetup."Print VAT specification in LCY") or
                               ("Sales Header"."Currency Code" = '') or
                               (VATAmountLine.GetTotalVATAmount = 0)
                            then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, VATAmountLine.COUNT);
                            CurrReport.CREATETOTALS(VALVATBaseLCY, VALVATAmountLCY);
                            if GLSetup."LCY Code" = '' then
                                VALSpecLCYHeader := Text007 + Text008
                            else
                                VALSpecLCYHeader := Text007 + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Header"."Posting Date", "Sales Header"."Currency Code", 1);
                            VALExchRate := STRSUBSTNO(Text009, CurrExchRate."Relational Exch. Rate Amount", CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(Total2; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesHeader; "Sales Header"."Sell-to Customer No.")
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(ShiptoAddressCaption; ShiptoAddressCaptionLbl)
                        {
                        }
                        column(SelltoCustNo_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            if not ShowShippingAddr then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(PrepmtPlanLineLoop; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(PrepmtPlanCaption; PrepmtPlanLbl)
                        {
                        }
                        column(PrepmtPlanLine_Level; PrepmtPlanLine.Level)
                        {
                        }
                        column(PrepmtPlanLine_Description; PrepmtPlanLine.Description)
                        {
                        }
                        column(PrepmtPlanLine_LevelAmount; PrepmtPlanLine."Level Amount")
                        {
                            AutoFormatType = 2;
                        }
                        column(PrepmtPlanLine_LevelAmountPercentage; PrepmtPlanLine."Level Amount %")
                        {
                            AutoFormatType = 2;
                        }
                        column(PrepmtPlanLine_PrepmtPaymentTermsCode; PrepmtPlanLine."Prepmt. Payment Terms Code")
                        {
                        }
                        column(PrepmtPlanLine_PrepaymentDueDate; FORMAT(PrepmtPlanLine."Prepayment Due Date"))
                        {
                        }
                        column(PrepmtPlanLine_DocumentNo; PrepmtPlanLine."Document No.")
                        {
                        }
                        column(PrepmtPlanLine_LevelCaption; PrepmtPlanLine.FIELDCAPTION(Level))
                        {
                        }
                        column(PrepmtPlanLine_DescriptionCaption; PrepmtPlanLine.FIELDCAPTION(Description))
                        {
                        }
                        column(PrepmtPlanLine_LevelAmountCaption; PrepmtPlanLine.FIELDCAPTION("Level Amount"))
                        {
                        }
                        column(PrepmtPlanLine_LevelAmountPercentageCaption; PrepmtPlanLine.FIELDCAPTION("Level Amount %"))
                        {
                        }
                        column(PrepmtPlanLine_PrepmtPaymentTermsCodeCaption; PrepmtPlanLine.FIELDCAPTION("Prepmt. Payment Terms Code"))
                        {
                        }
                        column(PrepmtPlanLine_PrepaymentDueDateCaption; PrepmtPlanLine.FIELDCAPTION("Prepayment Due Date"))
                        {
                        }
                        column(PrepmtPlanLineLoop_Number; Number)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            // >>> 2111-49001 14-02-2017 RM
                            if PrepmtPlanLineLoop.Number = 1 then
                                PrepmtPlanLine.FINDSET
                            else
                                PrepmtPlanLine.NEXT;
                            // <<< 2111-49001 14-02-2017 RM
                        end;

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 14-02-2017 RM
                            if PrepmtPlanLine.COUNT = 0 then
                                CurrReport.BREAK;
                            SETRANGE(Number, 1, PrepmtPlanLine.COUNT);
                            // <<< 2111-49001 14-02-2017 RM
                        end;
                    }
                    dataitem(PrepmtLoop; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                        column(PrepmtLineAmount; PrepmtLineAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtInvBufDesc; PrepmtInvBuf.Description)
                        {
                        }
                        column(PrepmtInvBufGLAccNo; PrepmtInvBuf."G/L Account No.")
                        {
                        }
                        column(TotalExclVATText2; TotalExclVATText)
                        {
                        }
                        column(PrepmtVATAmtLineVATAmtTxt; PrepmtVATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalInclVATText2; TotalInclVATText)
                        {
                        }
                        column(PrepmtInvAmount; PrepmtInvBuf.Amount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmount; PrepmtVATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtInvAmtInclVATAmt; PrepmtInvBuf.Amount + PrepmtVATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmtText2; VATAmountLine.VATAmountText)
                        {
                        }
                        column(PrepmtTotalAmountInclVAT; PrepmtTotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATBaseAmount; PrepmtVATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtLoopNumber; Number)
                        {
                        }
                        column(DescriptionCaption; DescriptionCaptionLbl)
                        {
                        }
                        column(GLAccountNoCaption; GLAccountNoCaptionLbl)
                        {
                        }
                        column(PrepaymentSpecCaption; PrepaymentSpecCaptionLbl)
                        {
                        }
                        dataitem(PrepmtDimLoop; "Integer")
                        {
                            DataItemTableView = SORTING(Number) WHERE(Number = FILTER(1 ..));
                            column(DimText3; DimText)
                            {
                            }

                            trigger OnAfterGetRecord();
                            begin
                                if Number = 1 then begin
                                    if not TempPrepmtDimSetEntry.FIND('-') then
                                        CurrReport.BREAK;
                                end else
                                    if not Continue then
                                        CurrReport.BREAK;

                                CLEAR(DimText);
                                Continue := false;
                                repeat
                                    OldDimText := DimText;
                                    if DimText = '' then
                                        DimText :=
                                          STRSUBSTNO('%1 %2', TempPrepmtDimSetEntry."Dimension Code", TempPrepmtDimSetEntry."Dimension Value Code")
                                    else
                                        DimText :=
                                          STRSUBSTNO(
                                            '%1, %2 %3', DimText,
                                            TempPrepmtDimSetEntry."Dimension Code", TempPrepmtDimSetEntry."Dimension Value Code");
                                    if STRLEN(DimText) > MAXSTRLEN(OldDimText) then begin
                                        DimText := OldDimText;
                                        Continue := true;
                                        exit;
                                    end;
                                until TempPrepmtDimSetEntry.NEXT = 0;
                            end;
                        }

                        trigger OnAfterGetRecord();
                        begin
                            if Number = 1 then begin
                                if not PrepmtInvBuf.FIND('-') then
                                    CurrReport.BREAK;
                            end else
                                if PrepmtInvBuf.NEXT = 0 then
                                    CurrReport.BREAK;

                            if ShowInternalInfo then
                                DimMgt.GetDimensionSet(TempPrepmtDimSetEntry, PrepmtInvBuf."Dimension Set ID");

                            if "Sales Header"."Prices Including VAT" then
                                PrepmtLineAmount := PrepmtInvBuf."Amount Incl. VAT"
                            else
                                PrepmtLineAmount := PrepmtInvBuf.Amount;
                        end;

                        trigger OnPreDataItem();
                        begin
                            CurrReport.CREATETOTALS(
                              PrepmtInvBuf.Amount, PrepmtInvBuf."Amount Incl. VAT",
                              PrepmtVATAmountLine."Line Amount", PrepmtVATAmountLine."VAT Base",
                              PrepmtVATAmountLine."VAT Amount",
                              PrepmtLineAmount);
                        end;
                    }
                    dataitem(PrepmtVATCounter; "Integer")
                    {
                        DataItemTableView = SORTING(Number);
                        column(PrepmtVATAmtLineVATAmt; PrepmtVATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineVATBase; PrepmtVATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineLineAmt; PrepmtVATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineVATPerc; PrepmtVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(PrepmtVATAmtLineVATIdent; PrepmtVATAmountLine."VAT Identifier")
                        {
                        }
                        column(PrepmtVATCounterNumber; Number)
                        {
                        }
                        column(PrepaymentVATAmtSpecCap; PrepaymentVATAmtSpecCapLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            PrepmtVATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem();
                        begin
                            SETRANGE(Number, 1, PrepmtVATAmountLine.COUNT);
                        end;
                    }
                    dataitem(PrepmtTotal; "Integer")
                    {
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));

                        trigger OnPreDataItem();
                        begin
                            if not PrepmtInvBuf.FIND('-') then
                                CurrReport.BREAK;
                        end;
                    }
                    dataitem(NAIExtPostText; "NAI Extended Text - Sales")
                    {
                        DataItemTableView = SORTING("Document Type", "Document No.", "Assigned for", "Text Position", "Line No.");
                        column(PostText; Text)
                        {
                        }
                        column(PostText_LineNo; "Line No.")
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            // >>> 2111-49001 14-02-2017 RM
                            NAIExtPostText.RESET;
                            NAIExtPostText.SETRANGE("Document Type", "Sales Header"."Document Type");
                            NAIExtPostText.SETRANGE("Document No.", "Sales Header"."No.");
                            NAIExtPostText.SETRANGE("Assigned for", NAIExtPostText."Assigned for"::Order);
                            NAIExtPostText.SETRANGE("Text Position", NAIExtPostText."Text Position"::Posttext);
                            // <<< 2111-49001 14-02-2017 RM
                        end;
                    }
                    dataitem(Totals; "Integer")
                    {
                        DataItemLinkReference = CopyLoop;
                        DataItemTableView = SORTING(Number) WHERE(Number = CONST(1));
                        column(TotalNetAmount; NNCTotalExclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountIncludingVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalVATAmount; NNCVATAmt)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalVATAmountText; VATAmountLine.VATAmountText)
                        {
                        }
                        column(TotalExcludingVATText; TotalExclVATText)
                        {
                        }
                        column(TotalIncludingVATText; TotalInclVATText)
                        {
                        }
                        column(OrderTotalText; TotalText)
                        {
                        }
                    }
                }

                trigger OnAfterGetRecord();
                var
                    PrepmtSalesLine: Record "Sales Line" temporary;
                    SalesPost: Codeunit "Sales-Post";
                    TempSalesLine: Record "Sales Line" temporary;
                begin
                    // >>> 2111-49001 14-02-2017 RM
                    PrepmtPlanLine.RESET;
                    PrepmtPlanLine.SETRANGE("Document Type", "Sales Header"."Document Type");
                    PrepmtPlanLine.SETRANGE("Document No.", "Sales Header"."No.");
                    // <<< 2111-49001 14-02-2017 RM
                    CLEAR(SalesLine);
                    CLEAR(SalesPost);
                    VATAmountLine.DELETEALL;
                    SalesLine.DELETEALL;
                    SalesPost.GetSalesLines("Sales Header", SalesLine, 0);
                    SalesLine.CalcVATAmountLines(0, "Sales Header", SalesLine, VATAmountLine);
                    SalesLine.UpdateVATOnLines(0, "Sales Header", SalesLine, VATAmountLine);
                    VATAmount := VATAmountLine.GetTotalVATAmount;
                    VATBaseAmount := VATAmountLine.GetTotalVATBase;
                    VATDiscountAmount :=
                      VATAmountLine.GetTotalVATDiscount("Sales Header"."Currency Code", "Sales Header"."Prices Including VAT");
                    TotalAmountInclVAT := VATAmountLine.GetTotalAmountInclVAT;

                    PrepmtInvBuf.DELETEALL;
                    SalesPostPrepmt.GetSalesLines("Sales Header", 0, PrepmtSalesLine);

                    if not PrepmtSalesLine.ISEMPTY then begin
                        SalesPostPrepmt.GetSalesLinesToDeduct("Sales Header", TempSalesLine);
                        if not TempSalesLine.ISEMPTY then
                            SalesPostPrepmt.CalcVATAmountLines("Sales Header", TempSalesLine, PrepmtVATAmountLineDeduct, 1);
                    end;
                    SalesPostPrepmt.CalcVATAmountLines("Sales Header", PrepmtSalesLine, PrepmtVATAmountLine, 0);
                    // >>> 2111-35951 06-04-2016 MMA
                    PrepmtVATAmountLine.DeductVATAmountLine(PrepmtVATAmountLineDeduct);
                    // IF PrepmtVATAmountLine.FINDSET THEN
                    //  REPEAT
                    //    PrepmtVATAmountLineDeduct := PrepmtVATAmountLine;
                    //    IF PrepmtVATAmountLineDeduct.FIND THEN BEGIN
                    //      PrepmtVATAmountLine."VAT Base" := PrepmtVATAmountLine."VAT Base" - PrepmtVATAmountLineDeduct."VAT Base";
                    //      PrepmtVATAmountLine."VAT Amount" := PrepmtVATAmountLine."VAT Amount" - PrepmtVATAmountLineDeduct."VAT Amount";
                    //      PrepmtVATAmountLine."Amount Including VAT" := PrepmtVATAmountLine."Amount Including VAT" -
                    //        PrepmtVATAmountLineDeduct."Amount Including VAT";
                    //      PrepmtVATAmountLine."Line Amount" := PrepmtVATAmountLine."Line Amount" - PrepmtVATAmountLineDeduct."Line Amount";
                    //      PrepmtVATAmountLine."Inv. Disc. Base Amount" := PrepmtVATAmountLine."Inv. Disc. Base Amount" -
                    //        PrepmtVATAmountLineDeduct."Inv. Disc. Base Amount";
                    //      PrepmtVATAmountLine."Invoice Discount Amount" := PrepmtVATAmountLine."Invoice Discount Amount" -
                    //        PrepmtVATAmountLineDeduct."Invoice Discount Amount";
                    //      PrepmtVATAmountLine."Calculated VAT Amount" := PrepmtVATAmountLine."Calculated VAT Amount" -
                    //        PrepmtVATAmountLineDeduct."Calculated VAT Amount";
                    //      PrepmtVATAmountLine.MODIFY;
                    //    END;
                    //  UNTIL PrepmtVATAmountLine.NEXT = 0;
                    // <<< 2111-35951 06-04-2016 MMA

                    SalesPostPrepmt.UpdateVATOnLines("Sales Header", PrepmtSalesLine, PrepmtVATAmountLine, 0);
                    SalesPostPrepmt.BuildInvLineBuffer2("Sales Header", PrepmtSalesLine, 0, PrepmtInvBuf);
                    // >>> 2111-35951 06-04-2016 MMA
                    CurrReport.CREATETOTALS(
                      PrepmtInvBuf.Amount, PrepmtInvBuf."Amount Incl. VAT",
                      PrepmtVATAmountLine."Line Amount", PrepmtVATAmountLine."VAT Base",
                      PrepmtVATAmountLine."VAT Amount",
                      PrepmtLineAmount);
                    // <<< 2111-35951 06-04-2016 MMA
                    PrepmtVATAmount := PrepmtVATAmountLine.GetTotalVATAmount;
                    PrepmtVATBaseAmount := PrepmtVATAmountLine.GetTotalVATBase;
                    PrepmtTotalAmountInclVAT := PrepmtVATAmountLine.GetTotalAmountInclVAT;

                    // >>> 2111-35951 06-04-2016 MMA
                    PrePAmountDec := PrepmtInvBuf.Amount + PrepmtVATAmount;
                    // <<< 2111-35951 06-04-2016 MMA

                    if Number > 1 then begin
                        CopyText := Text003;
                        OutputNo += 1;
                    end;
                    CurrReport.PAGENO := 1;

                    NNCTotalLCY := 0;
                    NNCTotalExclVAT := 0;
                    NNCVATAmt := 0;
                    NNCTotalInclVAT := 0;
                    NNCPmtDiscOnVAT := 0;
                    NNCTotalInclVAT2 := 0;
                    NNCVATAmt2 := 0;
                    NNCTotalExclVAT2 := 0;
                    NNCSalesLineLineAmt := 0;
                    NNCSalesLineInvDiscAmt := 0;
                end;

                trigger OnPostDataItem();
                begin
                    if Print then
                        SalesCountPrinted.RUN("Sales Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                "---NAI---": Integer;
                GeneralReportMgmt: Codeunit "General Report Management";
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");
                // >>> 2111-49001 14-02-2017 RM
                CompanyInfo.GET;
                // <<< 2111-49001 14-02-2017 RM

                if RespCenter.GET("Responsibility Center") then begin
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    // >>> 2111-49001 14-02-2017 RM
                    CompanyInfo."E-Mail" := RespCenter."E-Mail";
                    CompanyInfo."Home Page" := COPYSTR(RespCenter."Home Page", 1, STRLEN(CompanyInfo."Home Page"));
                    CompanyInfo.Name := RespCenter.Name;
                    CompanyInfo.Address := RespCenter.Address;
                    CompanyInfo."Post Code" := RespCenter."Post Code";
                    CompanyInfo.City := RespCenter.City;
                    CompanyAddressLine := RespCenter.Name + ' · ' + RespCenter.Address + ' · ' + RespCenter."Post Code" + ' ' + RespCenter.City;
                    // <<< 2111-49001 14-02-2017 RM
                end else begin
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    // >>> 2111-49001 14-02-2017 RM
                    CompanyAddressLine := CompanyInfo.Name + ' · ' + CompanyInfo.Address + ' · ' + CompanyInfo."Post Code" + ' ' + CompanyInfo.City;
                    // <<< 2111-49001 14-02-2017 RM
                end;

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                // >>> 2111-49001 14-02-2017 RM
                if ShowOfficeSalesperson then begin
                    if "Office Sales Person" <> '' then begin
                        SalesPurchPerson.GET("Office Sales Person");
                        SalesPersonText := Text000;
                    end else begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end;
                end else begin
                    // <<< 2111-49001 14-02-2017 RM
                    if "Salesperson Code" = '' then begin
                        SalesPurchPerson.INIT;
                        SalesPersonText := '';
                    end else begin
                        SalesPurchPerson.GET("Salesperson Code");
                        SalesPersonText := Text000;
                    end;
                    // >>> 2111-49001 14-02-2017 RM
                end;
                // <<< 2111-49001 14-02-2017 RM

                // >>> 2111-62379 20-06-2018 BD
                if "Office Sales Person" <> '' then begin
                    OfficeSalesperson.GET("Office Sales Person");
                end else begin
                    OfficeSalesperson.INIT;
                end;
                // <<< 2111-62379 20-06-2018 BD

                if "Your Reference" = '' then
                    ReferenceText := ''
                else
                    ReferenceText := FIELDCAPTION("Your Reference");
                if "VAT Registration No." = '' then
                    VATNoText := ''
                else
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                if "Currency Code" = '' then begin
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006, GLSetup."LCY Code");
                end else begin
                    TotalText := STRSUBSTNO(Text001, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006, "Currency Code");
                end;
                FormatAddr.SalesHeaderBillTo(CustAddr, "Sales Header");

                if "Payment Terms Code" = '' then
                    PaymentTerms.INIT
                else begin
                    PaymentTerms.GET("Payment Terms Code");
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                end;
                if "Prepmt. Payment Terms Code" = '' then
                    PrepmtPaymentTerms.INIT
                else begin
                    PrepmtPaymentTerms.GET("Prepmt. Payment Terms Code");
                    PrepmtPaymentTerms.TranslateDescription(PrepmtPaymentTerms, "Language Code");
                end;
                if "Prepmt. Payment Terms Code" = '' then
                    PrepmtPaymentTerms.INIT
                else begin
                    PrepmtPaymentTerms.GET("Prepmt. Payment Terms Code");
                    PrepmtPaymentTerms.TranslateDescription(PrepmtPaymentTerms, "Language Code");
                end;
                if "Shipment Method Code" = '' then
                    ShipmentMethod.INIT
                else begin
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                end;

                FormatAddr.SalesHeaderShipTo(ShipToAddr, CustAddr, "Sales Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                for i := 1 to ARRAYLEN(ShipToAddr) do
                    if ShipToAddr[i] <> CustAddr[i] then
                        ShowShippingAddr := true;

                if Print then begin
                    if ArchiveDocument then
                        ArchiveManagement.StoreSalesDocument("Sales Header", LogInteraction);

                    if LogInteraction then begin
                        CALCFIELDS("No. of Archived Versions");
                        if "Bill-to Contact No." <> '' then
                            SegManagement.LogDocument(
                              3, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Contact, "Bill-to Contact No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.")
                        else
                            SegManagement.LogDocument(
                              3, "No.", "Doc. No. Occurrence",
                              "No. of Archived Versions", DATABASE::Customer, "Bill-to Customer No.",
                              "Salesperson Code", "Campaign No.", "Posting Description", "Opportunity No.");
                    end;
                end;

                // >>> 2111-36685 04-08-2016 MK
                if "Sales Header"."Promised Delivery Date" <> 0D then begin
                    ExpectedWeekNumber := DATE2DWY("Sales Header"."Promised Delivery Date", 2);
                    ExpectedWeekNumberText := STRSUBSTNO(Text010, ExpectedWeekNumber);
                end else begin
                    if "Sales Header"."Shipment Date" <> 0D then begin
                        ExpectedWeekNumber := DATE2DWY("Sales Header"."Shipment Date", 2);
                        ExpectedWeekNumberText := STRSUBSTNO(Text010, ExpectedWeekNumber);
                    end else begin
                        ExpectedWeekNumber := 0;
                        ExpectedWeekNumberText := ExpectedWeeknvLbl;
                    end;
                end;
                // <<< 2111-36685 04-08-2016 MK

                // >>> 2111-23178 31-07-2014 UQ
                ShowDiscount := IsDiscount("Sales Header");
                // <<< 2111-23178 31-07-2014 UQ

                // >>> 2111-34381 28-12-2015 DB
                ShippingInformationMgt.GetExtendedShippingInfo("Sales Header", SumNetWeight, SumGrossWeight, ShowNetWeight, ShowGrossWeight);
                // <<< 2111-34381 28-12-2015 DB

                // >>> 2111-35951 06-04-2016 MMA
                PrePAmountDec := 0;
                // <<< 2111-35951 06-04-2016 MMA

                // >>> 2111-36940 16-11-2016 MMA
                // >>> 2111-62114 20-12-2017 JH
                //IF ShowWeekInsteadOfDate THEN BEGIN
                if NAISetup."Week instead of Date on OConf." then begin
                    // <<< 2111-62114 20-12-2017 JH
                    ShipmentDateOrWeek := STRSUBSTNO(Text010, DATE2DWY("Sales Header"."Shipment Date", 2));
                end else begin
                    ShipmentDateOrWeek := FORMAT("Sales Header"."Shipment Date");
                end;
                // <<< 2111-36940 16-11-2016 MMA

                // >>> 2111-62084 09-11-2017 AM
                //AssociationName := '';
                //CustAssociationSortCode := '';
                //if Cust.GET("Bill-to Customer No.") then begin
                //    if Association.GET(Association.Type::Customer, Cust."OPP Association No.") then begin
                //        AssociationName := Association.Name;
                //        CustAssociationSortCode := Cust."OPP Association Sort Code";
                //    end;
                //end;
                // <<< 2111-62084 09-11-2017 AM

                // >>> 2111-62234 13-03-2018 RM
                ShowSellToInfo := not GeneralReportMgmt.SalesHeaderSellToEqualsBillTo("Sales Header");
                // <<< 2111-62234 13-03-2018 RM
            end;

            trigger OnPreDataItem();
            var
                "---NAI---": Integer;
                PdfReportMgmt: Codeunit "NAI - Pdf Report Mgmt.";
            begin
                Print := Print or not CurrReport.PREVIEW;

                // >>> 2111-49001 14-02-2017 RM
                AsmInfoExistsForLine := false;
                // <<< 2111-49001 14-02-2017 RM

                // >>> 2111-30349 27-03-2015 RW
                if FINDFIRST then begin
                    PdfReportMgmt.CheckDocumentArchiving("Print as PDF", 1, "Document Type", ArchiveDocument);
                    "Print as PDF" := false;
                    MODIFY;
                end;
                // <<< 2111-30349 27-03-2015 RW
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Optionen)
                {
                    CaptionML = DEU = 'Optionen',
                                ENU = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        CaptionML = DEU = 'Anzahl Kopien',
                                    ENU = 'No. of Copies';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        CaptionML = DEU = 'Interne Informationen anzeigen',
                                    ENU = 'Show Internal Information';
                    }
                    field(ArchiveDocument; ArchiveDocument)
                    {
                        CaptionML = DEU = 'Beleg archivieren',
                                    ENU = 'Archive Document';

                        trigger OnValidate();
                        begin
                            if not ArchiveDocument then
                                LogInteraction := false;
                        end;
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        CaptionML = DEU = 'Aktivität protokollieren',
                                    ENU = 'Log Interaction';
                        Enabled = LogInteractionEnable;

                        trigger OnValidate();
                        begin
                            if LogInteraction then
                                ArchiveDocument := ArchiveDocumentEnable;
                        end;
                    }
                    field(ShowAssemblyComponents; DisplayAssemblyInfo)
                    {
                        CaptionML = DEU = 'Montagekomponenten anzeigen',
                                    ENU = 'Show Assembly Components';
                    }
                    field(ShowOfficeSalesperson; ShowOfficeSalesperson)
                    {
                        CaptionML = DEU = 'Verkäufercode Innendienst anzeigen',
                                    ENU = 'Show Office Salesperson Code';
                    }
                    field(PrintPrepPlan; PrintPrepPlan)
                    {
                        CaptionML = DEU = 'Vorauszahlungsplan drucken',
                                    ENU = 'Print Prepayment Plan';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := true;
            ArchiveDocument := SalesSetup."Archive Quotes and Orders";
        end;

        trigger OnOpenPage();
        begin
            LogInteraction := SegManagement.FindInteractTmplCode(3) <> '';

            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        GLSetup.GET;
        CompanyInfo.GET;
        SalesSetup.GET;
        // >>> 2111-62114 12-12-2017 JH
        NAISetup.GET;
        // <<< 2111-62114 12-12-2017 JH

        // >>> 2111-62114 20-12-2017 JH
        // // >>> 2111-36940 16-11-2016 MMA
        // IF NAISetup.GET THEN BEGIN
        //  ShowWeekInsteadOfDate := NAISetup."Week instead of Date on OConf.";
        // END;
        // // <<< 2111-36940 16-11-2016 MMA
        // <<< 2111-62114 20-12-2017 JH
    end;

    var
        Text000: TextConst DEU = 'Verkäufer', ENU = 'Salesperson', ITA = 'Venditore';
        Text001: TextConst DEU = 'Total %1', ENU = 'Total %1';
        Text002: TextConst DEU = 'Total %1 inkl. MwSt.', ENU = 'Total %1 Incl. VAT';
        Text003: TextConst DEU = 'KOPIE', ENU = 'COPY';
        Text004: TextConst DEU = 'Retourenschein %1 %2', ENU = 'Return Sipment %1 %2', ITA = 'Documento di reso';
        Text005: TextConst DEU = 'Seite %1', ENU = 'Page %1';
        Text006: TextConst DEU = 'Total %1 ohne MwSt.', ENU = 'Total %1 Excl. VAT';
        GLSetup: Record "General Ledger Setup";
        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        PrepmtPaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo1: Record "Company Information";
        CompanyInfo2: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        VATAmountLine: Record "VAT Amount Line" temporary;
        PrepmtVATAmountLine: Record "VAT Amount Line" temporary;
        PrepmtVATAmountLineDeduct: Record "VAT Amount Line" temporary;
        SalesLine: Record "Sales Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        TempPrepmtDimSetEntry: Record "Dimension Set Entry" temporary;
        PrepmtInvBuf: Record "Prepayment Inv. Line Buffer" temporary;
        RespCenter: Record "Responsibility Center";
        Language: Record Language;
        CurrExchRate: Record "Currency Exchange Rate";
        AsmHeader: Record "Assembly Header";
        AsmLine: Record "Assembly Line";
        SalesCountPrinted: Codeunit "Sales-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        ArchiveManagement: Codeunit ArchiveManagement;
        SalesPostPrepmt: Codeunit "Sales-Post Prepayments";
        DimMgt: Codeunit DimensionManagement;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        DimText: Text[120];
        OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        ArchiveDocument: Boolean;
        LogInteraction: Boolean;
        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        VALSpecLCYHeader: Text[80];
        Text007: TextConst DEU = 'MwSt.-Betrag Spezifikation in ', ENU = 'VAT Amount Specification in ';
        Text008: TextConst DEU = 'Landeswährung', ENU = 'Local Currency';
        Text009: TextConst DEU = 'Wechselkurs: %1/%2', ENU = 'Exchange rate: %1/%2';
        VALExchRate: Text[50];
        PrepmtVATAmount: Decimal;
        PrepmtVATBaseAmount: Decimal;
        PrepmtAmountInclVAT: Decimal;
        PrepmtTotalAmountInclVAT: Decimal;
        PrepmtLineAmount: Decimal;
        OutputNo: Integer;
        NNCTotalLCY: Decimal;
        NNCTotalExclVAT: Decimal;
        NNCVATAmt: Decimal;
        NNCTotalInclVAT: Decimal;
        NNCPmtDiscOnVAT: Decimal;
        NNCTotalInclVAT2: Decimal;
        NNCVATAmt2: Decimal;
        NNCTotalExclVAT2: Decimal;
        NNCSalesLineLineAmt: Decimal;
        NNCSalesLineInvDiscAmt: Decimal;
        Print: Boolean;
        [InDataSet]
        ArchiveDocumentEnable: Boolean;
        [InDataSet]
        LogInteractionEnable: Boolean;
        DisplayAssemblyInfo: Boolean;
        AsmInfoExistsForLine: Boolean;
        CompanyInfoPhoneNoCaptionLbl: TextConst DEU = 'Telefonnr.', ENU = 'Phone No.';
        VATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.';
        AccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        ShipmentDateCaptionLbl: TextConst DEU = 'Warenausg.-Datum', ENU = 'Shipment Date';
        OrderNoCaptionLbl: TextConst DEU = 'Auftragsnr.', ENU = 'Order No.', ITA = 'Reso ordine num.';
        HeaderDimensionsCaptionLbl: TextConst DEU = 'Kopfdimensionen', ENU = 'Header Dimensions';
        UnitPriceCaptionLbl: TextConst DEU = 'VK-Preis', ENU = 'Unit Price';
        Sales_Line___Line_Discount___CaptionLbl: TextConst DEU = 'Rab. %', ENU = 'Disc. %';
        AmountCaptionLbl: TextConst DEU = 'Betrag', ENU = 'Amount';
        InvDiscAmtCaptionLbl: TextConst DEU = 'Rechnungsrabattbetrag', ENU = 'Inv. Discount Amount';
        SubtotalCaptionLbl: TextConst DEU = 'Zwischensumme', ENU = 'Subtotal';
        PaymentDiscountVATCaptionLbl: TextConst DEU = 'Skonto auf MwSt.', ENU = 'Payment Discount on VAT';
        LineDimCaptionLbl: TextConst DEU = 'Zeilendimensionen', ENU = 'Line Dimensions';
        VATPercentageCaptionLbl: TextConst DEU = 'MwSt. %', ENU = 'VAT %';
        VATBaseCaptionLbl: TextConst DEU = 'MwSt.-Bemessungsgrundlage', ENU = 'VAT Base';
        VATAmtCaptionLbl: TextConst DEU = 'MwSt.-Betrag', ENU = 'VAT Amount';
        VATAmtSpecCaptionLbl: TextConst DEU = 'MwSt.-Betrag Spezifikation', ENU = 'VAT Amount Specification';
        InvDiscBaseAmtCaptionLbl: TextConst DEU = 'Rechnungsrab.-Bemessungsgr.', ENU = 'Invoice Discount Base Amount';
        LineAmtCaptionLbl: TextConst DEU = 'Zeilenbetrag', ENU = 'Line Amount';
        VATIdentifierCaptionLbl: TextConst DEU = 'MwSt.-Kennzeichen', ENU = 'VAT Identifier';
        TotalCaptionLbl: TextConst DEU = 'Gesamt', ENU = 'Total';
        PaymentTermsCaptionLbl: TextConst DEU = 'Zahlungsbedingungen', ENU = 'Payment Terms';
        ShipmentMethodCaptionLbl: TextConst DEU = 'Lieferbedingung', ENU = 'Shipment Method';
        ShiptoAddressCaptionLbl: TextConst DEU = 'Lieferadresse', ENU = 'Ship-to Address';
        DescriptionCaptionLbl: TextConst DEU = 'Beschreibung', ENU = 'Description';
        GLAccountNoCaptionLbl: TextConst DEU = 'Sachkontonr.', ENU = 'G/L Account No.';
        PrepaymentSpecCaptionLbl: TextConst DEU = 'Vorauszahlungsspezifikation', ENU = 'Prepayment Specification';
        PrepaymentVATAmtSpecCapLbl: TextConst DEU = 'MwSt-Betragsspezifikation Vorauszahlung', ENU = 'Prepayment VAT Amount Specification';
        PrepmtPmtTermsDescCaptionLbl: TextConst DEU = 'Zahlungsbedingungen Vorauszahlung', ENU = 'Prepmt. Payment Terms';
        NO2Lbl: TextConst DEU = 'Nr.', ENU = 'No.', ITA = 'Codice art.';
        QtyLbl: TextConst DEU = 'Menge', ENU = 'Quantity', ITA = 'Quantità ordine ';
        "---NAI---": Integer;
        NAISetup: Record "NAI - Setup";
        PrepmtPlanLine: Record "Prepayment Plan Line";
        Cust: Record Customer;
        //Association: Record "OPP Association";
        OfficeSalesperson: Record "Salesperson/Purchaser";
        ShippingInformationMgt: Codeunit "Shipping Information Mgt.";
        "----NAI----": TextConst DEU = '----NAI----', ENU = '----NAI----';
        Text010: TextConst DEU = '%1. KW', ENU = 'Week %1';
        Text5000000: TextConst DEU = 'der Auftragsbestätigung %1', ENU = 'of Order Confirmation %1';
        CompanyInfoHomePageCaptionLbl: TextConst DEU = 'Homepage', ENU = 'Home Page';
        CompanyInfoEmailCaptionLbl: TextConst DEU = 'E-Mail', ENU = 'E-Mail';
        DiscountPercentCaptionLbl: TextConst DEU = 'Rabatt %', ENU = 'Discount %';
        DocumentDateCaptionLbl: TextConst DEU = 'Datum', ENU = 'Date';
        AllowInvDiscCaptionLbl: TextConst DEU = 'Rech.-Rabatt zulassen', ENU = 'Allow Invoice Discount';
        ExpectedWeeknvLbl: TextConst DEU = 'n.v.', ENU = 'n/a';
        SalesHeaderBilltoCustomerNoCaptionLbl: TextConst DEU = 'Kundennr.', ENU = 'Customer No.', ITA = 'Vs. codice cliente';
        SalesPurchPersonPhoneNoCaptionLbl: TextConst DEU = 'Durchwahl', ENU = 'Direct Dialing';
        ExpectedWeekNumberCaptionLbl: TextConst DEU = 'Voraussichtl. Liefertermin', ENU = 'Expected Shipment Date';
        CompanyInfoVATRegNoCaptionLbl: TextConst DEU = 'USt-IdNr.', ENU = 'VAT Reg. No.';
        CompanyInfoBankAccountNoCaptionLbl: TextConst DEU = 'Kontonr.', ENU = 'Account No.';
        CompanyInfoBankBranchNoCaptionLbl: TextConst DEU = 'BLZ', ENU = 'Bank Branch No.';
        SalesLinePositionNoCaptionLbl: TextConst DEU = 'Pos.', ENU = 'Pos.';
        PrepmtPlanLbl: TextConst DEU = 'Vorauszahlungsplan', ENU = 'Prepayment Plan';
        PpmtAmountCaptionLbl: TextConst DEU = 'Betrag inkl. MwSt.', ENU = 'Amount incl. VAT';
        SalesHeader_PrepmtPaymentDiscountCaptionLbl: TextConst DEU = '% Skonto', ENU = '% Discount';
        SalesHeader_PrepmtPmtDiscountDateCaptionLbl: TextConst DEU = 'bis', ENU = 'until';
        UOM_SalesLineCaptionLbl: TextConst DEU = 'Einheit', ENU = 'Unit', FRA = 'Unité', NLD = 'Eenheid';
        CustomerItemNoCaptionLbl: TextConst DEU = 'Ihre Artikelnummer: ', ENU = 'Your item number: ';
        SumNetWeightLbl: TextConst DEU = 'Summe Netto-Gewicht', ENU = 'Sum Net Weight';
        SumGrossWeightLbl: TextConst DEU = 'Summe Brutto-Gewicht', ENU = 'Sum Gross Weight';
        MMC1: TextConst DEU = 'Moldino Tool Engineering Europe GmbH', ENU = 'Moldino Tool Engineering Europe GmbH', ITA = 'Moldino Tool Engineering Europe GmbH';
        MMC2: TextConst DEU = 'Via Pietro Nenni 18', ENU = 'Via Pietro Nenni 18', ITA = 'Via Pietro Nenni 18';
        MMC3: TextConst DEU = '25124 Brescia - Italy', ENU = '25124 Brescia - Italy', ITA = '25124 Brescia - Italy';
        TextITLINE01: TextConst DEU = 'Menge Rücksendung', ENU = 'Qty. Return Ship.', ITA = 'Quantità resa';
        TextITLINE02: TextConst DEU = 'Artikelzustand', ENU = 'Item Status', ITA = 'Cond. articolo';
        TextITLINE03: TextConst DEU = 'TAR', ENU = 'TAR', ITA = 'TAR';
        TextITLINE04: TextConst DEU = '', ENU = '', ITA = 'Azione';
        ExpectedWeekNumber: Integer;
        PpmtAmount: Decimal;
        PrintPrepPlan: Boolean;
        ExpectedWeekNumberText: Text[50];
        CompanyAddressLine: Text[160];
        ShowDiscount: Boolean;
        ItemTariffNo: Code[20];
        ItemSerialNo: Code[20];
        ItemLotNo: Code[20];
        ItemExpirationDate: Date;
        ItemQuantity: Decimal;
        HideInvoiceLine: Boolean;
        CustomerItemNo: Code[20];
        SumNetWeight: Decimal;
        SumGrossWeight: Decimal;
        ShowNetWeight: Boolean;
        ShowGrossWeight: Boolean;
        PrePAmountDec: Decimal;
        ShipmentDateOrWeek: Text;
        ShowOfficeSalesperson: Boolean;
        Quantity_txt: Text[15];
        UnitPrice_txt: Text[15];
        LineAmount_txt: Text[15];
        Discount_txt: Text[15];
        AssociationName: Text[50];
        CustAssociationSortCode: Code[20];
        ItemGTIN: Code[14];
        GTINLbl: TextConst DEU = 'EAN: ', ENU = 'GTIN: ';
        ShowSellToInfo: Boolean;

    procedure InitializeRequest(NoOfCopiesFrom: Integer; ShowInternalInfoFrom: Boolean; ArchiveDocumentFrom: Boolean; LogInteractionFrom: Boolean; PrintFrom: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NoOfCopiesFrom;
        ShowInternalInfo := ShowInternalInfoFrom;
        ArchiveDocument := ArchiveDocumentFrom;
        LogInteraction := LogInteractionFrom;
        Print := PrintFrom;
        DisplayAssemblyInfo := DisplayAsmInfo;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[10];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        if not UnitOfMeasure.GET(UOMCode) then
            exit(UOMCode);
        exit(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        exit(PADSTR('', 2, ' '));
    end;

    local procedure "-----NAI-----"();
    begin
    end;

    local procedure IsDiscount(SalesHeaderDisc: Record "Sales Header"): Boolean;
    var
        SalesLineDisc: Record "Sales Line";
    begin
        // >>> 2111-23178 31-07-2014 UQ
        SalesLineDisc.SETRANGE("Document Type", SalesHeaderDisc."Document Type");
        SalesLineDisc.SETRANGE("Document No.", SalesHeaderDisc."No.");
        SalesLineDisc.SETFILTER("Line Discount %", '<>0');
        exit(not SalesLineDisc.ISEMPTY);
        // <<< 2111-23178 31-07-2014 UQ
    end;

    local procedure GetItemCrossReference();
    var
        ItemCrossReference: Record "Item Cross Reference";
    begin
        // >>> 2111-34604 27-11-2015 SMS
        with "Sales Line" do begin
            ItemCrossReference.RESET;
            ItemCrossReference.SETRANGE("Item No.", "No.");
            ItemCrossReference.SETRANGE("Variant Code", "Variant Code");
            ItemCrossReference.SETRANGE("Unit of Measure", "Unit of Measure Code");
            ItemCrossReference.SETRANGE("Cross-Reference Type", "Cross-Reference Type"::Customer);
            ItemCrossReference.SETRANGE("Cross-Reference Type No.", "Sell-to Customer No.");
            ItemCrossReference.SETRANGE("Cross-Reference No.", "Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            ItemCrossReference.SETRANGE("Cross-Reference No.");
            if ItemCrossReference.FINDFIRST then begin
                CustomerItemNo := ItemCrossReference."Cross-Reference No.";
                exit;
            end;
            CustomerItemNo := "Cross-Reference No.";
        end;
        // <<< 2111-34604 27-11-2015 SMS
    end;
}

