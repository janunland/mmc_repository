report 50010 "MMC Appointment Order Creation"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    ProcessingOnly = true;


    dataset
    {
        dataitem(Customer; Customer)
        {
            column(ColumnName; "No.")
            {

            }
            trigger OnPreDataItem()
            begin
                Customer.SetRange("No.", Customerno);
            End;

            trigger OnAfterGetRecord()
            begin

                SalesHeader.INIT;
                SalesHeader."Document Type" := SalesHeader."Document Type"::Order;
                SalesHeader."No." := '';
                SalesHeader.INSERT(TRUE);

                SalesHeader.Validate("Sell-to Customer No.", Customerno);
                SalesHeader.Validate("Document Date", reqdeldate1);
                SalesHeader.Validate("Operation Occurred Date", reqdeldate1);
                SalesHeader.Validate("Posting Date", reqdeldate1);
                SalesHeader.Validate("Order Date Registration", reqdeldate1);

                SalesHeader.Modify(true);

                SalesLine.INIT;
                SalesLine."Document Type" := SalesHeader."Document Type";
                SalesLine."Document No." := SalesHeader."No.";
                SalesLine."Sell-to Customer No." := Customerno;

                SalesLine.Validate(Type, 2);
                SalesLine.Validate("No.", Itemno1);
                SalesLine.Quantity := qtyitem1;
                SalesLine."Requested Delivery Date" := reqdeldate1;
                SalesLine.insert(true);
                SalesLine.Modify(true);


                if reqdeldate1 = reqdeldate2 then Begin
                    SalesLine.INIT;
                    SalesLine."Document Type" := SalesHeader."Document Type";
                    SalesLine."Document No." := SalesHeader."No.";
                    SalesLine."Line No." := 20000;
                    SalesLine."Sell-to Customer No." := Customerno;

                    SalesLine.Validate(Type, 2);
                    SalesLine.Validate("No.", Itemno2);
                    SalesLine.Quantity := qtyitem2;
                    SalesLine."Requested Delivery Date" := reqdeldate2;
                    SalesLine.insert(true);
                    SalesLine.Modify(true);
                End else begin
                    SalesHeader.INIT;
                    SalesHeader."Document Type" := SalesHeader."Document Type"::Order;
                    SalesHeader."No." := '';
                    SalesHeader.INSERT(TRUE);

                    SalesHeader.Validate("Sell-to Customer No.", Customerno);
                    SalesHeader.Validate("Document Date", reqdeldate2);
                    SalesHeader.Validate("Operation Occurred Date", reqdeldate2);
                    SalesHeader.Validate("Posting Date", reqdeldate2);
                    SalesHeader.Validate("Order Date Registration", reqdeldate2);

                    SalesHeader.Modify(true);

                    SalesLine.INIT;
                    SalesLine."Document Type" := SalesHeader."Document Type";
                    SalesLine."Document No." := SalesHeader."No.";
                    SalesLine."Sell-to Customer No." := Customerno;

                    SalesLine.Validate(Type, 2);
                    SalesLine.Validate("No.", Itemno2);
                    SalesLine.Quantity := qtyitem2;
                    SalesLine."Requested Delivery Date" := reqdeldate2;
                    SalesLine.insert(true);
                    SalesLine.Modify(true);

                end;
            END;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(Content)
            {
                group("Customer Appointment Order")
                {


                    Group(Customer)
                    {

                        field(Name; Customerno)
                        {
                            ApplicationArea = All;
                            TableRelation = Customer."No.";
                            CaptionML = DEU = 'Kunde', ENU = 'Customer Number';

                        }

                    }

                    grid(Items)
                    {
                        GridLayout = Columns;
                        group(Articles)
                        {
                            group("First Item")
                            {
                                field(Item; Itemno1)
                                {
                                    ApplicationArea = All;
                                    TableRelation = item."No.";
                                    CaptionML = DEU = 'Artikel 1', ENU = 'Item 1';


                                }
                                field(Reqdeldate; reqdeldate1)
                                {
                                    ApplicationArea = All;
                                    CaptionML = DEU = 'Gewünschtes Lieferdatum Artikel 1', ENU = 'Requested Delivery Date Item 1';


                                }
                                field(qtyitem1; qtyitem1)
                                {
                                    ApplicationArea = All;
                                    CaptionML = DEU = 'Menge Artikel 1', ENU = 'Qty. Item 1';
                                    DecimalPlaces = 0;


                                }
                            }

                            group("Second Item")
                            {
                                field(Item2; Itemno2)
                                {
                                    ApplicationArea = All;
                                    TableRelation = item."No.";
                                    CaptionML = DEU = 'Artikel 2', ENU = 'Item 2';


                                }
                                field(Reqdeldate2; reqdeldate2)
                                {
                                    ApplicationArea = All;
                                    CaptionML = DEU = 'Gewünschtes Lieferdatum Artikel 2', ENU = 'Requested Delivery Date Item 2';


                                }
                                field(qtyitem2; qtyitem2)
                                {
                                    ApplicationArea = All;
                                    CaptionML = DEU = 'Menge Artikel 2', ENU = 'Qty. Item 2';
                                    DecimalPlaces = 0;
                                }
                            }
                        }
                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }






    var
        Customerno: Code[10];
        Itemno1: code[10];

        reqdeldate1: Date;
        qtyitem1: Decimal;
        Itemno2: code[10];

        reqdeldate2: Date;
        qtyitem2: Decimal;

        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
}