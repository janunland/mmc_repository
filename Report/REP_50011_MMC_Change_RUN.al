report 50011 MMC_Change_Run
{
    UsageCategory = Administration;
    ApplicationArea = All;
    ProcessingOnly = true;



    trigger OnPreReport()
    begin

        Begin
            locSalesHeader.reset;
            if locSalesHeader.Find('-') then
                repeat
                    if locSalesHeader."Document Type" = locSalesHeader."Document Type"::Order then
                        if locSalesHeader.ICTransfer = 'CompShp' then Begin
                            locSalesHeader.CalcFields("Completely Shipped");
                            if locSalesHeader."Completely Shipped" = false then
                                locSalesHeader.ICTransfer := 'PartShp';
                            locSalesHeader.Modify();
                        End;
                    if locSalesHeader.ICTransfer = 'PartShp' then Begin
                        locSalesHeader.CalcFields("Completely Shipped");
                        if locSalesHeader."Completely Shipped" = true then
                            locSalesHeader.ICTransfer := 'CompShp';
                        locSalesHeader.Modify();
                    End;


                until locSalesHeader.next = 0;
        End;

    end;



    var
        locSalesHeader: Record "Sales Header";
}