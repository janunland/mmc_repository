table 50000 "MMC Testtool Budget"
{

    fields
    {
        field(1; "Sales Person"; Code[10]) { TableRelation = "Salesperson/Purchaser".Code; }
        field(2; "Salesperson Name"; Text[50]) { }
        field(7; "Salesperson Nam."; Text[50]) { FieldClass = FlowField; CalcFormula = Lookup ("Salesperson/Purchaser".Name WHERE (Code = FIELD ("Sales Person"))); }
        field(3; "Valid from"; Date) { }
        field(4; "Valid to"; Date)
        {
            trigger OnValidate()
            begin
                IF ("Valid from" <> 0D) AND ("Valid to" < "Valid from") THEN
                    ERROR(C_KVS001);
            end;
        }
        field(5; "Budget (MW)"; Decimal)
        {
            trigger OnValidate()
            begin

                IF "Budget (MW)" < 0 THEN
                    ERROR(C_KVS002);

            end;


        }
        field(6; "Remaining budget (MW)"; Decimal) { Editable = false; }
    }

    keys
    {
        key(PK; "Sales Person", "Valid from", "Valid to")
        {
            Clustered = true;
        }

    }

    var

        C_KVS001: TextConst DEU = 'Das Enddatum ist kleiner als das Startdatum', ENU = 'The finish date is before the start date';
        C_KVS002: TextConst DEU = 'Das Startbudget kann nicht negativ sein', ENU = 'The budget can not be negative';
        C_KVS003: TextConst DEU = 'Wollen Sie das Restbudget zurücksetzen ?', ENU = 'Reset the Restbudget ?';

    procedure ResettRemainingBudget();
    var
        kvs001loc: TextConst DEU = 'Das neue Budget darf nicht kleiner als das Restbudget sein', ENU = 'The new Budget needs to be higher than the Restbudget';
    begin

        IF CONFIRM(C_KVS003) THEN BEGIN
            IF "Budget (MW)" > "Remaining budget (MW)" THEN BEGIN
                "Remaining budget (MW)" := "Budget (MW)";
                MODIFY;
            END ELSE BEGIN
                ERROR(kvs001loc);
            END;
        END;

    end;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}