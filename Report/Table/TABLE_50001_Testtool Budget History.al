table 50001 "MMC Testtool Budget History"
{

    fields
    {
        field(1; "Sales Person"; Code[10]) { }
        field(2; "Salesperson Name"; Text[50]) { }
        field(3; "Valid from"; Date) { }
        field(4; "Valid to"; Date) { }
        field(5; "Budget (MW)"; Decimal) { }
        field(6; "Remaining budget (MW)"; Decimal) { }
    }

    keys
    {
        key(PK; "Sales Person", "Valid from", "Valid to")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}
