table 50002 "MMC Testtool Budget Details"
{


    fields
    {

        field(1; "Position"; Integer) { }
        field(2; "Sales Person"; Code[10]) { }
        field(3; "Salesperson Name"; Text[50]) { }
        field(4; "Valid from"; Date) { }
        field(5; "Valid to"; Date) { }
        field(6; "Document Type"; Option)
        {
            OptionMembers = Quote,Order,Invoice,"Credit Memo","Blanket Order","Return Order";
            OptionCaption = 'Quote,Order,Invoice,"Credit Memo","Blanket Order","Return Order"';
        }
        field(7; "Document No"; Code[20]) { }
        field(8; "Amount"; Decimal) { }
        field(9; "Salesperson Nam."; Text[50]) { FieldClass = FlowField; CalcFormula = Lookup ("Salesperson/Purchaser".Name WHERE (Code = FIELD ("Sales Person"))); }
    }

    keys
    {
        key(PK; Position, "Sales Person", "Valid from", "Valid to")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}