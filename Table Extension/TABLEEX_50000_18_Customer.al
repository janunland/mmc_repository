tableextension 50000 MMC_Customer extends 18
{
    fields
    {
        field(50000; "Credit Check Factor"; Decimal)
        {
            CaptionML = DEU = 'Bonität Faktor', ENU = 'Credit Check Factor';

        }

        field(50010; "Credit Check Date"; Date)
        {
            CaptionML = ENU = 'Credit Check Date',
                       DEU = 'Bonitätscheck Datum';
        }

        field(50020; "Atradius-ID"; Code[20])
        {
            CaptionML = ENU = 'Atradius-ID',
                       DEU = 'Atradius-ID';
        }
        field(50030; "Combine Shipment Code"; Code[20])
        {
            CaptionML = ENU = 'Combine Shipment Code',
                       DEU = 'Sammelrechnungscode';
        }
        field(50040; CRMID; text[250])
        {
            CaptionML = ENU = 'CRMID',
                        DEU = 'CRMID';
        }
        field(50050; CRMURL; Text[250])
        {
            CaptionML = ENU = 'CRMURL',
                        DEU = 'CRMURL';
            ExtendedDatatype = URL;

        }
        field(50060; LTG; Option)
        {
            OptionMembers = ,Ja,Nein;


        }

        field(50070; "Area Responsible"; Code[10])
        {



        }
        field(50080; "MCA-ID"; Text[30]) { }
        field(50090; "Internal Limit"; Decimal) { }
        field(50100; MCA; Boolean) { }
        field(50110; "Creditreform-ID"; Text[50]) { }

    }
    fieldgroups
    {
        addlast(DropDown; "Name 2") { }
    }



    //test
}