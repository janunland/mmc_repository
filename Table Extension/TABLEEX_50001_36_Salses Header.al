tableextension 50001 "Sales Header" extends "Sales Header"
{
    fields
    {


        field(50000; "Order Date Registration"; Date)
        {
            CaptionML = ENU = 'Order Date Registration',
                       DEU = 'Auftragsdatum Erfassung';
        }
        field(50010; "MMC_Customer Type"; Code[20])
        {
            CaptionML = ENU = 'MMC_Customer Type',
                       DEU = 'MMC_Kundenart';
        }

        field(50020; "Create Combine Shipment"; Boolean)
        {

        }
        field(50030; "Combine shiptments code"; code[10])
        {
            CaptionML = ENU = 'Combine shiptments code',
                        DEU = 'Sammelrechnungscode';
        }
        field(50040; "OrderOnlyNo"; Code[10])
        {
            CaptionML = ENU = 'OrderOnlyNo',
                        DEU = 'OrderOnlyNo';
        }
        field(50050; "TTB"; Boolean)
        {
            CaptionML = ENU = 'TTB',
                        DEU = 'TTB';
            trigger OnValidate()
            Begin
                IF TTB = true THEN BEGIN
                    SalesLine.SETRANGE(SalesLine."Document Type", "Document Type");
                    SalesLine.SETRANGE(SalesLine."Document No.", "No.");
                    IF SalesLine.FINDSET THEN BEGIN
                        REPEAT
                            SalesLine.VALIDATE(SalesLine."Line Discount %", 100);
                            SalesLine.MODIFY;
                        UNTIL SalesLine.NEXT = 0;
                    END;
                End;
            End;
        }
        field(50060; "Changed by IC"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = exist ("Purchase Order Change Entry" where
            ("Document No." = field("No.")));


        }
        field(50070; ICTransfer; text[50]) { }

        modify("Sell-to Customer No.")
        {
            trigger OnAfterValidate()
            begin
                if "Payment Terms Code" = 'ANTPAG' then
                    Message('Payment in advance Customer !');
            end;
        }
        field(50080; ChangedDateSub; Boolean) { }

        field(50090; "PO Number"; code[20])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Sales Line"."Purchase Order No." where
            ("Document No." = field("No.")));
            CaptionML = ENU = 'PO Number',
                        DEU = 'PO Number';


        }
        field(50100; "HQ Number"; code[20])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Purchase Header"."Vendor Order No." where
            ("No." = field("PO Number")));
            CaptionML = ENU = 'HQ Number',
                        DEU = 'HQ Number';


        }



    }
    procedure CalcRemainingTestToolBudget() remainingBudgetLoc: Decimal;

    var
        testtoolBudgetLoc: record "MMC Testtool Budget";


    begin

        IF "Salesperson Code" <> '' THEN BEGIN
            testtoolBudgetLoc.SETRANGE("Sales Person", "Salesperson Code");
            testtoolBudgetLoc.SETFILTER("Valid from", '<=%1', WORKDATE);
            testtoolBudgetLoc.SETFILTER("Valid to", '>=%1', WORKDATE);
            IF testtoolBudgetLoc.FINDFIRST THEN BEGIN
                remainingBudgetLoc := testtoolBudgetLoc."Remaining budget (MW)";
            END ELSE BEGIN
                remainingBudgetLoc := 0;
            END;
        END;



    END;


    var
        SalesLine: record "Sales Line";
        SalesLine2: record "sales line";




    trigger OnInsert()
    begin

        "Assigned User ID" := UserId;
        ICTransfer := 'Open';
        REC.OrderOnlyNo := DelChr(REC."No.", '=', '-');
    end;




}