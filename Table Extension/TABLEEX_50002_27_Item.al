tableextension 50002 MMC_ITEM extends ITEM
{
    fields
    {
        field(50000; "Customer Request"; Boolean)
        {
            CaptionML = ENU = 'Customer Request',
                        DEU = 'Auf Kundenwunsch';
        }

        field(50010; "Tooltype"; Text[30])
        {
            CaptionML = ENU = 'Tooltype',
                        DEU = 'Tooltype';
        }

        field(50020; "Packing Unit"; Integer)
        {
            CaptionML = ENU = 'Packing Unit',
                        DEU = 'Verpackungseinheit';
        }
        field(50030; "Outranged Item"; Integer)
        {

        }
        field(50040; Outranged; Boolean)
        {
            CaptionML = ENU = 'Outranged Item',
                        DEU = 'Auslaufkennzeichen';
        }
        field(50050; "0000001"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('0000001')));
            CaptionML = ENU = '0000001',
                        DEU = '0000001';
            DecimalPlaces = 0 : 0;

        }
        field(50060; "0000002"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('0000002')));
            CaptionML = ENU = '0000002',
                        DEU = '0000002';
            DecimalPlaces = 0 : 0;

        }
        field(50070; "TESTLAG"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('TESTLAG')));
            CaptionML = ENU = 'TESTLAG',
                        DEU = 'TESTLAG';
            DecimalPlaces = 0 : 0;

        }
        field(50080; "CONF"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('CONF')));
            CaptionML = ENU = 'CONF',
                        DEU = 'CONF';
            DecimalPlaces = 0 : 0;

        }
        field(50090; "OUTMMCHQ"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('OUTMMCHQ')));
            CaptionML = ENU = 'OUTMMCHQ',
                        DEU = 'OUTMMCHQ';
            DecimalPlaces = 0 : 0;

        }
        field(50100; "PO"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("FAV External Inventory".Quantity where
            ("Item No." = field("No."),
            "Location Code" = const('PO')));
            CaptionML = ENU = 'PO',
                        DEU = 'PO';
            DecimalPlaces = 0 : 0;

        }

    }


}