tableextension 50003 "MMC Sales Invoice Header" extends "Sales Invoice Header"
{

    fields
    {
        field(50000; "Order Date Registration"; Date)
        {
            CaptionML = ENU = 'Order Date Registration',
                       DEU = 'Auftragsdatum Erfassung';
        }
        field(50010; "MMC_Customer Type"; Code[20])
        {
            CaptionML = ENU = 'MMC_Customer Type',
                       DEU = 'MMC_Kundenart';
        }

        field(50020; "Create Combine Shipment"; Boolean)
        {

        }
        field(50030; "Combine shiptments code"; code[10])
        {
            CaptionML = ENU = 'Combine shiptments code',
                        DEU = 'Sammelrechnungscode';
        }



    }

}