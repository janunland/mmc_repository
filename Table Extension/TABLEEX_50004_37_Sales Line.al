tableextension 50004 "MMC Sales Line" extends "Sales Line"
{


    fields
    {

        field(50010; ChangedDate; Boolean) { }
        field(50020; DatebeforeChange; Date) { }

        modify(Quantity)
        {
            trigger OnAfterValidate()

            Begin
                if not SalesHeader.GET("Document Type", "Document No.") then
                    CLEAR(SalesHeader);
                IF SalesHeader."TTB" = true THEN BEGIN
                    VALIDATE("Line Discount %", 100);
                    validate("Line Discount 1 %", 100);
                END ELSE BEGIN

                END;

                begin

                    if Item1.GET(rec."No.") then begin
                        IF Item1."Packing Unit" > 0 THEN BEGIN
                            IF NOT
                             (Quantity / item1."Packing Unit" MOD 1 = 0) THEN
                                MESSAGE('Packing Unit for this Item: %1 Pcs.', item1."Packing Unit");
                        END;
                        If Quantity > 0 then Begin
                            if item1."Minimum Order Quantity" > 0 then
                                if item1."Customer Request" = true then begin
                                    if rec.Quantity < item1."Minimum Order Quantity" then begin
                                        if "Document Type" <> 2 then
                                            Message('Min Order Quantity for this item is: %1', item1."Minimum Order Quantity");

                                    end;
                                end;
                        end;

                        If Quantity > 0 then Begin
                            if item1.Outranged = true then begin
                                ExtInvent.init;
                                ExtInvent.setrange("Item No.", rec."No.");
                                ExtInvent.setrange("Location Code", '0000001');
                                ExtInvent.FindFirst();
                                if ExtInvent.Quantity > 0 then begin
                                    if "Document Type" <> 2 then
                                        Message('Outranged Item: Only Restqty. available!')
                                end else begin
                                    if "Sell-to Customer No." <> '2252013' then
                                        if "Document Type" <> 2 then
                                            Error('Outranged Item without Qty.....not possible !');

                                END;

                            end;
                        End;

                    end;

                end;

                begin

                end;

            End;
        }
        modify("No.")
        {
            trigger OnAfterValidate()
            Begin
                if not SalesHeader.GET("Document Type", "Document No.") then
                    CLEAR(SalesHeader);
                IF SalesHeader."TTB" = true THEN BEGIN
                    VALIDATE("Line Discount %", 100);
                    validate("Line Discount 1 %", 100);
                END ELSE BEGIN

                END;

                if "Document Type" = 1 then Begin
                    SalesLine1.init;
                    SalesLine1.SetRange("No.", Rec."No.");
                    SalesLine1.setrange("Document Type", 0);
                    SalesLine1.Setrange("Sell-to Customer No.", rec."Sell-to Customer No.");
                    if SalesLine1.Findfirst then
                        Message('There are offers for this Customer No.: ' + SalesLine1."Document No.")

                END;
                if "Document Type" = 1 then Begin
                    SalesLine1.init;
                    SalesLine1.SetRange("No.", Rec."No.");
                    SalesLine1.setrange("Document Type", 4);
                    SalesLine1.Setrange("Sell-to Customer No.", rec."Sell-to Customer No.");
                    if SalesLine1.Findfirst then
                        Message('There are Orders on Call for this Customer No.: ' + SalesLine1."Document No.")

                END;




            End;
        }
        modify("Blanket Order Line No.")
        {
            trigger OnAfterValidate()
            Begin
                SalesLineBlanket.init;
                SalesLineBlanket.SetRange("Document Type", 4);
                SalesLineBlanket.SetRange("Document No.", rec."Blanket Order No.");
                SalesLineBlanket.SetRange("Line No.", rec."Blanket Order Line No.");
                SalesLineBlanket.FindFirst();
                rec."Allow Invoice Disc." := SalesLineBlanket."Allow Invoice Disc.";
                rec.Modify();
                validate(rec."Allow Invoice Disc.");

            END;

        }


        modify("Planned Delivery Date")

        {
            trigger OnAfterValidate()
            Begin
                if rec."No." <> '' then
                    if rec.Quantity > 0 then
                        if rec."Planned Delivery Date" <> xrec."Planned Delivery Date" + 1 then Begin
                            SalesHeader2.init;
                            SalesHeader2.setrange("Document Type", 1);
                            SalesHeader2.setrange("No.", rec."Document No.");
                            SalesHeader2.findfirst;
                            SalesHeader2.ChangedDateSub := true;
                            SalesHeader2.Modify();

                            rec.ChangedDate := true;
                            rec.DatebeforeChange := xrec."Planned Delivery Date";
                        End;


            End;
        }





        field(50000; "Customer Wish"; Boolean)

        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Item"."Customer Request" where
            ("No." = field("No.")));
            CaptionML = ENU = 'Customer Request',
                        DEU = 'Kundenwunsch';
        }

    }


    var
        SalesHeader: record "Sales Header";
        SalesLine1: record "Sales Line";
        SalesLine2: record "Sales line";
        SalesLine3: record "Sales Line";
        Item1: record Item;
        SalesLineBlanket: record "Sales Line";
        SalesHeader2: record "Sales Header";
        SalesHeader3: Record "Sales Header";
        ExtInvent: record "FAV External Inventory";










}