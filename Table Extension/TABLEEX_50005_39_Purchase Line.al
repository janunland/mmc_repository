tableextension 50005 "MMC Purchase Line" extends "Purchase Line"
{
    fields
    {
        field(50010; "Customer Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Sales Header"."Sell-to Customer Name" where
            ("No." = field("Order Number")));
            CaptionML = ENU = 'Customer Name',
                        DEU = 'Kundenname';

        }
        field(50020; "MMC Invoice No."; Text[50]) { }
        field(50030; "MMC Ship-to Name"; Text[50]) { }

        field(50040; "Salesperson"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Sales Header"."Salesperson Code" where
            ("No." = field("Order Number")));
            CaptionML = ENU = 'Salesperson Code',
                        DEU = 'Verkäufer';

        }
    }


    var
        myInt: Integer;
}