tableextension 50006 "MMC Purch. Rcpt. Line" extends "Purch. Rcpt. Line"
{
    fields
    {
        field(50010; "MMC Invoice No"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Purchase Header"."Vendor Invoice No." where
            ("No." = field("Order No.")));
            CaptionML = ENU = 'MMC Invoice No',
                        DEU = 'MMC Rechnungsnr.';

        }
    }

    var
        myInt: Integer;
}