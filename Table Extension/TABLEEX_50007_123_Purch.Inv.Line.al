tableextension 50007 "MMC Purch.Inv.Line" extends "Purch. Inv. Line"
{
    fields
    {
        field(50010; "Customer Name"; Text[50]) { }
        field(50020; "MMC Invoice No."; Text[50]) { }
        field(50030; "MMC Ship-to Name"; Text[50]) { }

        field(50040; "Tariff No."; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = lookup (Item."Tariff No." where
            ("No." = field("No.")));
            CaptionML = ENU = 'Tariff No.',
                        DEU = 'Zolltarifnummer';

        }
        field(50050; "Netweight"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup (Item."Net Weight" where
            ("No." = field("No.")));
            CaptionML = ENU = 'Net Weight',
                        DEU = 'Nettogewicht';

        }
        field(50060; "Customer No."; code[20])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Purchase Header"."Sell-to Customer No." where
            ("No." = field("Order No.")));
            CaptionML = ENU = 'Customer No.',
                        DEU = 'Kundennummer';

        }
        field(50070; "Province"; text[30])
        {
            FieldClass = FlowField;
            CalcFormula = lookup (Customer.County where
            ("No." = field("Customer No.")));
            CaptionML = ENU = 'Province',
                        DEU = 'Bundesregion';

        }
        field(50080; "Salesperson"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Sales Header"."Salesperson Code" where
            ("No." = field("Order Number")));
            CaptionML = ENU = 'Salesperson Code',
                        DEU = 'Verkäufer';

        }


    }

    var
        myInt: Integer;
}