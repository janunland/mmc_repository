tableextension 50009 "MMC Reminder" extends "Reminder Header"
{
    fields
    {
        field(50010; "Internal Limit"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Customer"."Internal Limit" where
            ("No." = field("Customer No.")));
            CaptionML = ENU = 'Internal Limit',
                        DEU = 'Internes Limit';
        }
        field(50020; "Credit Limit"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = lookup ("Customer"."Credit Limit (LCY)" where
            ("No." = field("Customer No.")));
            CaptionML = ENU = 'Credit Limit',
                        DEU = 'Kreditlimit';
        }
    }

    var
        myInt: Integer;
}