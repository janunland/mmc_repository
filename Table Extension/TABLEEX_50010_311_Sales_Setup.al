tableextension 50010 MMC_Sales_Setup extends "Sales & Receivables Setup"
{
    fields
    {
        field(50010; "Invoice Text"; Text[250])
        {

            CaptionML = ENU = 'Invoice Text',
                        DEU = 'Rechnungstext';
        }
    }

    var
        myInt: Integer;
}